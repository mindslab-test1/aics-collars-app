import 'package:flutter/cupertino.dart';

const kCopyright = '© Copyright (주)칼라스컴퍼니. All Right Reserved.';

//AiPractice
const kAiMsgStyle = TextStyle(
    fontSize: 12,
    fontFamily: 'NotoSansCJKkr',
    fontWeight: FontWeight.w400
);

const kAiMsgNameStyle = TextStyle(
    fontSize: 14,
    fontFamily: 'NotoSansCJKkr',
    fontWeight: FontWeight.w700
);

const kAiFinishMsg = "Good job! You completed AI Practice #1. The voice bot course is finished. Please press ‘Yes’ to exit.";