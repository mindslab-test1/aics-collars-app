// Avatar
const kAvatarMary = "assets/avatar/mary.png";

// Icon
// @ AppBar
const kArrowBack = "assets/icon/arrow_back.svg";
const kCloseSvg = "assets/icon/close_btn.svg";
// Bottom Navigation Bar
const kHome = "assets/icon/home.svg";
const kCollars = "assets/icon/collars.svg";
const kMyPage = "assets/icon/mypage.svg";

// @ Social Login
const kGoogleIcon = "assets/icon/social/google.svg";
const kNaverIcon = "assets/icon/social/naver.svg";
const kKakaoIcon = "assets/icon/social/kakao.svg";

const kUserSvg = "assets/icon/user.svg";
const KCheckSvg = "assets/icon/check.svg";
const kNextSvg = "assets/icon/next.svg";
const kMicSvg = "assets/icon/mic.svg";
const kMicSmallSvg = "assets/icon/mic_small.svg";
const kMicSmall2Svg = "assets/icon/mic_small_2.svg";
const kMail = "assets/icon/mail.svg";
const kLock = "assets/icon/lock.svg";
const kSetting = "assets/icon/setting.svg";
const kHangulSvg = "assets/icon/hangul.svg";
const kSoundSvg = "assets/icon/sound.svg";
const kArrowNext = "assets/icon/arrow_next.svg";
const kClockSvg = "assets/icon/clock.svg";
const kCalendarSvg = "assets/icon/calendar.svg";
const kCheckedBoxSvg = "assets/icon/checked_box.svg";
const kBuilding = "assets/icon/building.svg";
const kId = "assets/icon/id.svg";

// @Temp
const kUserEditSvg = "assets/icon/user_edit.svg";
const kFakeUserEditSvg = "assets/icon/fake_user_edit.svg";

// ai practice & two cent
const kHintSoundSvg = "assets/icon/hint_sound.svg";
const kHintHangulSvg = "assets/icon/translate.svg";
const eGradeAsvg = "assets/icon/grade_a.svg";
const eGradBsvg = "assets/icon/grade_b.svg";
const eGradeCsvg = "assets/icon/grade_c.svg";