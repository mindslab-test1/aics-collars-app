import 'package:flutter/cupertino.dart';

import 'palette.dart';

const kInfoColor = gray4;
// Active, InActive
const kActive = blue5;
const kInActive = gray4;
const kInActive2 = blue6;

// Error
const kError = red5;

// AppBar
const kAppBarTitle = black;
const kAppBarBack = black;

// Sign In
const kSignInBtn1 = blue5;
const kSignInBtn2 = white;

// Social Login
// @ Background
const kGoogleBg = white9;
const kNaverBg = green;
const kKakaoBg = yellow;
// @ Item
const kGoogleColor = white;
const kNaverColor = white;
const kKaKaoColor = brown;

// Intro
const kLaterColor = gray4;

// Bnb(BottomNavigationBar)
const kBnbBg = black;
const kBnbSelected = white;
const kBnbUnselected = gray6;

// Button
const kBtnActive = blue5;
const kBtnInActive = gray4;
const kBtnActive2 = red8;

// Decor
const kBlankBox = gray3;
const kDivider = gray3;

// RatingBar
const kStartActive = red9;
const kStartInActive = red0;

const kCorrect = blue5;
const kWrong = red5;

// Radar Chart
const kRadarBg = blue2;
const kRadarCircle = blue3;
const kRadarVertex = blue4;
const kRadarGraphBg = Color.fromRGBO(71, 92, 240, 0.15);
const kRadarGraphBorder = blue4;

const kDefaultGradient = LinearGradient(
  colors: [Color(0xff1d63fc), Color(0xff896cfd)],
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
);

const kGradientHorizontal = LinearGradient(
  colors: [Color(0xff5262f2), Color(0xff896cfd)],
  begin: Alignment.centerRight,
  end: Alignment.centerLeft,
);

// TwoCents
const kLeftMsgBorder = Color(0xfff8f8f8);
const kLeftMsgBg = Color(0xffeaeaea);
const kRightMsgBorder = Color(0xffeaeaea);
const kRightMsgBg = Color(0xffc7c7c7);

//AiPractice
const kTopMsgBg = Color(0xffeff0ff);
const kChatbotMsgBg = Color.fromRGBO(240, 240, 240, 1);
const kUserRecordBg = Color(0xffeff0ff);
