import 'package:flutter/material.dart';

// BLACK to WHITE
const black = Colors.black;
const black0 = Color(0xff333333);
const gray6 = Color(0xff71707b);
const gray5 = Color(0xff707070);
const gray4 = Color(0xffbdbecc);
const gray3 = Color(0xfff2f2f2);
const white9 = Color(0xfff2f2f2);
const white = Colors.white;
// RED
const red = Color(0xffdb4437);
const red8 = Color(0xfff25252);
const red5 = Color(0xffda5354);
const red9 = Color(0xffe05c5c);
const red0 = Color(0xfff5cdcd);
// BROWN
const brown = Color(0xff391b1b);
// YELLOW
const yellow = Color(0xffffe500);
// GREEN
const green = Color(0xff1ec800);
// BLUE
const blue6 = Color(0xffd1d5fa);
const blue5 = Color(0xff4858ea);
const blue4 = Color(0xff5262f2);
const blue3 = Color(0xffdee0e9);
const blue2 = Color(0xffF6F7FE);