import 'package:flutter/material.dart';

// Social Login
const kSocialLoginButtonSize = 50.0;
const kSocialLoginIconSize = 30.0;

// Bnb(BottomNavigationBar)
const kBnbRadius = 8.0;

// Padding
const kPaddingAll = EdgeInsets.all(24);
const kPaddingSymmetric = EdgeInsets.symmetric(horizontal: 24, vertical: 20);
const kPaddingHorizontal = EdgeInsets.symmetric(horizontal: 24);
const kPaddingVertical = EdgeInsets.symmetric(vertical: 20);
const kPaddingBtnVertical = EdgeInsets.symmetric(vertical: 16.0);

// Container
const kPaddginTextField = EdgeInsets.symmetric(horizontal: 20, vertical: 5);