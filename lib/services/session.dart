import 'dart:convert';

import 'package:collars_app_flutter/data/session/session.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class SessionService {
  String _appServer = DotEnv().env['APP_SERVER'];

  Future<List<Session>> getSessionList(
      {int current, int increment, bool data}) async {
    String url = '$_appServer/session/data:list';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, dynamic> body = {
      'current': current,
      'increment': increment,
      'data': data
    };
    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));

    List<dynamic> map = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    List<Session> result = map.map((v) => Session.fromJson(v)).toList();

    return result;
  }
}
