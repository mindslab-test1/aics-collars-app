import 'dart:convert';

import 'package:collars_app_flutter/data/evereading/evereading.dart';
import 'package:collars_app_flutter/data/hashtag/hash_tag.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class EvereadingService {
  String _appServer = DotEnv().env['APP_SERVER'];

  Future<List<HashTag>> getSectors(String lang) async {
    String url = '$_appServer/evereading/sectors';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, String> body = {'lang': lang};
    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));

    List<dynamic> map = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    List<HashTag> result = map.map((v) => HashTag.fromJson(v)).toList();

    return result;
  }

  Future getColumns({int tagId, int current, int increment}) async {
    String url = '$_appServer/evereading/data:list';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, dynamic> body = {
      'tagId': tagId,
      'current': current,
      'increment': increment,
      'data': true
    };
    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));

    List<dynamic> map = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    List<Evereading> result = map.map((v) => Evereading.fromJson(v)).toList();

    return result;
  }
}
