import 'dart:convert';

import 'package:collars_app_flutter/data/survey_data.dart';
import 'package:collars_app_flutter/data/survey_result.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class SurveyService {
  String _appServer = DotEnv().env['APP_SERVER'];

  Future<List<SurveyData>> getSurvey() async {
    String url = '$_appServer/survey';
    Map<String, String> headers = {'content-type': 'application/json'};

    http.Response response = await http.get(url, headers: headers);

    List<dynamic> surveyDataMap =
        jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    List<SurveyData> surveyDataList = List();
    surveyDataMap.forEach((data) {
      SurveyData surveyData = SurveyData.fromJson(data);
      surveyDataList.add(surveyData);
    });
    return surveyDataList;
  }

  Future<bool> uploadSurveyResult(
      {int id, String uid, List<SurveyResult> result}) async {
    String url = '$_appServer/survey/result:upload';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, dynamic> body = {'id': id, 'uid': uid, 'result': result};
    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));
    return jsonDecode(utf8.decode(response.bodyBytes))['payload'];
  }
}
