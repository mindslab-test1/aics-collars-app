import 'dart:convert';
import 'dart:io';

import 'package:collars_app_flutter/data/twoCents/twoCents.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class TwoCentsService {
  String _appServer = DotEnv().env['APP_SERVER'];

  Future getScripts({int current, int increment}) async {
    String url = '$_appServer/twoCents/data:list';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, dynamic> body = {
      'current': current,
      'increment': increment,
      'data': true
    };
    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));
    List<dynamic> map = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    List<TwoCents> result = map.map((v) => TwoCents.fromJson(v)).toList();
    return result;
  }
}
