import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class PurchaseService {
  String _appServer = DotEnv().env['APP_SERVER'];

  Future<int> purchaseSession({
    int id,
    String uid,
    int sessionId,
    int expiration,
    String purchaseToken,
  }) async {
    String url = '$_appServer/purchase/session';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, dynamic> body = {
      'id': id,
      'uid': uid,
      'sessionId': sessionId,
      'expiration': expiration,
      'purchaseToken': purchaseToken,
    };

    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));
    int code = jsonDecode(utf8.decode(response.bodyBytes))['code'];
    return code;
  }
}
