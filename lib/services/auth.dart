import 'dart:convert';

import 'package:collars_app_flutter/data/user_data.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_naver_login/flutter_naver_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:kakao_flutter_sdk/all.dart' as Kakao;

const SIGN_IN_EMAIL_VALID_FAIL = 'SIGN_IN_EMAIL_VALID_FAIL';
const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
const SIGN_IN_FAIL = 'SIGN_IN_FAIL';

class AuthService {
  String _appServer = DotEnv().env['APP_SERVER'];
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Init user data on a provider
  User _userFromFirebaseUser(FirebaseUser user) {
    print("USER FROM FIREBASE");
    return user != null && user.isEmailVerified
        ? User(uid: user.uid, email: user.email)
        : User(uid: null, email: null);
  }

  Stream<User> get user {
    return _auth.onAuthStateChanged.map((FirebaseUser user) {
      return _userFromFirebaseUser(user);
    });
  }

  // Sign Up
  Future<AuthResult> signUpWithEmail(String email, String password) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);

      return result;
    } catch (e) {
      print(e);
    }
  }

  void sendEmailVerification(String email, String password) async {
    final result = await _auth.signInWithEmailAndPassword(
        email: email, password: password);
    if (!result.user.isEmailVerified) {
      result.user.sendEmailVerification();
    }
  }

  void resetPassword({String email}) async {
    await _auth.sendPasswordResetEmail(email: email);
  }

  // Sign in
  Future<String> signInWithEmail(String email, String password) async {
    try {
      bool isValid = await validUser(
        provider: PASSWORD,
        email: email,
      );
      if (isValid) {
        final result = await _auth.signInWithEmailAndPassword(
            email: email, password: password);
        if (!result.user.isEmailVerified) {
          return SIGN_IN_EMAIL_VALID_FAIL;
        }
        return SIGN_IN_SUCCESS;
      } else {
        return SIGN_IN_FAIL;
      }
    } catch (e) {
      return SIGN_IN_FAIL;
    }
  }

  void signInWithGoogle() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();

    bool isValid = await validUser(
      provider: GOOGLE,
      email: googleSignInAccount.email,
      name: googleSignInAccount.displayName,
    );

    if (isValid) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleSignInAuthentication.idToken,
        accessToken: googleSignInAuthentication.accessToken,
      );
      final AuthResult authResult =
          await _auth.signInWithCredential(credential);
    }
  }

  void signInWithNaver() async {
    NaverLoginResult naverLogin = await FlutterNaverLogin.logIn();
    NaverAccessToken naverToken = await FlutterNaverLogin.currentAccessToken;

    String customToken = await validUser(
      provider: NAVER,
      email: naverLogin.account.email,
      name: naverLogin.account.name,
    );

    final AuthResult authResult =
        await _auth.signInWithCustomToken(token: customToken);
  }

  void signInWithKakao() async {
    final installed = await Kakao.isKakaoTalkInstalled();
    final authCode = installed
        ? await Kakao.AuthCodeClient.instance.requestWithTalk()
        : await Kakao.AuthCodeClient.instance.request();
    Kakao.AccessTokenResponse kakaoToken =
        await Kakao.AuthApi.instance.issueAccessToken(authCode);
    await Kakao.AccessTokenStore.instance.toStore(kakaoToken);
    Kakao.User kakaoUser = await Kakao.UserApi.instance.me();
    String customToken = await validUser(
      provider: KAKAO,
      email: kakaoUser.kakaoAccount.email,
      name: kakaoUser.kakaoAccount.profile.nickname,
    );

    final AuthResult authResult =
        await _auth.signInWithCustomToken(token: customToken);
  }

  // Sign out
  void signOut() async {
    await GoogleSignIn().signOut();
    await _auth.signOut();
  }

  // Function
  // @desc: Check user is exist in the DB (for Google)
  Future<dynamic> validUser(
      {String provider, String email, String name}) async {
    String url = '$_appServer/auth/validUser';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, String> body = {
      'provider': provider,
      'email': email,
      'name': name
    };

    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));
    return jsonDecode(utf8.decode(response.bodyBytes))['payload'];
  }

  Future<int> validCompanyUser({String code, String id, String pw}) async {
    String url = '$_appServer/auth/validCompanyUser';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, String> body = {'code': code, 'id': id, 'pw': pw};

    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));
    return jsonDecode(utf8.decode(response.bodyBytes))['payload'] ;
  }

  Future<UserData> getUser(String uid) async {
    String url = '$_appServer/auth/user';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, String> body = {'uid': uid};
    http.Response response =
    await http.post(url, headers: headers, body: jsonEncode(body));
    Map userMap = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    UserData result = UserData.fromJson(userMap);
    return result;
  }

  Future<UserData> getUserById(int id) async {
    String url = '$_appServer/auth/userById';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, int> body = {'id': id};
    http.Response response =
    await http.post(url, headers: headers, body: jsonEncode(body));
    Map userMap = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    UserData result = UserData.fromJson(userMap);
    return result;
  }

  Future<bool> updateUserName({int id,String uid, String name}) async {
    String url = '$_appServer/auth/user/name';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, dynamic> body = {'id': id, 'uid': uid, 'name': name};
    http.Response response =
        await http.patch(url, headers: headers, body: jsonEncode(body));
    bool result = jsonDecode(response.body)['payload'];
    return result;
  }
}
