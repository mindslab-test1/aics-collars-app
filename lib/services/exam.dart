import 'dart:convert';
import 'dart:io';

import 'package:collars_app_flutter/data/exam/exam.dart';
import 'package:collars_app_flutter/data/exam/exam_result.dart';
import 'package:collars_app_flutter/data/user/user_exam.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class ExamService {
  String _appServer = DotEnv().env['APP_SERVER'];

  Future<String> uploadExamAudio(File file) async {
    String url = '$_appServer/exam/audio';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files.add(
      http.MultipartFile.fromBytes('file', file.readAsBytesSync(),
          filename: 'request.wav'),
    );
    String result;
    await request.send().then((value) async {
      await http.Response.fromStream(value).then((response) {
        response.statusCode == 200
            ? result = jsonDecode(utf8.decode(response.bodyBytes))['payload']
            : result = "";
      });
    });
    return result;
  }

  Future<UserExam> uploadExamResult(
      {int id,String uid, String type, int examId, List<ExamResult> result}) async {
    String url = '$_appServer/exam/result:upload';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, dynamic> body = {
      'id': id,
      'uid': uid,
      'type': type,
      'examId': examId,
      'result': result
    };

    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));
    Map examMap = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    UserExam userExam = UserExam.fromJson(examMap);

    return userExam;
  }

  Future<Exam> getLevelExam({int id}) async {
    String url = '$_appServer/exam/data';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, int> body = {'id': id};

    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));
    Map examMap = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    Exam result = Exam.fromJson(examMap);

    return result;
  }

  Future<Exam> getSessionExam({int examId}) async {
    String url = '$_appServer/exam/data';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, dynamic> body = {
      'type': 'session',
      'id': examId,
    };
    http.Response response =
        await http.post(url, headers: headers, body: jsonEncode(body));
    Map examMap = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    Exam result = Exam.fromJson(examMap);

    return result;
  }
}
