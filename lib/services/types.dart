
// Sign In Types
import 'package:collars_app_flutter/providers/evereading_provider.dart';

const PASSWORD = "PASSWORD";
const GOOGLE = "GOOGLE";
const NAVER = "NAVER";
const KAKAO = "KAKAO";
const FACEBOOK = "FACEBOOK";

// Intro Types
const INTRO_CONSULT_NAME = "INTRO_CONSULT_NAME";
const INTRO_CONSULT_SURVEY = "INTRO_CONSULT_SURVEY";
const INTRO_CONSULT_LEVEL = "INTRO_CONSULT_LEVEL";

// Button Types
const ACTIVATE = "activate";
const FIXED = "fixed";
const TOGGLE = "toggle";
const ONCE = "once";

// Exam Types
const LEVEL = "LEVEL";
const SESSION = "SESSION";

const CHOICE = "CHOICE";
const PUZZLE = "PUZZLE";
const SPEAKING = "SPEAKING";
const SHORT_ANSWER = "SHORT_ANSWER";
const HIGHLIGHT = "HIGHLIGHT";

const alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

// Record Status
const AUDIO_RECORDING = "RECORDING";
const AUDIO_SENDING = "SENDING";
const AUDIO_STOPPED = "STOPPED";

// Evereading
// @ Sector
const LANG_KOR = "KOR";
const LANG_ENG = "ENG";