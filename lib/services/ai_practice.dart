import 'dart:convert';
import 'dart:io';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class ChatbotService{
  // chatbot test api
  String chatbotServer = DotEnv().env['CHATBOT_SERVER'];
  // stt
  String _appServer = DotEnv().env['APP_SERVER'];


  // chatbot에서 user답변을 보냄
  Future sendUserMsg(String userInfo, String msg) async{
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
    };

    Map<String, dynamic> params = {
      'host' : '2',
      'session' : userInfo,
      'data' :{
        'utter' : msg
      },
      'lang' : '1'
    };

    final response = await http.post(chatbotServer,
        headers: headers,
        body: jsonEncode(params));
    return jsonDecode(response.body);

  }

  // user답변 => stt
  Future<String> uploadUserAudio(File file) async{
    String url = '$_appServer/exam/audio';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files.add(
      http.MultipartFile.fromBytes('file', file.readAsBytesSync(),
      filename: 'request.wav'),
    );
    String res;
    await request.send().then((value) async {
      await http.Response.fromStream(value).then((response){
        res = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
      });
    });
    return res;
  }



}