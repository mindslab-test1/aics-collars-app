import 'dart:convert';
import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class MaumService {
  String _appServer = DotEnv().env['APP_SERVER'];

  Future<int> speaking({File file, List<String> textList}) async {
    String url = '$_appServer/maum/speaking';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files.add(
      http.MultipartFile.fromBytes('file', file.readAsBytesSync(),
          filename: 'request.wav'),
    );
    textList.forEach((text) {
      request.fields['text'] = text;
    });

    int score;
    try {
      await request.send().then((value) async {
        await http.Response.fromStream(value).then((response) {
          score = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
        });
      });
      return score;
    } catch (e) {
      return 0;
    }
  }

  //chatbot 답변 => tts
  Future downloadChatAudio(String msg) async {
    String url = '$_appServer/maum/tts';

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    Map<String, dynamic> params = {"text": msg};

    final response =
        await http.post(url, headers: headers, body: jsonEncode(params));
    return response.bodyBytes;
  }
}
