import 'package:flutter/material.dart';

class RouteForm {
  final String name;
  final String icon;
  final Widget component;

  RouteForm({this.name, this.icon, this.component});
}
