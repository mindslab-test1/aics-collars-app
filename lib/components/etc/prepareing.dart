import 'package:flutter/material.dart';

class Preparing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: Center(
        child: Text(
          '서비스를 준비중입니다.',
          style: Theme.of(context).textTheme.headline2,
        ),
      ),
    );
  }
}
