import 'package:flutter/material.dart';

class RatingBar extends StatelessWidget {
  final int itemCount;
  final num rating;
  final Color activeColor;
  final Color inactiveColor;
  final Text label;

  const RatingBar(
      {this.rating,
      this.itemCount,
      this.activeColor,
      this.inactiveColor,
      this.label});

  Widget buildRatingBar() {
    List<Widget> items = [];

    for (int i = 0; i < itemCount; i++) {
      if (i + 1 <= rating/2) {
        items.add(Icon(
          Icons.star,
          color: activeColor,
        ));
      } else {
        items.add(Icon(
          Icons.star,
          color: inactiveColor,
        ));
      }
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ...items,
        SizedBox(width: 5),
        label != null ? label : Container(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildRatingBar();
  }
}
