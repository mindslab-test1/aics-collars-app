import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SettingAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Color titleColor;
  final Widget action;

  const SettingAppBar({
    Key key,
    this.title,
    this.action,
    this.titleColor,
  }) : super(key: key);

  @override
  Size get preferredSize => new Size.fromHeight(56);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(
        title,
        style:
            Theme.of(context).textTheme.headline1.copyWith(color: titleColor),
      ),
      leading: Navigator.canPop(context)
          ? GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: SvgPicture.asset(
                kArrowBack,
                color: Colors.black,
              ),
            )
          : null,
      actions: [
        action,
      ],
      backgroundColor: Colors.transparent,
      bottomOpacity: 0.0,
      elevation: 0.0,
    );
  }
}
