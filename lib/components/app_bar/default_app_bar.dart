import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DefaultAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Color color;
  final Function leading;
  final String icon;

  const DefaultAppBar({
    Key key,
    this.title = "",
    this.color = black,
    this.leading, this.icon,
  }) : super(key: key);

  @override
  Size get preferredSize => new Size.fromHeight(56);

  @override
  Widget build(BuildContext context) {
    Widget buildLeading() {
      if (leading != null) {
        return GestureDetector(
          onTap: () {
            leading();
          },
          child: SvgPicture.asset(
            kArrowBack,
            color: color,
          )
        );
      } else {
        return Navigator.canPop(context)
            ? GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: icon==null?SvgPicture.asset(
                  kArrowBack,
                  color: color,
                ):Icon(Icons.close, color: color,)
              )
            : null;
      }
    }

    return AppBar(
      centerTitle: true,
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'NotoSansCJKkr',
          fontSize: 12,
          fontWeight: FontWeight.w400,
          color: Colors.black
        ),
      ),
      leading: buildLeading(),
      backgroundColor: Colors.transparent,
      bottomOpacity: 0.0,
      elevation: 0.0,
    );
  }
}
