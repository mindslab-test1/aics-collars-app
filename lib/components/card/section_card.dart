import 'package:flutter/material.dart';

class SectionCard extends StatelessWidget {
  final Color borderColor;
  final Widget child;

  const SectionCard({
    Key key,
    this.borderColor,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16.0),
        border: Border.all(color: borderColor, width: 2),
      ),
      child: child,
    );
  }
}
