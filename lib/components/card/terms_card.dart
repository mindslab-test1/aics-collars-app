import 'package:collars_app_flutter/components/button/custom_radio_button.dart';
import 'package:collars_app_flutter/components/container/rect_container.dart';
import 'package:collars_app_flutter/data/terms_form.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class TermsCard extends StatelessWidget {
  final TermsForm terms;

  const TermsCard({
    Key key,
    this.terms,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RectContainer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Header
          Row(
            children: [
              CustomRadioButton(
                onChanged: (isChecked) {
                  terms.checked = isChecked;
                },
              ),
              SizedBox(width: 8),
              Text(terms.title, style: Theme.of(context).textTheme.subtitle2),
              SizedBox(width: 2),
              Text(
                terms.require ? '(필수)' : '(선택)',
                style: Theme.of(context)
                    .textTheme
                    .subtitle2
                    .copyWith(color: kActive),
              ),
            ],
          ),
          SizedBox(height: 24),
          // Body
          Container(
            width: double.infinity,
            height: 104,
            child: FutureBuilder(
              future: rootBundle.loadString(terms.content),
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                return snapshot.hasData
                    ? Markdown(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        data: snapshot.data,
                        styleSheet: MarkdownStyleSheet.fromTheme(Theme.of(context)).copyWith(
                          h1: Theme.of(context).textTheme.bodyText1,
                          p: Theme.of(context).textTheme.bodyText2,
                        ),
                      )
                    : Container();
              },
            ),
          ),
        ],
      ),
    );
  }
}
