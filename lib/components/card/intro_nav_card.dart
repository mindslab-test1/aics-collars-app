import 'package:collars_app_flutter/layouts/intro/consult/consult_level_screen.dart';
import 'package:collars_app_flutter/layouts/intro/consult/consult_name_screen.dart';
import 'package:collars_app_flutter/layouts/intro/consult/consult_survey_screen.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class IntroNavCard extends StatefulWidget {
  final Function jumpToPage;

  const IntroNavCard({Key key, this.jumpToPage}) : super(key: key);

  @override
  _IntroNavCardState createState() => _IntroNavCardState();
}

class _IntroNavCardState extends State<IntroNavCard> {
  double percentage = 0;
  String status;

  @override
  Widget build(BuildContext context) {
    final User _user = Provider.of<User>(context);

    if (_user.data.name == null) {
      setState(() {
        percentage = 0.1;
        status = "STAGE1";
      });
    } else if (_user.data.survey == null || _user.data.survey.length <= 0) {
      setState(() {
        percentage = 0.3;
        status = "STAGE2";
      });
    } else if (_user.data.exam == null || _user.data.exam.length <= 0) {
      setState(() {
        percentage = 0.6;
        status = "STAGE3";
      });
    } else if (_user.data.session.length <= 0) {
      setState(() {
        percentage = 0.8;
        status = "STAGE4";
      });
    } else {
      setState(() {
        percentage = 1.0;
        status = "STAGE5";
      });
    }

    return percentage >= 1.0
        ? Container()
        : ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  padding:
                      EdgeInsets.only(left: 16, top: 16, right: 16, bottom: 12),
                  decoration: BoxDecoration(
                    gradient: kGradientHorizontal,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'AI 맞춤강의 추천까지 완성도',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2
                            .copyWith(color: white),
                      ),
                      SizedBox(height: 8),
                      Stack(
                        children: [
                          Container(
                            height: 16,
                            child: ClipRRect(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50)),
                              child: LinearProgressIndicator(
                                value: percentage,
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(white),
                                backgroundColor:
                                    Color.fromRGBO(255, 255, 255, 0.5),
                              ),
                            ),
                          ),
                          Center(
                            child: Text('${(percentage * 100).toInt()} %'),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 15),
                  color: white9,
                  child: InkWell(
                    onTap: () {
                      switch (status) {
                        case "STAGE1":
                          Navigator.pushNamed(context, ConsultNameScreen.id);
                          break;
                        case "STAGE2":
                          Navigator.pushNamed(context, ConsultSurveyScreen.id);
                          break;
                        case "STAGE3":
                          Navigator.pushNamed(context, ConsultLevelScreen.id);
                          break;
                        default:
                          widget.jumpToPage(1);
                          break;
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '프로필을 완성하고 나에게 꼭 맞춘 강의 추천 받기',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              .copyWith(fontSize: 12, color: kBtnActive),
                        ),
                        SvgPicture.asset(kArrowNext),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
  }
}
