import 'package:collars_app_flutter/styles/image/default.dart';
import 'package:flutter/material.dart';

class CarouselCard extends StatelessWidget {
  final String image;

  const CarouselCard({
    Key key,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 312,
      height: 200,
      margin: EdgeInsets.symmetric(horizontal: 6),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      ),
      child: FadeInImage(
        image: image != null ? NetworkImage(image) : AssetImage(kEmptyImage),
        placeholder: AssetImage(kEmptyImage),
      ),
    );
  }
}
