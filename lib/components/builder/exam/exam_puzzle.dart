import 'package:collars_app_flutter/components/button/border_button.dart';
import 'package:collars_app_flutter/data/exam/exam_data.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ExamPuzzle extends StatefulWidget {
  final ExamData data;
  final ValueChanged<List<String>> onChanged;

  const ExamPuzzle({Key key, this.data, this.onChanged}) : super(key: key);

  @override
  _ExamPuzzleState createState() => _ExamPuzzleState();
}

class _ExamPuzzleState extends State<ExamPuzzle> {
  List<String> _answer = List();
  List<String> _shuffledAnswer;
  String _text = "";

  @override
  void initState() {
    _shuffledAnswer = List<String>.from(widget.data.answer);
    setState(() {
      _shuffledAnswer.shuffle();
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> buildPuzzle() {
      List<Widget> _list = new List();
      _shuffledAnswer.asMap().forEach((index, value) {
        _list.add(
          GestureDetector(
            onTap: () {
              setState(() {
                if (_answer.contains(value))
                  _answer.remove(value);
                else
                  _answer.add(value);
                _text = "";
                _answer.forEach((e) {
                  _text += e + " ";
                });
                widget.onChanged(_answer);
              });
            },
            child: BorderButton(
              isActive: _answer.contains(value) ? true : false,
              text: value,
              activeColor: kBtnActive,
              inActiveColor: kBtnInActive,
            ),
          ),
        );
      });
      return _list;
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 36),
          Text(
            widget.data.question,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          SizedBox(height: 8),
          Wrap(
            children: [
              SvgPicture.asset(
                kNextSvg,
                color: _text == "" ? kBtnInActive : kBtnActive,
              ),
              Text(
                _text,
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                      color: _text == "" ? kBtnInActive : kBtnActive,
                    ),
              )
            ],
          ),
          SizedBox(height: 20),
          SizedBox(height: 8),
          Wrap(
            children: buildPuzzle(),
          )
        ],
      ),
    );
  }
}
