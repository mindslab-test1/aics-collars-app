import 'package:collars_app_flutter/components/button/border_button.dart';
import 'package:collars_app_flutter/components/deco/blank_box.dart';
import 'package:collars_app_flutter/data/exam/exam_data.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';

class ExamChoice extends StatefulWidget {
  final ExamData data;
  final ValueChanged<int> onChanged;

  const ExamChoice({
    Key key,
    this.data,
    this.onChanged,
  }) : super(key: key);

  @override
  _ExamChoiceState createState() => _ExamChoiceState();
}

class _ExamChoiceState extends State<ExamChoice> {
  List<bool> activeController;

  @override
  void initState() {
    activeController = List.filled(widget.data.subQuestion.length, false);
  }

  Widget buildAnswer() {
    List<Widget> _list = List();
    widget.data.subQuestion.asMap().forEach((index, value) {
      _list.add(
        GestureDetector(
          onTap: () {
            setState(() {
              activeController.fillRange(
                  0, widget.data.subQuestion.length, false);
              activeController[index] = true;
            });
            widget.onChanged(index); // THIS IS ANSWER
          },
          child: BorderButton(
            isActive: activeController[index],
            text: '${alphabet[index]}. $value',
            activeColor: kBtnActive,
            inActiveColor: kBtnInActive,
          ),
        ),
      );
    });
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _list,
    );
  }

  Widget buildQuestion() {
    List<String> questions = widget.data.question.split("<blank>");
    return Text.rich(
      TextSpan(
        style: Theme.of(context).textTheme.subtitle1,
        children: [
          TextSpan(
            text: questions[0],
          ),
          questions.length > 1
              ? WidgetSpan(child: BlankBox())
              : TextSpan(text: ""),
          questions.length > 1
              ? TextSpan(
                  text: questions[1],
                )
              : TextSpan(text: ""),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 36),
          buildQuestion(),
          SizedBox(height: 20),
          Text(
            'Choose one of the following answers :',
            style: Theme.of(context).textTheme.caption.copyWith(fontSize: 10),
          ),
          SizedBox(height: 8),
          buildAnswer(),
        ],
      ),
    );
  }
}
