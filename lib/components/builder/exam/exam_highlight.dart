import 'package:collars_app_flutter/data/exam/exam_data.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';

class ExamHighlight extends StatefulWidget {
  final ExamData data;
  final ValueChanged<List<dynamic>> onChanged;

  const ExamHighlight({
    Key key,
    this.data,
    this.onChanged,
  }) : super(key: key);

  @override
  _ExamHighlightState createState() {
    data.answer.forEach((words) {
      words.forEach((value) => value['is_answer'] = false);
    });
    return _ExamHighlightState(answers: data.answer);
  }
}

class _ExamHighlightState extends State<ExamHighlight> {
  List<dynamic> answers;

  _ExamHighlightState({this.answers});

  List<Widget> _buildConversation() {
    List<Widget> _list = List();
    widget.data.subQuestion.asMap().forEach((idx, v) {
      List<Widget> _row = List();
      _row.add(
        Text(
          "${v['speaker']}: ",
          style: Theme.of(context).textTheme.subtitle1,
        ),
      );
      List<String> _words = v['text'].split(new RegExp(r"\b"));
      answers[idx].asMap().forEach((answerIdx, answer) {
        String word = answer['word'];
        bool isAnswer = answer['is_answer'];
        Widget text = word.contains(' ')
            ? Text(word, style: Theme.of(context).textTheme.subtitle1)
            : GestureDetector(
                onTap: () {
                  setState(() {
                    answer['is_answer'] = !answer['is_answer'];
                  });
                  widget.onChanged(answers);
                },
                child: Text(word,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1
                        .copyWith(color: isAnswer ? kBtnActive : Colors.black)),
              );
        _row.add(text);
      });
      _list.add(Wrap(children: _row));
    });
    return _list;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 60),
          // ...buildSubQuestion(),
          ..._buildConversation()
        ],
      ),
    );
  }
}
