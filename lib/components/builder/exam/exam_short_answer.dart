import 'package:collars_app_flutter/components/deco/blank_box.dart';
import 'package:collars_app_flutter/components/form/custom_text_field.dart';
import 'package:collars_app_flutter/data/exam/exam_data.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';

class ExamShortAnswer extends StatefulWidget {
  final ExamData data;
  final ValueChanged<String> onChanged;

  const ExamShortAnswer({Key key, this.data, this.onChanged}) : super(key: key);

  @override
  _ExamShortAnswerState createState() => _ExamShortAnswerState();
}

class _ExamShortAnswerState extends State<ExamShortAnswer> {
  List<InlineSpan> buildQustionText(List<String> stringList) {
    List<InlineSpan> _list = List();
    stringList.forEach((v) {
      if (v == "<blank>") {
        _list.add(WidgetSpan(child: BlankBox()));
      } else {
        _list.add(
          TextSpan(text: v),
        );
      }
    });

    return _list;
  }

  Widget buildQuestion() {
    List<String> questions = List();
    // questions.add(widget.data.subQuestion[0]);
    if(widget.data.subQuestion[0].contains('<black>')) {
          questions = widget.data.subQuestion[0].splitWithDelim(new RegExp(r"<blank>"));
    }
    else{
      questions.add(widget.data.subQuestion[0]);
    }

    return Text.rich(
      TextSpan(
        style: Theme.of(context).textTheme.subtitle1,
        children: buildQustionText(questions),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 60),
          buildQuestion(),
          SizedBox(height: 8),
          CustomTextField(
            onChanged: (value) {
              widget.onChanged(value);
            },
            activeColor: kBtnActive,
            inactiveColor: kBtnInActive,
            hintText: "이곳을 터치 후 입력해주세요.",
            icon: kNextSvg,
          )
        ],
      ),
    );
  }
}
