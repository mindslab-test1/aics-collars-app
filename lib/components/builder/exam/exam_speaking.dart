import 'dart:async';
import 'dart:io';

import 'package:collars_app_flutter/components/button/circle_button.dart';
import 'package:collars_app_flutter/components/indicator/custom_linear_indicator.dart';
import 'package:collars_app_flutter/data/exam/exam_data.dart';
import 'package:collars_app_flutter/services/exam.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/utils/directory_helper.dart';
import 'package:file/local.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:path_provider/path_provider.dart';

class ExamSpeaking extends StatefulWidget {
  final ExamData data;
  final ValueChanged<String> onChanged;

  const ExamSpeaking({Key key, this.data, this.onChanged}) : super(key: key);

  @override
  _ExamSpeakingState createState() => _ExamSpeakingState();
}

class _ExamSpeakingState extends State<ExamSpeaking>
    with SingleTickerProviderStateMixin {
  DirectoryHelper _directoryHelper = DirectoryHelper();
  LocalFileSystem localFileSystem = LocalFileSystem();
  ExamService _examService = ExamService();
  FlutterAudioRecorder _recorder;
  Recording _current;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  String tempPath;
  String _result;

  AnimationController _controller;
  CurvedAnimation _animation;

  String recordingStatus = AUDIO_STOPPED;
  final recordingTime = 10;

  @override
  void dispose() {
    super.dispose();
    if (_currentStatus != RecordingStatus.Unset &&
        _currentStatus != RecordingStatus.Initialized) _recorder.stop();
    _controller?.dispose();
  }

  @override
  void initState() {
    _init();
    _animationInit();
  }

  _init() async {
    if (await FlutterAudioRecorder.hasPermissions) {
      await _directoryHelper.deleteCacheDir();
      dynamic tempDirectory = await getTemporaryDirectory();
      tempPath = tempDirectory.path + "/request.wav";
      _recorder = FlutterAudioRecorder(tempPath, audioFormat: AudioFormat.WAV,sampleRate: 8000);
      await _recorder.initialized;
      var current = await _recorder.current(channel: 0);
      setState(() {
        _current = current;
        _currentStatus = current.status;
      });
    }
  }

  _animationInit() {
    _controller = new AnimationController(
      duration: Duration(seconds: recordingTime),
      vsync: this,
    );
    _animation = new CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    )..addListener(() {
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _buildConversation() {
      List<Widget> _list = List();
      widget.data.subQuestion.forEach((v) {
        _list.add(Text(
          '${v['speaker']}: ${v['text']}',
          style: Theme.of(context).textTheme.subtitle1,
        ));
      });
      return _list;
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          SizedBox(height: 60),
          ..._buildConversation(),
          SizedBox(height: 48),
          GestureDetector(
            onTap: () async {
              if (recordingStatus == AUDIO_STOPPED) {
                setState(() {
                  recordingStatus = AUDIO_RECORDING;
                });
                _controller.forward(from: 0);
                _recorder.start();
                var recording = await _recorder.current(channel: 0);
                setState(() {
                  _current = recording;
                });
                Timer(Duration(seconds: recordingTime), () async {
                  Recording recordResult = await _recorder.stop();
                  setState(() {
                    recordingStatus = AUDIO_SENDING;
                  });
                  File file = localFileSystem.file(recordResult.path);
                  String response = await _examService.uploadExamAudio(file);
                  localFileSystem.file(recordResult.path).delete();
                  setState(() {
                    _result = response;
                    recordingStatus = AUDIO_STOPPED;
                  });
                  widget.onChanged(_result);
                });
              }
            },
            child: Opacity(
              opacity: recordingStatus != AUDIO_STOPPED ? 0.6 : 1.0,
              child: CircleButton(
                size: 56.0,
                color: Colors.blue,
                child: SvgPicture.asset(
                  kMicSvg,
                  width: 32,
                  height: 32,
                  color: white,
                ),
              ),
            ),
          ),
          SizedBox(height: 32),
          CustomLinearIndicator(
            value: _animation.value,
          ),
        ],
      ),
    );
  }
}
