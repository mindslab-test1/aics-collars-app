import 'package:collars_app_flutter/components/card/carousel_card.dart';
import 'package:collars_app_flutter/data/evereading/evereading.dart';
import 'package:collars_app_flutter/layouts/evereading/intro_evereading.dart';
import 'package:collars_app_flutter/providers/evereading_provider.dart';
import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:provider/provider.dart';

class EvereadingCarousel extends StatelessWidget {
  final double height;

  const EvereadingCarousel({
    Key key,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    EvereadingProvider ep = Provider.of<EvereadingProvider>(context);

    return Container(
      height: height,
      child: LazyLoadScrollView(
        isLoading: ep.isColumnsLoading,
        onEndOfPage: () => ep.loadColumn(),
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          itemCount: ep.columns.length,
          itemBuilder: (context, index) {
            Evereading data = ep.columns[index];
            return GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => IntroEvereading(data: data)),
                );
              },
              child: CarouselCard(
                image: data.links.thumbnail,
              ),
            ); // return Container();
          },
        ),
      ),
    );
  }
}
