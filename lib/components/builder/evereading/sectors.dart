import 'package:collars_app_flutter/components/button/capsule_button.dart';
import 'package:collars_app_flutter/data/hashtag/hash_tag.dart';
import 'package:collars_app_flutter/providers/evereading_provider.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Sectors extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    EvereadingProvider _ep = Provider.of<EvereadingProvider>(context);

    List<Widget> buildSectors() {
      List<HashTag> _sectors = _ep.sectors;
      List<Widget> _list = List();
      _list.add(SizedBox(width: 24));
      _sectors.asMap().forEach((index, sector) {
        int _currentIndex = _ep.currentSector;
        _list.add(
          GestureDetector(
            onTap: () {
              _ep.currentSector = index;
              _ep.clear();
            },
            child: CapsuleButton(
                width: 64,
                isActive: index == _currentIndex ? true : false,
                padding: EdgeInsets.symmetric(vertical: 8),
                activeColor: kBtnActive,
                inActiveColor: kBtnInActive,
                text: sector.tag),
          ),
        );
        _list.add(SizedBox(width: 6));
      });
      return _list;
    }

    return Container(
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: Row(
          children: buildSectors(),
        ),
      ),
    );
  }
}
