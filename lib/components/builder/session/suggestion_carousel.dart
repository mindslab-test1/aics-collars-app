import 'package:collars_app_flutter/layouts/session/intro_session.dart';
import 'package:collars_app_flutter/providers/session_provider.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/image/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SuggestionCarousel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SessionProvider _session = Provider.of<SessionProvider>(context);

    List<Widget> _buildCarousel() {
      List<Widget> _list = List();
      _session.recommand.forEach((session) {
        _list.add(
          GestureDetector(
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => IntroSession(data: session)),
              );
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 6),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 312,
                    height: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      image: DecorationImage(
                        image: session.links.thumbnail != null ? NetworkImage(session.links.thumbnail) : AssetImage(kEmptyImage),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(height: 12),
                  Text(
                    session.data.title,
                    style: Theme.of(context).textTheme.headline2,
                  ),
                  Text(
                    '',
                    // "Suggestion | ${session.learningTime} minutes",
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(color: gray4),
                  )
                ],
              ),
            ),
          ),
        );
      });
      return _list;
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 24),
          child: Text(
            "Collars 추천 세션",
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
        SizedBox(height: 12),
        _session.recommand.length <= 0
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: _buildCarousel(),
                ),
              )
      ],
    );
  }
}
