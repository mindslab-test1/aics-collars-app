import 'package:collars_app_flutter/providers/session_provider.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/image/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MySessionCarousel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);

    List<Widget> _buildCarousel() {
      List<Widget> _list = List();
      _user.data.session.forEach((session) {
        int _time = (session.data.section.video.time / 10).toInt();

        _list.add(
          GestureDetector(
            onTap: () {
              Provider.of<SessionProvider>(context, listen: false).current =
                  session;
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 6),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 312,
                    height: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      image: DecorationImage(
                        image: session.links.thumbnail != null
                            ? NetworkImage(session.links.thumbnail)
                            : AssetImage(kEmptyImage),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(height: 12),
                  Text(
                    session.data.title,
                    style: Theme.of(context).textTheme.headline2,
                  ),
                  Text(
                    "${_time} minutes",
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(color: gray4),
                  )
                ],
              ),
            ),
          ),
        );
      });
      return _list;
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 24),
          child: Text(
            "내가 신청한 세션",
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
        SizedBox(height: 12),
        _user.data.session.length <= 0
            ? Container()
            : SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: _buildCarousel(),
                ),
              )
      ],
    );
  }
}
