import 'package:collars_app_flutter/components/card/carousel_card.dart';
import 'package:collars_app_flutter/data/twoCents/twoCents.dart';
import 'package:collars_app_flutter/layouts/twocents/intro_two_cents.dart';
import 'package:collars_app_flutter/providers/twocents_provider.dart';
import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:provider/provider.dart';

class TwoCentsCarousel extends StatelessWidget {
  final double height;

  const TwoCentsCarousel({
    Key key,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TwoCentsProvider tp = Provider.of<TwoCentsProvider>(context);
    return Container(
      height: height,
      child: LazyLoadScrollView(
        isLoading: tp.isSciprtsLoading,
        onEndOfPage: () => tp.loadScript(),
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          itemCount: tp.scripts.length,
          itemBuilder: (context, index) {
            TwoCents data = tp.scripts[index];
            return GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (_) => IntroTwoCents(data: data)),
                );
              },
              child: CarouselCard(
                image: data.links.thumbnail,
              ),
            );
          },
        ),
      ),
    );
  }
}
