import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';

class InfoError extends StatelessWidget {
  final bool show;
  final String text;

  const InfoError({
    Key key,
    this.show,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return show
        ? Container(
            child: Text(
              text,
              style:
                  Theme.of(context).textTheme.caption.copyWith(color: kError),
            ),
          )
        : Container();
  }
}
