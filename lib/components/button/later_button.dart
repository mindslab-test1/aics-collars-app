import 'package:collars_app_flutter/layouts/main/main_screen.dart';
import 'package:collars_app_flutter/layouts/wrap_screen.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';

class LaterButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, MainScreen.id);
        },
        child: Text(
          '다음에 할게요',
          style: Theme.of(context)
              .textTheme
              .caption
              .copyWith(color: kInfoColor),
        ),
      ),
    );
  }
}
