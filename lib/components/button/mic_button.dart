import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class MicButton extends StatefulWidget {
  final bool isLeft;

  const MicButton({Key key, this.isLeft}) : super(key: key);

  @override
  _MicButtonState createState() => _MicButtonState();
}

class _MicButtonState extends State<MicButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(9.0),
      height: 42,
      decoration: BoxDecoration(
          color: blue4,
          borderRadius: BorderRadius.only(
            topLeft: widget.isLeft?Radius.zero:Radius.circular(8.0),
            topRight: widget.isLeft?Radius.circular(8.0):Radius.zero,
            bottomLeft: widget.isLeft?Radius.zero:Radius.circular(8.0),
            bottomRight: widget.isLeft?Radius.circular(8.0):Radius.zero,
          )
      ),
      child: SvgPicture.asset('assets/icon/mic_2.svg'),
    );
  }
}
