import 'package:flutter/material.dart';

class CapsuleButton extends StatelessWidget {
  final double width;
  final bool isActive;
  final EdgeInsets padding;
  final Color activeColor;
  final Color inActiveColor;
  final String text;

  const CapsuleButton({
    Key key,
    this.isActive,
    this.padding,
    this.activeColor,
    this.inActiveColor,
    this.text,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      padding: padding,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(
          color: isActive ? activeColor : inActiveColor,
        ),
      ),
      child: Center(
        child: Text(
          text,
          style: Theme.of(context).textTheme.subtitle1.copyWith(
                fontSize: 12,
                color: isActive ? activeColor : inActiveColor,
              ),
        ),
      ),
    );
  }
}
