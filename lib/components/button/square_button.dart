import 'package:flutter/material.dart';

class SquareButton extends StatelessWidget {
  final Function onTap;
  final double size;
  final Widget child;
  final Color bg;

  const SquareButton({
    Key key,
    this.size,
    this.bg,
    this.onTap,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) onTap();
      },
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          color: bg,
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
        ),
        child: child,
      ),
    );
  }
}
