import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class CircleButton extends StatelessWidget {
  final Function onPressed;
  final double size;
  final Color color;
  final Widget child;

  const CircleButton({
    Key key,
    this.onPressed,
    @required this.size,
    this.color = Colors.green,
    this.child,
  }) : assert(size != null);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: color,
        ),
        child: child,
      ),
    );
  }
}
