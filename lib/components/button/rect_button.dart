import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RectButton extends StatelessWidget {
  final Function onTap;
  final double width;
  final double height;
  final Widget child;
  final Color bg;
  final Color color;

  const RectButton({
    Key key,
    this.bg,
    this.color,
    this.onTap,
    this.width,
    this.height,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) onTap();
      },
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: bg,
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
        ),
        child: child,
      ),
    );
  }
}
