import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HintButton extends StatefulWidget {
  final Function onTap;
  final bool isLeft;
  final String imgPath;
  final Color color;

  const HintButton({Key key, this.onTap, this.isLeft, this.imgPath,this.color}) : super(key: key);
  @override
  _HintButtonState createState() => _HintButtonState();
}

class _HintButtonState extends State<HintButton> {
  // sound hint play
  // bool isPlayed = false;
  // AudioPlayer audioPlayer = AudioPlayer();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        padding: const EdgeInsets.all(9.0),
        height: 42,
        decoration: BoxDecoration(
            color: widget.color,
            borderRadius: BorderRadius.only(
                topLeft: widget.isLeft?Radius.circular(1.0):Radius.zero,
                topRight: widget.isLeft?Radius.zero:Radius.circular(1.0),
                bottomLeft: widget.isLeft?Radius.circular(8.0):Radius.zero,
                bottomRight: widget.isLeft?Radius.zero:Radius.circular(8.0)
            )
        ),
        child: SvgPicture.asset(widget.imgPath),
      ),
    );
  }
}
