import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/text/default.dart';
import 'package:flutter/material.dart';

class ChatbotNoButton extends StatefulWidget {
  @override
  _ChatbotNoButtonState createState() => _ChatbotNoButtonState();
}

class _ChatbotNoButtonState extends State<ChatbotNoButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
      },
      child: Container(
        margin: const EdgeInsets.only(top: 4),
        padding: const EdgeInsets.symmetric(vertical: 11, horizontal: 12),
        height: 42,
        decoration: BoxDecoration(
            color: blue4,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(8.0),
              bottomRight: Radius.circular(8.0)
            )
        ),
        child: Text('No',
          style: kAiMsgNameStyle.copyWith(fontWeight: FontWeight.w500, color: Colors.white),),
      ),
    );
  }
}
