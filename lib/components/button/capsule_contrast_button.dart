import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:flutter/material.dart';

class CapsuleContrastButton extends StatefulWidget {
  final bool isActive;
  final String type;
  final Function onTap;
  final double width;
  final double height;
  final EdgeInsets padding;
  final Color primaryColor;
  final Color secondaryColor;
  final String text;

  const CapsuleContrastButton({
    this.isActive = false,
    @required this.type,
    this.onTap,
    this.width,
    this.height,
    this.padding,
    this.primaryColor,
    this.secondaryColor,
    this.text,
  }) : assert(type != null);

  @override
  _CapsuleContrastButtonState createState() =>
      _CapsuleContrastButtonState(isActive: isActive);
}

class _CapsuleContrastButtonState extends State<CapsuleContrastButton> {
  bool isActive;

  _CapsuleContrastButtonState({this.isActive = false});

  Widget buildButton() {
    return Container(
      width: widget.width,
      height: widget.height,
      padding: widget.padding,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(
          color: isActive ? Colors.transparent : widget.primaryColor,
        ),
        color: isActive ? widget.primaryColor : Colors.transparent,
      ),
      child: Center(
        child: Text(
          widget.text,
          style: Theme.of(context).textTheme.subtitle1.copyWith(
                color: isActive ? white : widget.primaryColor,
              ),
        ),
      ),
    );
  }

  Widget buildActivateButton() {
    return Container(
      width: widget.width,
      height: widget.height,
      padding: widget.padding,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(
          color: Colors.transparent,
        ),
        color: widget.primaryColor,
      ),
      child: Center(
        child: Text(
          widget.text,
          style: Theme.of(context).textTheme.subtitle1.copyWith(
                color: white,
              ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    switch (widget.type) {
      case ONCE:
        return GestureDetector(
          onTapDown: (TapDownDetails details) {
            setState(() {
              isActive = true;
            });
            if (widget.onTap != null) widget.onTap();
          },
          onTapUp: (TapUpDetails details) {
            setState(() {
              isActive = false;
            });
          },
          onTapCancel: () {
            setState(() {
              isActive = false;
            });
          },
          child: buildButton(),
        );
      case FIXED:
        return GestureDetector(
          onTap: () {
            if (widget.onTap != null) widget.onTap();
          },
          child: buildButton(),
        );
      case ACTIVATE:
        return GestureDetector(
          onTap: () {
            if (widget.isActive) {
              if (widget.onTap != null) widget.onTap();
            }
          },
          child: Opacity(
            child: buildActivateButton(),
            opacity: widget.isActive ? 1.0 : 0.6,
          ),
        );
      default:
        return Container();
    }
  }
}
