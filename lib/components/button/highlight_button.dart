import 'package:flutter/material.dart';

class HighlightButton extends StatefulWidget {
  final bool isActive;
  final int id;
  final String word;
  final ValueChanged<List<dynamic>> onChanged;
  final Color activeColor;

  const HighlightButton({
    Key key,
    this.isActive,
    this.word,
    this.onChanged,
    this.activeColor, this.id,
  }) : super(key: key);

  @override
  _HighlightButtonState createState() => _HighlightButtonState();
}

class _HighlightButtonState extends State<HighlightButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onChanged([widget.id, widget.word]);
      },
      child: Container(
        padding: EdgeInsets.all(3),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: widget.isActive ? widget.activeColor : Colors.transparent,
        ),
        child: Text(
          widget.word,
          style: Theme.of(context).textTheme.subtitle1.copyWith(
                color: widget.isActive ? Colors.white : Colors.black,
              ),
        ),
      ),
    );
  }
}
