import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/text/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ChatbotYesButton extends StatefulWidget {
  @override
  _ChatbotYesButtonState createState() => _ChatbotYesButtonState();
}

class _ChatbotYesButtonState extends State<ChatbotYesButton> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        setState(() {
          Navigator.pop(context);
        });
      },
      child: Container(
        margin: const EdgeInsets.only(top: 4.0, right: 1.0),
        padding: const EdgeInsets.only(left: 11, top: 10, right: 11, bottom: 11),
        height: 42,
        decoration: BoxDecoration(
            color: blue4,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(8.0)
            )
        ),
        child: Text('Yes',
        style: kAiMsgNameStyle.copyWith(fontWeight: FontWeight.w500, color: Colors.white),),
      ),
    );
  }
}
