import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CheckBoxButton extends StatelessWidget {
  final bool isActive;
  final EdgeInsets padding;
  final String text;
  final Color color;

  CheckBoxButton({
    this.isActive,
    this.padding,
    this.text,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      child: Row(
        children: [
          Expanded(
            child: Text(
              '$text',
              style: Theme.of(context).textTheme.bodyText2.copyWith(color: color),
            ),
          ),
          isActive ? SvgPicture.asset(KCheckSvg, color: color) : Container(),
        ],
      ),
    );
  }
}
