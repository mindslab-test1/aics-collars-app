import 'package:flutter/material.dart';

class BorderButton extends StatelessWidget {
  final bool isActive;
  final Color activeColor;
  final Color inActiveColor;
  final String text;

  const BorderButton({
    Key key,
    this.isActive = false,
    this.activeColor,
    this.inActiveColor,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 14, horizontal: 16),
      margin: EdgeInsets.only(bottom: 8.0, right: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        border: Border.all(color: isActive ? activeColor : inActiveColor),
      ),
      child: Text(
        text,
        style: Theme.of(context).textTheme.bodyText2.copyWith(
              color: isActive ? activeColor : Colors.black,
            ),
      ),
    );
  }
}
