import 'package:collars_app_flutter/components/container/circle_container.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomRadioButton extends StatefulWidget {
  final ValueChanged<bool> onChanged;

  const CustomRadioButton({
    Key key,
    this.onChanged,
  }) : super(key: key);

  @override
  _CustomRadioButtonState createState() => _CustomRadioButtonState();
}

class _CustomRadioButtonState extends State<CustomRadioButton> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          isChecked = !isChecked;
        });
        widget.onChanged(isChecked);
      },
      child: CircleContainer(
        padding: EdgeInsets.all(5),
        bgColor: isChecked ? kActive : kInActive2,
        borderColor: kActive,
        child: isChecked
            ? SvgPicture.asset(
                KCheckSvg,
                color: white,
              )
            : Container(),
      ),
    );
  }
}
