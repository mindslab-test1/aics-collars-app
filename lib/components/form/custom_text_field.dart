import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomTextField extends StatefulWidget {
  final ValueChanged<String> onChanged;
  final Color activeColor;
  final Color inactiveColor;
  final String icon;
  final String hintText;
  final bool autoFocus;
  final bool secure;

  const CustomTextField({
    Key key,
    this.onChanged,
    this.activeColor = Colors.blue,
    this.inactiveColor = Colors.grey,
    this.icon,
    this.hintText,
    this.autoFocus = false,
    this.secure = false
  }) : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool _isActive = false;

  @override
  Widget build(BuildContext context) {
    return TextField(
      autofocus: widget.autoFocus,
      obscureText: widget.secure,
      onChanged: (value) {
        if (value == null || value == "")
          setState(() {
            _isActive = false;
          });
        else
          setState(() {
            _isActive = true;
          });

        widget.onChanged(value);
      },
      style: TextStyle(
        decoration: TextDecoration.none,
        color: widget.activeColor,
      ),
      decoration: InputDecoration(
        border: InputBorder.none,
        icon: SvgPicture.asset(
          widget.icon,
          color: _isActive ? widget.activeColor : widget.inactiveColor,
        ),
        hintText: widget.hintText,
        hintStyle: TextStyle(color: widget.inactiveColor),
      ),
    );
  }
}
