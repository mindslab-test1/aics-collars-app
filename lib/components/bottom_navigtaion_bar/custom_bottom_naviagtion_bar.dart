import 'package:collars_app_flutter/routes/route_form.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  final double height;
  final int currentPage;
  final Function jumpToPage;
  final List<RouteForm> children;

  const CustomBottomNavigationBar({
    Key key,
    this.children,
    this.height,
    this.jumpToPage,
    this.currentPage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screen = MediaQuery.of(context).size;

    List<Widget> _buildChildren() {
      List<Widget> _list = List();
      children.asMap().forEach((index, child) {
        _list.add(
          Expanded(
            child: InkWell(
              onTap: () {
                jumpToPage(index);
              },
              child: Opacity(
                opacity: currentPage == index ? 1.0 : 0.5,
                child: Center(
                  child: SvgPicture.asset(
                    child.icon,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        );
      });
      return _list;
    }

    return Container(
      height: height,
      width: screen.width,
      padding: kPaddingHorizontal,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(kBnbRadius),
          topRight: Radius.circular(kBnbRadius),
        ),
        color: Colors.black,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: _buildChildren(),
      ),
    );
  }
}
