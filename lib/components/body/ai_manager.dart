import 'package:flutter/material.dart';

class AiManager extends StatelessWidget {
  final String image;
  final double imageSize;
  final String text;
  final Widget child;

  const AiManager({
    Key key,
    this.imageSize,
    this.image,
    this.text = "",
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: imageSize,
          height: imageSize,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(image),
            ),
          ),
        ),
        SizedBox(height: 16),
        Text(
          text,
          style: Theme.of(context).textTheme.headline2,
        ),
        SizedBox(height: 48),
        child != null? child: Container()
      ],
    );
  }
}
