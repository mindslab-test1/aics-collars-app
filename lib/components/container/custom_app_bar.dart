import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomAppBar extends StatelessWidget {
  final String title;
  final Color titleColor;
  final Widget action;

  const CustomAppBar({
    Key key,
    this.title,
    this.titleColor = Colors.black,
    this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: AppBar().preferredSize.height,
      padding: kPaddingHorizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 56,
            height: 56,
            child: Center(
              child: Navigator.canPop(context)
                  ? GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: SvgPicture.asset(
                        kArrowBack,
                        color: Colors.black,
                      ),
                    )
                  : null,
            ),
          ),
          Expanded(
            child: Center(
              child: Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(color: titleColor),
              ),
            ),
          ),
          Container(
            width: 56,
            height: 56,
            child: Center(
              child: action,
            ),
          )
        ],
      ),
    );
  }
}
