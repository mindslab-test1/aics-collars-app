import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/text/default.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

class TopMsgContainer extends StatefulWidget {
  final bool isAppeared;
  final Function onTap;

  const TopMsgContainer({Key key, this.isAppeared, this.onTap}) : super(key: key);
  @override
  _TopMsgContainerState createState() => _TopMsgContainerState();
}

class _TopMsgContainerState extends State<TopMsgContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 36,
        color: kTopMsgBg,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 48),
                child: Text('대화창을 눌러 음성으로 들어보세요.',
                    style: kAiMsgStyle),
              ),
              GestureDetector(
                  onTap: widget.onTap,
                  child: Container(
                    width: 24,
                    height: 24,
                    child: Center(
                      child : SvgPicture.asset('assets/icon/close.svg'),
                    ),
                  )
              )
            ],
          ),
        )
    );
  }
}
