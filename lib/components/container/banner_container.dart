import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';

class BannerContainer extends StatelessWidget {
  final double height;
  final Widget child;

  const BannerContainer({
    Key key,
    this.height,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      decoration: BoxDecoration(
        gradient: kDefaultGradient,
      ),
      child: child,
    );
  }
}
