import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';

class RectContainer extends StatelessWidget {
  final double width;
  final double height;
  final EdgeInsets padding;
  final Color color;
  final Widget child;

  const RectContainer({
    Key key,
    this.width,
    this.height,
    this.padding = kPaddingAll,
    this.color = kInActive,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      padding: padding,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        border: Border.all(color: color),
      ),
      child: child,
    );
  }
}
