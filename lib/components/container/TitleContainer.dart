import 'package:flutter/material.dart';

class TitleContainer extends StatelessWidget {
  final String text;
  final Widget child;

  const TitleContainer({
    Key key,
    this.text = "",
    @required this.child,
  })  : assert(child != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Text(
            text,
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
        child
      ],
    );
  }
}
