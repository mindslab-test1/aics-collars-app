import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';

class CircleContainer extends StatelessWidget {
  final EdgeInsets padding;
  final double size;
  final Color bgColor;
  final Color borderColor;
  final Widget child;

  const CircleContainer({
    Key key,
    this.padding,
    this.size = 24.0,
    this.bgColor = Colors.white,
    this.borderColor = Colors.transparent,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      padding: padding,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(
          color: borderColor,
        ),
        color: bgColor,
      ),
      child: child,
    );
  }
}
