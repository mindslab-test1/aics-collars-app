import 'package:flutter/material.dart';

class CustomSafeArea extends StatelessWidget {
  AppBar appBar = new AppBar();


  @override
  Widget build(BuildContext context) {
    return SizedBox(height: MediaQuery.of(context).padding.top);
  }
}
