import 'dart:io';

import 'package:collars_app_flutter/data/chatbot_result.dart';
import 'package:collars_app_flutter/providers/twocents_provider.dart';
import 'package:collars_app_flutter/services/ai_practice.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/text/default.dart';
import 'package:collars_app_flutter/utils/directory_helper.dart';
import 'package:file/local.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class MicRecordContainer extends StatefulWidget {
  final Function chatbotMsg;
  final Function callChatbot;
  final bool isTwoCent;
  final Widget micButton;
  final Widget hintButton;
  final String direction;
  final Function recordFinish;
  final double screenWidth;

  const MicRecordContainer({
    Key key,
    this.chatbotMsg,
    this.callChatbot,
    this.isTwoCent,
    this.micButton,
    this.hintButton,
    this.direction,
    this.recordFinish,
    this.screenWidth,
  }) : super(key: key);

  @override
  _MicRecordContainerState createState() => _MicRecordContainerState();
}

class _MicRecordContainerState extends State<MicRecordContainer>
    with SingleTickerProviderStateMixin {
  //animation
  Animation _animation;
  AnimationController _animationController;
  Animation _curve;

  // record
  DirectoryHelper _directoryHelper = DirectoryHelper();
  LocalFileSystem localFileSystem = LocalFileSystem();
  FlutterAudioRecorder _recorder;
  ChatbotService _chatbotService = ChatbotService();
  String tempPath;
  String recordingStatus = AUDIO_STOPPED;
  String _result;
  final recordingTime = 7;
  bool recording = false;
  ChatbotResult chatbotRes;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  Recording _current;
  bool isStarted = false;

  TwoCentsProvider twoCentsProvider;

  @override
  void initState() {
    _result = widget.isTwoCent ? "위 지문을 읽어주세요." : "발화해주세요.";
    _micInit();
    _animationInit(widget.screenWidth);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    if (_currentStatus != RecordingStatus.Unset &&
        _currentStatus != RecordingStatus.Initialized) _recorder.stop();
    _animationController?.dispose();
  }

  // mic
  _micInit() async {
    if (await FlutterAudioRecorder.hasPermissions) {
      await _directoryHelper.deleteCacheDir();
      dynamic tempDirectory = await getTemporaryDirectory();
      tempPath = tempDirectory.path + "/request.wav";
      _recorder = FlutterAudioRecorder(tempPath,
          audioFormat: AudioFormat.WAV, sampleRate: 8000);
      await _recorder.initialized;
      Recording current = await _recorder.current(channel: 0);
      setState(() {
        _currentStatus = current.status;
      });
    }
  }

  _startMic() async {
    if (recordingStatus == AUDIO_STOPPED) {
      setState(() {
        recordingStatus = AUDIO_RECORDING;
      });
      _recorder.start();
      Recording recording = await _recorder.current(channel: 0);
      setState(() {
        _current = recording;
      });
    }
  }

  _stopMic() async {
    Recording recordRes = await _recorder.stop();
    // setState(() {
    //   recordingStatus = AUDIO_SENDING;
    // });
    File file = localFileSystem.file(recordRes.path);
    if (widget.isTwoCent) {
      widget.recordFinish(
        recordRes,
        file,
      );
      twoCentsProvider.isRecordBtnActive = false;
    } else {
      String response = await _chatbotService.uploadUserAudio(file);
      localFileSystem.file(recordRes.path).delete();
      setState(() {
        if (response == '') {
          response = '. . .';
        }
        _result = response;
        // recordingStatus = AUDIO_STOPPED;
        recording = true;
      });
      widget.callChatbot('test_ha', _result);
    }
  }

  // animation
  _animationInit(double width) {
    double size = widget.isTwoCent ? 0.481 : 0.35;
    width = width * size;
    _animationController = AnimationController(
        vsync: this, duration: Duration(seconds: recordingTime));
    _curve =
        CurvedAnimation(parent: _animationController, curve: Curves.easeOut);
    _animation = Tween(begin: 0.0, end: width).animate(_curve)
      ..addListener(() {
        setState(() {});
      });
  }

  // usr msg : 발화 후 발화내용이 text로 필요한경우(ai practice)
  Widget afterTextMsg() {
    return Container(
      constraints: BoxConstraints(maxWidth: 240),
      padding: const EdgeInsets.all(12.0),
      decoration: BoxDecoration(
          color: kTopMsgBg,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.0),
            bottomLeft: Radius.circular(8.0),
            bottomRight: Radius.circular(8.0),
          )),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            _result,
            style: kAiMsgNameStyle.copyWith(fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }

  // user msg : 발화내용이 text로 필요하지 않은 경우(twocent)
  Widget afterMsg(Widget firstMenu, Widget secondMenu) {
    return Container(
      child: Row(
        children: [
          if (widget.direction == 'right') Flexible(child: Container()),
          firstMenu,
          SizedBox(
            width: 1,
          ),
          secondMenu
        ],
      ),
    );
  }

  Widget beforeMsg() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 12.0),
          height: 44,
          decoration: BoxDecoration(
              color: kUserRecordBg,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                bottomLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
              )),
        ),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 12.0),
          width: _animation.value,
          height: 44,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xff5262f2), Color(0xff896cfd)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                bottomLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
              )),
        ),
        Container(
          padding: EdgeInsets.all(12.0),
          child: GestureDetector(
            onTap: () async {
              _animationController.forward();
              setState(() {
                isStarted = true;
              });
              _startMic();
              await Future.delayed(Duration(seconds: recordingTime), () {
                // 녹음 끝
                _stopMic();
              });
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                isStarted
                    ? SvgPicture.asset('assets/icon/mic_2.svg')
                    : SvgPicture.asset('assets/icon/mic_blue.svg'),
                Text(
                  _result,
                  style: kAiMsgNameStyle.copyWith(
                      color: isStarted ? Colors.white : blue4,
                      fontWeight: FontWeight.w500),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    twoCentsProvider = Provider.of<TwoCentsProvider>(context, listen: false);
    return recording == false
        ? beforeMsg()
        : widget.isTwoCent
            ? afterMsg(widget.hintButton, widget.micButton)
            : afterTextMsg();
  }
}
