import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';

class TextFieldContainer extends StatelessWidget {
  final isActive;
  final Color activeColor;
  final Color inActiveColor;
  final child;

  const TextFieldContainer({
    this.isActive = false,
    this.activeColor = Colors.blue,
    this.inActiveColor = Colors.grey,
    @required this.child,
  }) : assert(child != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kPaddginTextField,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(
          color: isActive ? activeColor : inActiveColor,
        ),
      ),
      child: child,
    );
  }
}
