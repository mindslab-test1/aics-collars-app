import 'package:collars_app_flutter/routes/route_form.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';

class CustomTabBar extends StatefulWidget {
  final List<RouteForm> children;
  final ValueChanged<int> onChanged;
  final int initialIndex;

  const CustomTabBar({
    Key key,
    this.children,
    this.onChanged,
    this.initialIndex = 0,
  }) : super(key: key);

  @override
  _CustomTabBarState createState() => _CustomTabBarState(current: initialIndex);
}

class _CustomTabBarState extends State<CustomTabBar> {
  int current;

  _CustomTabBarState({this.current});

  @override
  Widget build(BuildContext context) {
    List<Widget> _buildChildren() {
      List<Widget> _list = List();
      widget.children.asMap().forEach((index, value) {
        _list.add(
          Expanded(
            child: InkWell(
              onTap: () {
                setState(() {
                  current = index;
                });
                widget.onChanged(current);
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1.5,
                          color: index == current
                              ? kBtnActive
                              : Colors.transparent)),
                ),
                child: Center(
                  child: Text(
                    value.name,
                    style: Theme.of(context).textTheme.bodyText2.copyWith(
                          fontWeight: FontWeight.bold,
                          color: index == current ? black : kBtnInActive,
                        ),
                  ),
                ),
              ),
            ),
          ),
        );
      });
      return _list;
    }

    return Container(
      width: double.infinity,
      height: 45,
      padding: kPaddingHorizontal,
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(width: 1, color: white9)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildChildren(),
      ),
    );
  }
}
