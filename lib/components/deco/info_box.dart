import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';

class InfoBox extends StatelessWidget {
  final String title;
  final String info;
  final Function onTap;

  const InfoBox({
    Key key,
    this.title,
    this.info,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 24, right: 24, top: 4, bottom: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(
          color: kBtnInActive,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .copyWith(fontSize: 10, color: kBtnInActive),
              ),
              Text(info, style: Theme.of(context).textTheme.bodyText1),
            ],
          ),
          GestureDetector(
            onTap: (){
              if(onTap != null) onTap();
            },
            child: Text(
              "수정",
              style: Theme.of(context)
                  .textTheme
                  .bodyText2
                  .copyWith(color: kBtnInActive),
            ),
          )
        ],
      ),
    );
  }
}
