import 'package:collars_app_flutter/components/card/section_card.dart';
import 'package:collars_app_flutter/data/user/user_exam_data.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:flutter/material.dart';

class LinearChartBox extends StatelessWidget {
  final List<UserExamData> data;

  const LinearChartBox({
    Key key,
    this.data,
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    int choice_count= 0;
    int puzzle_count = 0;
    int speaking_count = 0;
    int short_answer_count = 0;
    int highlight_count = 0;

    double choice = 0;
    double puzzle = 0;
    double speaking = 0;
    double short_answer = 0;
    double highlight = 0;
    data.forEach((value) {
      switch(value.type.toUpperCase()){
        case CHOICE:
          choice_count++;
          choice += value.score;
          break;
        case PUZZLE:
          puzzle_count++;
          puzzle += value.score;
          break;
        case SPEAKING:
          speaking_count++;
          speaking += value.score;
          break;
        case SHORT_ANSWER:
          short_answer_count++;
          short_answer += value.score;
          break;
        case HIGHLIGHT:
          highlight_count++;
          highlight += value.score;
          break;
        default:
          break;
      }
    });
    if(choice_count>0) choice /= choice_count;
    if(puzzle_count>0) puzzle /= puzzle_count;
    if(speaking_count>0) speaking /= speaking_count;
    if(short_answer_count>0) short_answer /= short_answer_count;
    if(highlight_count>0) highlight /= highlight_count;

    data.forEach((value) {
      switch(value.type.toUpperCase()){
        case CHOICE:
          choice += value.score;
          break;
        case PUZZLE:
          puzzle += value.score;
          break;
        case SPEAKING:
          speaking += value.score;
          break;
        case SHORT_ANSWER:
          short_answer += value.score;
          break;
        case HIGHLIGHT:
          highlight += value.score;
          break;
        default:
          break;
      }
    });

    String getGrade(double score) {
      if (score >= 70)
        return 'A';
      else if (score >= 40)
        return 'B';
      else
        return 'C';
    }

    Widget chart({String title, String grade, double score}) {
      return Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2
                    .copyWith(fontWeight: FontWeight.bold, fontSize: 14),
              ),
              Text(
                grade,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2
                    .copyWith(fontWeight: FontWeight.bold, fontSize: 20),
              )
            ],
          ),
          SizedBox(height: 12),
          Container(
            height: 4,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(16)),
              child: LinearProgressIndicator(
                value: score / 100,
                valueColor: AlwaysStoppedAnimation<Color>(kBtnActive),
                backgroundColor: Color.fromRGBO(0, 0, 0, 0.1),
              ),
            ),
          ),
        ],
      );
    }

    List<Widget> buildChart() {
      List<Widget> _list = List();
      _list.add(chart(
        title: "맥락",
        grade: getGrade(speaking),
        score: speaking,
      ));
      _list.add(SizedBox(height: 28));
      _list.add(chart(
          title: "톤앤매너",
          grade: getGrade(short_answer),
          score: short_answer));
      _list.add(SizedBox(height: 28));
      _list.add(chart(
          title: "문화적 지식",
          grade: getGrade(highlight),
          score: highlight));
      _list.add(SizedBox(height: 28));
      _list.add(chart(
        title: "문법",
        grade: getGrade(puzzle),
        score: puzzle,
      ));
      _list.add(SizedBox(height: 28));
      _list.add(chart(
        title: "어휘",
        grade: getGrade(choice),
        score: choice,
      ));
      return _list;
    }

    return SectionCard(
      borderColor: white9,
      child: Column(
        children: buildChart(),
      ),
    );
  }
}
