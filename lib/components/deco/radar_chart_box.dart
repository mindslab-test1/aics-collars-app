import 'package:collars_app_flutter/components/deco/chart/radar_chart.dart';
import 'package:collars_app_flutter/data/user/user_exam_data.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';

class RadarChartBox extends StatelessWidget {
  final double size;
  final List<UserExamData> data;

  const RadarChartBox({
    Key key,
    this.data,
    this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const numberOfFeatures = 5;

    int choice_count= 0;
    int puzzle_count = 0;
    int speaking_count = 0;
    int short_answer_count = 0;
    int highlight_count = 0;

    double choice = 0;
    double puzzle = 0;
    double speaking = 0;
    double short_answer = 0;
    double highlight = 0;
    data.forEach((value) {
      switch(value.type.toUpperCase()){
        case CHOICE:
          choice_count++;
          choice += value.score;
          break;
        case PUZZLE:
          puzzle_count++;
          puzzle += value.score;
          break;
        case SPEAKING:
          speaking_count++;
          speaking += value.score;
          break;
        case SHORT_ANSWER:
          short_answer_count++;
          short_answer += value.score;
          break;
        case HIGHLIGHT:
          highlight_count++;
          highlight += value.score;
          break;
        default:
          break;
      }
    });
    if(choice_count>0) choice /= choice_count;
    if(puzzle_count>0) puzzle /= puzzle_count;
    if(speaking_count>0) speaking /= speaking_count;
    if(short_answer_count>0) short_answer /= short_answer_count;
    if(highlight_count>0) highlight /= highlight_count;

    List<RadarChartData> chartData = [
      RadarChartData(field: "맥락", score: choice/100),
      RadarChartData(field: "톤앤매너", score: puzzle/100),
      RadarChartData(field: "문화적", score: speaking/100),
      RadarChartData(field: "문법", score: short_answer/100),
      RadarChartData(field: "어휘", score: highlight/100)
    ];

    return Container(
      width: size,
      height: size,
      child: RadarChart(
        data: chartData,
        textStyle: Theme.of(context).textTheme.subtitle1.copyWith(fontSize: 14),
        vertexSize: 3,
        innerBorderSize: 2.0,
        outerBorderSize: 2.0,
        graphBorderSize: 2.0,
        backgroundColor: kRadarBg,
        vertexColor: kRadarVertex,
        outerColor: kRadarCircle,
        innerColor: kRadarCircle,
        graphBackgroundColor: kRadarGraphBg,
        graphBorderColor: kRadarGraphBorder,
      ),
    );
  }
}
