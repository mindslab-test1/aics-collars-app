import 'package:flutter/material.dart';

class VerticalDivider extends StatelessWidget {
  final double indent;
  final double thickness;
  final Color color;

  const VerticalDivider({
    Key key,
    this.indent = 0,
    this.thickness = 1,
    this.color = Colors.black,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: indent),
      width: thickness,
      decoration: BoxDecoration(color: color),
    );
  }
}
