import 'package:flutter/material.dart';

class HorizontalDivider extends StatelessWidget {
  final double indent;
  final double thickness;
  final Color color;

  const HorizontalDivider({
    Key key,
    this.indent = 0,
    this.thickness = 1,
    this.color = Colors.black,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: indent),
      height: thickness,
      decoration: BoxDecoration(color: color),
    );
  }
}
