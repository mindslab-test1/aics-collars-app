import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IconBox extends StatelessWidget {
  final String icon;
  final String text;
  final Color color;

  const IconBox({
    Key key,
    this.icon,
    this.text,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(color: color, width: 1),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            icon,
            width: 12,
            height: 12,
            color: color,
          ),
          SizedBox(width: 4),
          Text(
            text,
            style: Theme.of(context).textTheme.subtitle1.copyWith(
                  fontSize: 10,
                  color: color,
                ),
          ),
        ],
      ),
    );
  }
}
