import 'package:collars_app_flutter/components/card/section_card.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SuggestionBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final User _user = Provider.of<User>(context);

    String _getSuggestion() {
      String result = "";
      _user.data.survey.forEach((survey) {
        if (survey.questionId == 1) {
          switch (survey.answerId) {
            case 2:
              result =
                  "기획 담당자는 조직 단위에서는 회사가 처한 경영 환경, 팀 단위에서는 개별 사업이나 프로젝트에 대해 분석하고, 분석 결과를 근거로 회사의 나아갈 방향을 제시하여야 합니다. 통상적으로 이러한 업무는 다양한 부서에서 전달받는 자료와 의견을 조율하고 합의점을 찾는 과정을 수반하기 때문에 효과적인 업무적 의사소통 능력 및 문서 작성 능력이 대단히 중요합니다.\n따라서, 기획 직무를 담당하시는 학습자님께는 이와 같은 소통 역량을 강화할 수 있는 세션으로 제안 드리겠습니다.";
              break;
            case 3:
              result =
                  "마케팅 담당자들에게 가장 중요한 업무 역량은 의사소통 능력일거예요. 어떤 메시지를 어떻게 전달할 것인가에 대한 전략을 세우는 것이, 마케팅에서 가장 핵심적일 수 밖에 없을 텐데요! 마케팅 업무는 비주얼 커뮤니케이션 요소도 중요하지만, 고객을 움직이게 만드는 건 결국 날카로운 메시지이죠.\n그런 차원에서 마케팅 담당자들은 말과 글을 모두 잘 다룰 수 있어야 합니다. 업무적 소통을 위한 회화나 텍스트 기반 소통은 물론, 발표 역량도 중요하죠!\n따라서, 마케팅 직무를 담당하시는 학습자님은 이와 같은 업무 소통 역량을 강화할 수 있는 세션으로 제안 드리겠습니다 !";
              break;
            case 4:
              result =
                  "세일즈 직무 담당자는 회사를 대표해서 국내외 기업들과 협상을 진행하고 협의를 이끌어야 하는 상황을 다수 마주하게 됩니다. 이러한 업무 상황에서는 상대측 현지 국가에 대한 문화적 이해도를 갖추는 한편, 종합적 상황 이해와 분석을 기반으로 소속 회사가 원하는 바를 이끌어내기 위한 능숙한 소통 역량이 필요합니다. 다른 직무에 비해 업무적 회화 역량은 물론, 상대측과 신뢰 기반의 관계를 공고히 구축하기 위한 비업무적 회화 역량도 갖추는 것이 필수적이죠!\n따라서, 세일즈 직무를 담당하시는 학습자님은 이와 같은 소통 역량을 강화할 수 있는 세션으로 제안 드리겠습니다.";
              break;
            case 5:
              result =
                  "인사 직무를 원활히 수행하기 위해서는 조직 내 다양한 의견을 수용하는 친화적 소통 능력이 필수적입니다. 즉, 업무와 관련하여 대내외적 고객의 요구사항을 잘 이해하고, 결과적으로 그들의 요구가 적절히 충족될 수 있도록 배려하며 소통하는 것이 중요합니다. 빠른 시대적 변화에 따라 인사 전문가는, 소속 기업이 추진하는 사업을 포함하여 연관된 다양한 산업 분야와 트렌드에 대해 이해도를 갖추고 기업의 전략적 판단에 리더쉽을 발휘하는 직무로서 진화하고 있습니다.\n따라서, 인사 직무를 담당하시는 학습자님은 이와 같은 소통 역량을 강화할 수 있는 세션으로 제안 드리겠습니다 !";
              break;
            case 6:
              result =
                  "업종을 불문하고 R&D 부문에서 종사하시는 분들은,  타 직무의 전문가님들에 비해 영어 소통 역량의 중요성이 크지는 않습니다.\n단, R&D 직무 담당자들은 상대적으로 문서 작성 역량과 업무 관련 회화 집중도가 높은 편입니다.\n하지만, 대기업이나 외국계 기업 등 다양한 국적의 동료들과 협업을 해야 하거나, 임원으로서 대외적 소통을 해야 하는 위치에 선 경우에는 상황적으로 소통 역량의 중요성이 커지는 것이 사실이죠. 따라서  R&D 직무를 담당하시는 학습자님께는 이와 같은 소통 역량을 강화할 수 있는 세션으로 제안 드리겠습니다 !";
              break;
          }
        }
      });
      return result;
    }

    return SectionCard(
      borderColor: white9,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Suggestion",
            style: Theme.of(context).textTheme.subtitle1,
          ),
          SizedBox(height: 12),
          Text(
            _getSuggestion(),
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ],
      ),
    );
  }
}
