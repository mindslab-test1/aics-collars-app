import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

class RadarChart extends StatefulWidget {
  List<RadarChartData> data;
  double textGap;
  TextStyle textStyle;
  int duration;

  // Size
  double vertexSize;
  double outerBorderSize;
  double innerBorderSize;
  double graphBorderSize;

  // Solor
  Color backgroundColor;
  Color vertexColor;
  Color outerColor;
  Color innerColor;
  Color graphBackgroundColor;
  Color graphBorderColor;

  RadarChart({
    this.data,
    this.textGap = 10.0,
    this.textStyle,
    this.duration = 2500,
    this.vertexSize = 1,
    this.outerBorderSize = 1.0,
    this.innerBorderSize = 1.0,
    this.graphBorderSize = 2.0,
    this.backgroundColor = Colors.transparent,
    this.vertexColor = Colors.black,
    this.outerColor = Colors.black,
    this.innerColor = Colors.black,
    this.graphBackgroundColor = Colors.grey,
    this.graphBorderColor = Colors.yellow,
  });

  @override
  _RadarChartState createState() => _RadarChartState();
}

class _RadarChartState extends State<RadarChart>
    with SingleTickerProviderStateMixin {
  double animationValue = 0;
  Animation<double> animation;
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: Duration(milliseconds: widget.duration), vsync: this);

    animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      curve: Curves.fastOutSlowIn,
      parent: animationController,
    ))
      ..addListener(() {
        setState(() {
          animationValue = animation.value;
        });
      });

    animationController.forward();
  }

  @override
  void didUpdateWidget(RadarChart oldWidget) {
    super.didUpdateWidget(oldWidget);

    animationController.reset();
    animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.data.length > 2) {
      return CustomPaint(
        size: Size(double.infinity, double.infinity),
        painter: RadarChartPainter(
          data: widget.data,
          textGap: widget.textGap,
          textStyle: widget.textStyle,
          animationValue: this.animationValue,
          vertexSize: widget.vertexSize,
          outerBorderSize: widget.outerBorderSize,
          innerBorderSize: widget.innerBorderSize,
          graphBorderSize: widget.graphBorderSize,
          backgroundColor: widget.backgroundColor,
          vertexColor: widget.vertexColor,
          outerColor: widget.outerColor,
          innerColor: widget.innerColor,
          graphBackgroundColor: widget.graphBackgroundColor,
          graphBorderColor: widget.graphBorderColor,
        ),
      );
    } else {
      return Container();
    }
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}

class RadarChartPainter extends CustomPainter {
  final List<RadarChartData> data;
  final double textGap;
  final TextStyle textStyle;
  final double animationValue;
  final double vertexSize;
  final double outerBorderSize;
  final double innerBorderSize;
  final double graphBorderSize;
  final Color backgroundColor;
  final Color vertexColor;
  final Color outerColor;
  final Color innerColor;
  final Color graphBackgroundColor;
  final Color graphBorderColor;

  RadarChartPainter({
    this.data,
    this.textGap,
    this.textStyle,
    this.animationValue,
    this.vertexSize,
    this.outerBorderSize,
    this.innerBorderSize,
    this.graphBorderSize,
    this.backgroundColor,
    this.vertexColor,
    this.outerColor,
    this.innerColor,
    this.graphBackgroundColor,
    this.graphBorderColor,
  });

  // List<double> data = [0.0, 0.25, 0.5, 0.75];
  List<Map<String, double>> vertex = [];

  @override
  void paint(Canvas canvas, Size size) {
    final centerX = size.width * 0.5;
    final centerY = size.width * 0.5;
    final centerOffset = Offset(centerX, centerY);
    final radius = centerX * 0.7;
    Paint backgroundPaint = new Paint()
      ..color = backgroundColor
      ..style = PaintingStyle.fill;
    var outerPaint = Paint()
      ..color = outerColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = outerBorderSize
      ..isAntiAlias = true;
    var innerPaint = Paint()
      ..color = innerColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = innerBorderSize
      ..isAntiAlias = true;
    var graphBackgroundPaint = Paint()
      ..color = graphBackgroundColor
      ..style = PaintingStyle.fill
      ..isAntiAlias = true;
    var graphBorderPaint = Paint()
      ..color = graphBorderColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = graphBorderSize
      ..isAntiAlias = true;
    var vertexPaint = new Paint()
      ..color = vertexColor
      ..style = PaintingStyle.fill;

    // Draw background
    canvas.drawCircle(centerOffset, radius, backgroundPaint);
    // Draw outer circle
    Path outerCircle = Path();
    outerCircle.addOval(Rect.fromCircle(center: centerOffset, radius: radius));
    // Draw inner circle
    Path innerCircle = Path();
    Path graph = Path();
    innerCircle
        .addOval(Rect.fromCircle(center: centerOffset, radius: radius * 0.75));
    innerCircle
        .addOval(Rect.fromCircle(center: centerOffset, radius: radius * 0.5));
    innerCircle
        .addOval(Rect.fromCircle(center: centerOffset, radius: radius * 0.25));
    innerCircle.addOval(Rect.fromCircle(center: centerOffset, radius: 1));
    // Draw inner line
    Path innerLine = Path();
    double changeDeg = 360 / data.length;
    double baseDeg = 90;
    for (int i = 0; i < data.length; i++) {
      double deg = (baseDeg + (changeDeg * i)) % 360;

      innerLine.moveTo(centerX, centerY);
      double _x = (cos(deg * pi / 180) * radius);
      double _y = (sin(deg * pi / 180) * radius);
      innerLine.lineTo(centerX + _x, centerY - _y);
      if (i == 0) {
        graph.moveTo(
            centerX + (_x * data[i].score)* animationValue, centerY - (_y * data[i].score)* animationValue);
      }
      graph.lineTo(centerX + (_x * data[i].score) * animationValue,
          centerY - (_y * data[i].score) * animationValue);
      vertex.add({
        'deg': deg,
        'x': centerX + _x,
        'y': centerY - _y,
      });
    }
    graph.close();
    canvas.drawPath(outerCircle, outerPaint);
    canvas.drawPath(innerCircle, innerPaint);
    canvas.drawPath(innerLine, innerPaint);
    canvas.drawPath(graph, graphBackgroundPaint);
    canvas.drawPath(graph, graphBorderPaint);

    // Draw vertex & text
    vertex.asMap().forEach((idx, value) {
      if (idx < data.length) {
        canvas.drawCircle(
            Offset(value['x'], value['y']), vertexSize, vertexPaint);
        TextSpan span = TextSpan(text: data[idx].field, style: textStyle);
        TextPainter tp =
            TextPainter(text: span, textDirection: TextDirection.ltr);
        tp.layout();

        if (value['deg'] == 90) {
          tp.paint(
              canvas,
              Offset(value['x'] - (tp.width / 2),
                  value['y'] - (tp.height) - textGap));
        } else if (value['deg'] > 90 && value['deg'] <= 180) {
          tp.paint(
              canvas,
              Offset(
                  value['x'] - (tp.width) - textGap, value['y'] - (tp.height)));
        } else if (value['deg'] > 180 && value['deg'] < 270) {
          tp.paint(
              canvas, Offset(value['x'] - (tp.width), value['y'] + textGap));
        } else if (value['deg'] == 270) {
          tp.paint(
              canvas,
              Offset(value['x'] - (tp.width / 2),
                  value['y'] + tp.height + textGap));
        } else if (value['deg'] > 270 && value['deg'] < 360) {
          tp.paint(canvas, Offset(value['x'], value['y'] + textGap));
        } else if (value['deg'] >= 0 && value['deg'] < 90) {
          tp.paint(
              canvas, Offset(value['x'] + textGap, value['y'] - (tp.height)));
        }
      }
    });
  }

  @override
  bool shouldRepaint(RadarChartPainter oldDelegate) {
    return oldDelegate.animationValue != animationValue;
  }
}

class RadarChartData {
  String field;
  double score;

  RadarChartData({
    this.field,
    this.score,
  });

  RadarChartData.fromJson(Map<String, dynamic> json) {
    field = json['field'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['field'] = this.field;
    data['score'] = this.score;
    return data;
  }
}
