import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';

class BlankBox extends StatelessWidget {
  final double width;
  final double heigth;
  final double radius;
  final Color color;

  const BlankBox({
    Key key,
    this.width = 72.0,
    this.heigth = 24.0,
    this.radius = 8.0,
    this.color = kBlankBox,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: heigth,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        color: color,
      ),
    );
  }
}
