import 'package:collars_app_flutter/styles/image/default.dart';
import 'package:flutter/material.dart';

class ImageSlider extends StatefulWidget {
  final List<String> items;

  const ImageSlider({Key key, this.items}) : super(key: key);

  @override
  _ImageSliderState createState() => _ImageSliderState(items: items);
}

class _ImageSliderState extends State<ImageSlider> {
  int dragSensitive = 10;
  int index = 0;
  List<String> items;

  _ImageSliderState({this.items});

  void _previousItem() {
    setState(() {
      index = index > 0 ? index - 1 : items.length - 1;
    });
  }

  void _nextItem() {
    setState(() {
      index = index < items.length - 1 ? index + 1 : 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (endDetails) {
        if (endDetails.primaryVelocity < 0) {
          _nextItem();
        } else {
          _previousItem();
        }
      },
      child: Container(
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: items[index] != null ?NetworkImage(items[index]) : AssetImage(kEmptyImage),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              left: 0,
              right: 0,
              child: ImageSliderDots(
                numberOfDots: items.length,
                index: index,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ImageSliderDots extends StatelessWidget {
  final int numberOfDots;
  final int index;

  const ImageSliderDots({this.numberOfDots, this.index});

  Widget _inactiveItem() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.grey, borderRadius: BorderRadius.circular(5.0)),
        padding: const EdgeInsets.symmetric(horizontal: 3.0),
        width: 8.0,
        height: 8.0,
      ),
    );
  }

  Widget _activeItem() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(color: Colors.grey, spreadRadius: 0.0, blurRadius: 2.0)
            ]),
        width: 10.0,
        height: 10.0,
      ),
    );
  }

  List<Widget> _buildDots() {
    List<Widget> dots = [];
    for (int i = 0; i < numberOfDots; ++i) {
      dots.add(i == index ? _activeItem() : _inactiveItem());
    }
    return dots;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildDots(),
      ),
    );
  }
}
