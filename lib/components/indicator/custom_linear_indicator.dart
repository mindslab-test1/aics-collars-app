import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:flutter/material.dart';

class CustomLinearIndicator extends StatelessWidget {
  final double value;

  const CustomLinearIndicator({Key key, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 16,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16.0),
        child: LinearProgressIndicator(
          value: value,
          backgroundColor: kBtnInActive,
          valueColor: AlwaysStoppedAnimation<Color>(kBtnActive),
        ),
      ),
    );
  }
}
