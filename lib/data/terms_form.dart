class TermsForm {
  bool checked;
  final bool require;
  final String title;
  final String content;

  TermsForm({
    this.checked = false,
    this.require = true,
    this.title,
    this.content,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['checked'] = this.checked;
    data['require'] = this.require;
    data['title'] = this.title;
    data['content'] = this.content;
    return data;
  }
}
