class Links {
  String video;
  String thumbnail;

  Links({this.video, this.thumbnail});

  Links.fromJson(Map<String, dynamic> json) {
    video = json['video'];
    thumbnail = json['thumbnail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['video'] = this.video;
    data['thumbnail'] = this.thumbnail;
    return data;
  }
}