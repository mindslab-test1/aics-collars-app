import 'package:collars_app_flutter/data/session/data/session_quiz.dart';
import 'package:collars_app_flutter/data/session/data/session_video.dart';

class Section {
  Video video;
  Quiz quiz;

  Section({this.video, this.quiz});

  Section.fromJson(Map<String, dynamic> json) {
    video = json['video'] != null ? new Video.fromJson(json['video']) : null;
    quiz = json['quiz'] != null ? new Quiz.fromJson(json['quiz']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.video != null) {
      data['video'] = this.video.toJson();
    }
    if (this.quiz != null) {
      data['quiz'] = this.quiz.toJson();
    }
    return data;
  }
}
