import 'package:collars_app_flutter/data/session/data/session_effect.dart';
import 'package:collars_app_flutter/data/session/data/session_practice.dart';
import 'package:collars_app_flutter/data/session/data/session_section.dart';

class Data {
  String title;
  int expiration;
  String introduction;
  List<String> goal;
  Effect effect;
  Section section;
  List<Practice> practice;

  Data({
    this.title,
    this.expiration,
    this.introduction,
    this.goal,
    this.effect,
    this.section,
    this.practice,
  });

  Data.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    expiration = json['expiration'];
    introduction = json['introduction'];
    goal = json['goal'].cast<String>();
    effect =
        json['effect'] != null ? new Effect.fromJson(json['effect']) : null;
    section =
        json['section'] != null ? new Section.fromJson(json['section']) : null;
    if (json['practice'] != null) {
      practice = new List<Practice>();
      json['practice'].forEach((v) {
        practice.add(new Practice.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['expiration'] = this.expiration;
    data['introduction'] = this.introduction;
    data['goal'] = this.goal;
    if (this.effect != null) {
      data['effect'] = this.effect.toJson();
    }
    if (this.section != null) {
      data['section'] = this.section.toJson();
    }
    // if (this.practice != null) {
    //   data['practice'] = this.practice.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}
