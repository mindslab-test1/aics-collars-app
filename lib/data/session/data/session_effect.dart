class Effect {
  int voca;
  int grammar;
  int context;
  int tone;
  int knowledge;

  Effect({this.voca, this.grammar, this.context, this.tone, this.knowledge});

  Effect.fromJson(Map<String, dynamic> json) {
    voca = json['voca'];
    grammar = json['grammar'];
    context = json['context'];
    tone = json['tone'];
    knowledge = json['knowledge'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['voca'] = this.voca;
    data['grammar'] = this.grammar;
    data['context'] = this.context;
    data['tone'] = this.tone;
    data['knowledge'] = this.knowledge;
    return data;
  }
}