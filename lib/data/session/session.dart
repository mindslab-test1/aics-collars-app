
import 'package:collars_app_flutter/data/session/data/session_data.dart';
import 'package:collars_app_flutter/data/session/links.dart';

class Session {
  int id;
  String name;
  int duration;
  String price;
  String createDate;
  int examId;
  Data data;
  Links links;

  Session(
      {this.id,
        this.name,
        this.duration,
        this.price,
        this.createDate,
        this.examId,
        this.data,
        this.links});

  Session.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    duration = json['duration'];
    price = json['price'];
    createDate = json['createDate'];
    examId = json['examId'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    links = json['links'] != null ? new Links.fromJson(json['links']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['duration'] = this.duration;
    data['price'] = this.price;
    data['createDate'] = this.createDate;
    data['examId'] = this.examId;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.links != null) {
      data['links'] = this.links.toJson();
    }
    return data;
  }
}
