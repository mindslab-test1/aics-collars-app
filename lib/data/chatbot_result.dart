class ChatbotResult {
  String answer;

  ChatbotResult({this.answer});

  ChatbotResult.fromJson(Map<String, dynamic> json){
    answer = json['answer']['answer'];
  }


  Map<String, dynamic> toJson(){
    final Map<String, dynamic> response = new Map<String, dynamic>();
    response['answer'] = this.answer;
    return response;
  }
}