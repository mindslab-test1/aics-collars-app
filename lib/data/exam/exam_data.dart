

class ExamData {
  String type;
  String question;
  List<dynamic> subQuestion;
  List<dynamic> answer;

  ExamData({this.type, this.question, this.subQuestion, this.answer});

  ExamData.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    question = json['question'];
    subQuestion = json['subQuestion'];
    answer = json['answer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['question'] = this.question;
    data['subQuestion'] = this.subQuestion;
    //.map((v) => v.toJson()).toList()
    data['answer'] = this.answer;
    return data;
  }
}
