import 'package:collars_app_flutter/data/exam/exam_data.dart';

class Exam {
  int id;
  String type;
  String name;
  List<ExamData> data;

  Exam({this.id, this.type, this.name, this.data});

  Exam.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    name = json['name'];
    if (json['data'] != null) {
      data = new List<ExamData>();
      json['data'].forEach((v) {
        data.add(new ExamData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['name'] = this.name;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
