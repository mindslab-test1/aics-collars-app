class ExamResult {
  String type;
  dynamic result;

  ExamResult({this.type, this.result});

  ExamResult.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['result'] = this.result;
    return data;
  }
}
