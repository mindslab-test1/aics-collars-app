class UserSurvey {
  int questionId;
  int answerId;
  String question;
  String answer;

  UserSurvey({this.questionId, this.answerId, this.question, this.answer});

  UserSurvey.fromJson(Map<String, dynamic> json) {
    questionId = json['questionId'];
    answerId = json['answerId'];
    question = json['question'];
    answer = json['answer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['questionId'] = this.questionId;
    data['answerId'] = this.answerId;
    data['question'] = this.question;
    data['answer'] = this.answer;
    return data;
  }
}