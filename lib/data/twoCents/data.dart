import 'package:collars_app_flutter/data/common/lang.dart';
import 'package:collars_app_flutter/data/twoCents/data/content.dart';
import 'package:collars_app_flutter/data/twoCents/data/vocabulary.dart';

class Data {
  Lang title;
  List<Content> content;
  List<Vocabulary> vocabulary;

  Data({this.title, this.content, this.vocabulary});

  Data.fromJson(Map<String, dynamic> json) {
    title = json['title'] != null ? new Lang.fromJson(json['title']) : null;
    if (json['content'] != null) {
      content = new List<Content>();
      json['content'].forEach((v) {
        content.add(new Content.fromJson(v));
      });
    }
    if (json['vocabulary'] != null) {
      vocabulary = new List<Vocabulary>();
      json['vocabulary'].forEach((v) {
        vocabulary.add(new Vocabulary.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.title != null) {
      data['title'] = this.title.toJson();
    }
    if (this.content != null) {
      data['content'] = this.content.map((v) => v.toJson()).toList();
    }
    if (this.vocabulary != null) {
      data['vocabulary'] = this.vocabulary.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
