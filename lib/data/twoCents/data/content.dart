class Content {
  String floating;
  String name;
  String eng;
  String kor;
  String audio;

  Content({this.floating, this.name, this.eng, this.kor, this.audio});

  Content.fromJson(Map<String, dynamic> json) {
    floating = json['floating'];
    name = json['name'];
    eng = json['eng'];
    kor = json['kor'];
    audio = json['audio'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['floating'] = this.floating;
    data['name'] = this.name;
    data['eng'] = this.eng;
    data['kor'] = this.kor;
    data['audio'] = this.audio;
    return data;
  }
}
