import 'package:collars_app_flutter/data/user/user_exam_data.dart';

class UserExam {
  String id;
  int userId;
  int examId;
  String type;
  List<UserExamData> data;

  UserExam({this.id, this.userId, this.examId, this.type, this.data});

  UserExam.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    examId = json['examId'];
    type = json['type'];
    if (json['data'] != null) {
      data = new List<UserExamData>();
      json['data'].forEach((v) {
        data.add(new UserExamData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['examId'] = this.examId;
    data['type'] = this.type;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
