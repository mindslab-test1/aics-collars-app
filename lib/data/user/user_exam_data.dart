class UserExamData{
  String type;
  int score;

  UserExamData({this.type, this.score});

  UserExamData.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['score'] = this.score;
    return data;
  }
}