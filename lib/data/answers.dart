class Answers {
  int id;
  String answer;
  String response;

  Answers({this.id, this.answer, this.response});

  Answers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    answer = json['answer'];
    response = json['response'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['answer'] = this.answer;
    data['response'] = this.response;
    return data;
  }
}