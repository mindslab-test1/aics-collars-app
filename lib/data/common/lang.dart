class Lang {
  String kor;
  String eng;

  Lang({this.kor, this.eng});

  Lang.fromJson(Map<String, dynamic> json) {
    kor = json['kor'];
    eng = json['eng'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kor'] = this.kor;
    data['eng'] = this.eng;
    return data;
  }
}

