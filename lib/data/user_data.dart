import 'package:collars_app_flutter/data/session/session.dart';
import 'package:collars_app_flutter/data/user/user_exam.dart';
import 'package:collars_app_flutter/data/user_survey.dart';

class UserData {
  String email;
  String name;
  String signUpDate;
  String signInDate;
  bool google;
  bool naver;
  bool kakao;
  bool facebook;
  List<UserSurvey> survey;
  List<UserExam> exam;
  List<Session> session;
  String company;
  String companyId;

  UserData({
    this.email,
    this.name,
    this.signUpDate,
    this.signInDate,
    this.google,
    this.naver,
    this.kakao,
    this.facebook,
    this.survey,
    this.exam,
    this.session,
    this.company,
    this.companyId,
  });

  UserData.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    name = json['name'];
    signUpDate = json['signUpDate'];
    signInDate = json['signInDate'];
    google = json['google'];
    naver = json['naver'];
    kakao = json['kakao'];
    facebook = json['facebook'];
    if (json['survey'] != null) {
      survey = new List<UserSurvey>();
      json['survey'].forEach((v) {
        survey.add(new UserSurvey.fromJson(v));
      });
    }
    if (json['exam'] != null) {
      exam = new List<UserExam>();
      json['exam'].forEach((v) {
        exam.add(new UserExam.fromJson(v));
      });
    }
    if (json['session'] != null) {
      session = new List<Session>();
      json['session'].forEach((v) {
        session.add(new Session.fromJson(v));
      });
    }
    company = json['company'];
    companyId = json['companyId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['name'] = this.name;
    data['signUpDate'] = this.signUpDate;
    data['signInDate'] = this.signInDate;
    data['google'] = this.google;
    data['naver'] = this.naver;
    data['kakao'] = this.kakao;
    data['facebook'] = this.facebook;
    if (this.survey != null) {
      data['survey'] = this.survey.map((v) => v.toJson()).toList();
    }
    if (this.exam != null) {
      data['exam'] = this.exam.map((v) => v.toJson()).toList();
    }
    if (this.session != null) {
      data['session'] = this.session.map((v) => v.toJson()).toList();
    }
    data['company'] = this.company;
    data['companyId'] = this.companyId;
    return data;
  }
}
