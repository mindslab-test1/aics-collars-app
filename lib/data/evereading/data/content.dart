class Content {
  String header;
  String text;

  Content({this.header, this.text});

  Content.fromJson(Map<String, dynamic> json) {
    header = json['header'];
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['header'] = this.header;
    data['text'] = this.text;
    return data;
  }
}
