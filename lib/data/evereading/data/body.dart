import 'package:collars_app_flutter/data/evereading/data/content.dart';

class Body {
  String title;
  String description;
  List<Content> content;

  Body({this.title, this.description, this.content});

  Body.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    description = json['description'];
    if (json['content'] != null) {
      content = new List<Content>();
      json['content'].forEach((v) {
        content.add(new Content.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['description'] = this.description;
    if (this.content != null) {
      data['content'] = this.content.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
