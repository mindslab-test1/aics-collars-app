
class Links {
  String thumbnail;
  String introBanner;
  String banner;

  Links({this.thumbnail, this.introBanner, this.banner});

  Links.fromJson(Map<String, dynamic> json) {
    thumbnail = json['thumbnail'];
    introBanner = json['introBanner'];
    banner = json['banner'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['thumbnail'] = this.thumbnail;
    data['introBanner'] = this.introBanner;
    data['banner'] = this.banner;
    return data;
  }
}