import 'package:collars_app_flutter/data/evereading/data/body.dart';
import 'package:collars_app_flutter/data/evereading/data/vocabulary.dart';

class Data {
  Body eng;
  Body kor;
  List<Vocabulary> vocabulary;

  Data({this.eng, this.kor, this.vocabulary});

  Data.fromJson(Map<String, dynamic> json) {
    eng = json['eng'] != null ? new Body.fromJson(json['eng']) : null;
    kor = json['kor'] != null ? new Body.fromJson(json['kor']) : null;
    if (json['vocabulary'] != null) {
      vocabulary = new List<Vocabulary>();
      json['vocabulary'].forEach((v) {
        vocabulary.add(new Vocabulary.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.eng != null) {
      data['eng'] = this.eng.toJson();
    }
    if (this.kor != null) {
      data['kor'] = this.kor.toJson();
    }
    if (this.vocabulary != null) {
      data['vocabulary'] = this.vocabulary.map((v) => v.toJson()).toList();
    }
    return data;
  }

}