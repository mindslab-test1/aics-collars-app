import 'package:collars_app_flutter/data/common/lang.dart';
import 'package:collars_app_flutter/data/evereading/data.dart';
import 'package:collars_app_flutter/data/evereading/links.dart';

class Evereading {
  int id;
  String name;
  Lang sector;
  int lev;
  String createDate;
  Data data;
  Links links;

  Evereading({this.id,
    this.name,
    this.sector,
    this.lev,
    this.createDate,
    this.data,
    this.links});

  Evereading.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    sector =
    json['sector'] != null ? new Lang.fromJson(json['sector']) : null;
    lev = json['lev'];
    createDate = json['createDate'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    links = json['links'] != null ? new Links.fromJson(json['links']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    if (this.sector != null) {
      data['sector'] = this.sector.toJson();
    }
    data['lev'] = this.lev;
    data['createDate'] = this.createDate;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.links != null) {
      data['links'] = this.links.toJson();
    }
    return data;
  }
}