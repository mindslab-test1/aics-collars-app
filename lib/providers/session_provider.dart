import 'package:collars_app_flutter/data/session/session.dart';
import 'package:collars_app_flutter/services/session.dart';
import 'package:flutter/cupertino.dart';

class SessionProvider extends ChangeNotifier {
  SessionService _sessionService = SessionService();

  bool _isRecommandLoading = false;
  List<Session> _recommand = [];

  Session _current;

  SessionProvider() {
    _init();
  }

  void _init() async {
    await loadRecommand(init: true);
  }

  void loadRecommand({bool init = false}) async {
    if (!init) _isRecommandLoading = true;
    notifyListeners();
    List<Session> _list = List();
    _list += await _sessionService.getSessionList(current: 0, increment: 5, data: true);
    _recommand = _list;
    if (!init) _isRecommandLoading = false;
    notifyListeners();
  }

  // Getter & Setter
  List<Session> get recommand => _recommand;

  set recommand(List<Session> value) {
    _recommand = value;
    notifyListeners();
  }

  bool get isRecommandLoading => _isRecommandLoading;

  set isRecommandLoading(bool value) {
    _isRecommandLoading = value;
    notifyListeners();
  }

  Session get current => _current;

  set current(Session value) {
    _current = value;
    notifyListeners();
  }
}
