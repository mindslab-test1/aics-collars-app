import 'package:collars_app_flutter/data/twoCents/twoCents.dart';
import 'package:collars_app_flutter/services/twocents.dart';
import 'package:flutter/material.dart';

class TwoCentsProvider extends ChangeNotifier {
  TwoCentsService _twoCentsService = TwoCentsService();

  // Script
  bool _isSciprtsLoading = false;
  int _increment = 2;
  List<TwoCents> _scripts = [];

  // Audio
  bool _isAudioActive = false;
  bool _isMicActive = false;

  // mic record(mic 녹음 중복을 피하기 위해서)
  bool _isRecordBtnActive = false;

  TwoCentsProvider() {
    _init();
  }

  Future<void> _init() async {
    await loadScript(init: true);
  }

  void loadScript({bool init = false}) async {
    if (!init) _isSciprtsLoading = true;
    notifyListeners();
    _scripts += await _twoCentsService.getScripts(
      current: _scripts.length,
      increment: _increment,
    );

    if (!init) _isSciprtsLoading = false;
    notifyListeners();
  }

  // Getter & Setter
  List<TwoCents> get scripts => _scripts;

  set scripts(List<TwoCents> value) {
    _scripts = value;
    notifyListeners();
  }

  bool get isSciprtsLoading => _isSciprtsLoading;

  set isSciprtsLoading(bool value) {
    _isSciprtsLoading = value;
    notifyListeners();
  }

  TwoCentsService get twoCentsService => _twoCentsService;

  set twoCentsService(TwoCentsService value) {
    _twoCentsService = value;
    notifyListeners();
  }

  bool get isAudioActive => _isAudioActive;

  set isAudioActive(bool value) {
    _isAudioActive = value;
    notifyListeners();
  }

  bool get isMicActive => _isMicActive;

  set isMicActive(bool value) {
    _isMicActive = value;
    notifyListeners();
  }

  bool get isRecordBtnActive => _isRecordBtnActive;

  set isRecordBtnActive(bool value){
    _isRecordBtnActive = value;
    notifyListeners();
  }
}
