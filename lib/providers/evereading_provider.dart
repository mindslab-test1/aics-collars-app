import 'package:collars_app_flutter/data/evereading/evereading.dart';
import 'package:collars_app_flutter/data/hashtag/hash_tag.dart';
import 'package:collars_app_flutter/services/evereading.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:flutter/cupertino.dart';

class EvereadingProvider extends ChangeNotifier {
  EvereadingService _evereadingService = EvereadingService();

  // Sector
  bool _isSectorsLoading = false;
  int _currentSector = 0;
  List<HashTag> _sectors = [];

  // Column
  bool _isColumnsLoading = false;
  int _increment = 2;
  List<Evereading> _columns = [];

  EvereadingProvider() {
    _init();
  }

  Future<void> _init() async {
    await loadSectors(lang: LANG_KOR, init: true);
    await loadColumn(init: true);
  }

  void loadSectors({String lang, bool init = false}) async {
    if (!init) _isSectorsLoading = true;
    notifyListeners();
    List<HashTag> _list = List();
    HashTag hashTag = HashTag();
    hashTag.id = 0;
    if (lang.toUpperCase() == LANG_ENG) {
      hashTag.tag = "all";
    } else {
      hashTag.tag = "전체";
    }
    _list.add(hashTag);
    _list += await _evereadingService.getSectors(lang);
    _sectors = _list;
    if (!init) _isSectorsLoading = false;
    notifyListeners();
  }

  void loadColumn({bool init = false}) async {
    if (!init) _isColumnsLoading = true;
    notifyListeners();
    int _tagId = _sectors[_currentSector].id;
    _columns += await _evereadingService.getColumns(
      tagId: _tagId,
      current: _columns.length,
      increment: _increment,
    );
    if (!init) _isColumnsLoading = false;
    notifyListeners();
  }

  void clear(){
    _columns = [];
    loadColumn();
  }

  // Getter & Setter
  List<HashTag> get sectors => _sectors;

  set sectors(List<HashTag> value) {
    _sectors = value;
    notifyListeners();
  }

  int get currentSector => _currentSector;

  set currentSector(int value) {
    _currentSector = value;
    notifyListeners();
  }

  bool get isSectorsLoading => _isSectorsLoading;

  set isSectorsLoading(bool value) {
    _isSectorsLoading = value;
    notifyListeners();
  }

  List<Evereading> get columns => _columns;

  set columns(List<Evereading> value) {
    _columns = value;
    notifyListeners();
  }

  int get increment => _increment;

  set increment(int value) {
    _increment = value;
    notifyListeners();
  }

  bool get isColumnsLoading => _isColumnsLoading;

  set isColumnsLoading(bool value) {
    _isColumnsLoading = value;
    notifyListeners();
  }
}
