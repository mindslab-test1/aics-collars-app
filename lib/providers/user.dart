import 'package:collars_app_flutter/data/user_data.dart';
import 'package:collars_app_flutter/services/auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

class User {
  final String uid;
  final String email;
  int _companyId;
  UserData _data;

  User({this.uid, this.email});

  Future<void> loadUser() async {
    AuthService _authService = AuthService();
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getBool('isCompany')??false) {
      _data = await _authService.getUserById(pref.getInt("companyId"));
    } else {
      _data = await _authService.getUser(uid);
    }
  }

  // Getter & Setter
  int get companyId => _companyId;

  set companyId(int value) => _companyId = value;

  UserData get data => _data;

  set data(UserData value) => _data = value;
}
