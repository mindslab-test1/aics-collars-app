import 'package:flutter/material.dart';

class MainProvider with ChangeNotifier {
  // #1
  int _currentPage;
  PageController _pageController;

  // Constructor
  MainProvider() {
    _currentPage = 0;
    // _pageController = PageController(initialPage: _currentPage);
  }


  // Getter & Setter
  int get currentPage => _currentPage;
  set currentPage(int value) {
    _currentPage = value;
    _pageController.jumpToPage(value);
    notifyListeners();
  }
  PageController get pageController => _pageController;
  set pageController(PageController value) {
    _pageController = value;
  }
}
