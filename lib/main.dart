import 'package:collars_app_flutter/layouts/auth/reset_password.dart';
import 'package:collars_app_flutter/layouts/auth/sign_in_company_screen.dart';
import 'package:collars_app_flutter/layouts/auth/sign_in_screen.dart';
import 'package:collars_app_flutter/layouts/auth/sign_up_screen.dart';
import 'package:collars_app_flutter/layouts/intro/consult/consult_level_result_screen.dart';
import 'package:collars_app_flutter/layouts/intro/consult/consult_level_screen.dart';
import 'package:collars_app_flutter/layouts/intro/consult/consult_name_screen.dart';
import 'package:collars_app_flutter/layouts/intro/consult/consult_survey_screen.dart';
import 'package:collars_app_flutter/layouts/main/main_screen.dart';
import 'package:collars_app_flutter/layouts/setting/setting_screen.dart';
import 'package:collars_app_flutter/layouts/twocents/two_cents_content_screen.dart';
import 'package:collars_app_flutter/layouts/wrap_screen.dart';
import 'package:collars_app_flutter/providers/evereading_provider.dart';
import 'package:collars_app_flutter/providers/main_provider.dart';
import 'package:collars_app_flutter/providers/session_provider.dart';
import 'package:collars_app_flutter/providers/twocents_provider.dart';
import 'package:collars_app_flutter/services/auth.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:provider/provider.dart';

void main() async {
  await DotEnv().load('.env');
  InAppPurchaseConnection.enablePendingPurchases();

  runApp(CollarsApp());
}

class CollarsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider(create: (context) => AuthService().user),
        ChangeNotifierProvider(create: (context) => MainProvider()),
        ChangeNotifierProvider(create: (context) => EvereadingProvider()),
        ChangeNotifierProvider(create: (context) => TwoCentsProvider()),
        ChangeNotifierProvider(create: (context) => SessionProvider()),
      ],
      child: MaterialApp(
        theme: ThemeData(
            fontFamily: 'Raleway',
            textTheme: TextTheme(
              headline1: TextStyle(
                fontSize: 12.0,
                fontWeight: FontWeight.w500,
                fontFamilyFallback: <String>[
                  'NotoSansCJKkr',
                ],
                color: black,
              ),
              headline2: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                fontFamilyFallback: <String>[
                  'NotoSansCJKkr',
                ],
                color: black,
              ),
              subtitle1: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                fontFamilyFallback: <String>[
                  'NotoSansCJKkr',
                ],
                color: black,
              ),
              subtitle2: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                fontFamilyFallback: <String>[
                  'NotoSansCJKkr',
                ],
                color: black,
              ),
              bodyText1: TextStyle(
                fontSize: 16,
                fontFamilyFallback: <String>[
                  'NotoSansCJKkr',
                ],
                color: black,
              ),
              bodyText2: TextStyle(
                fontSize: 14,
                fontFamilyFallback: <String>[
                  'NotoSansCJKkr',
                ],
                color: black,
              ),
              caption: TextStyle(
                fontSize: 12,
                fontFamilyFallback: <String>[
                  'NotoSansCJKkr',
                ],
                color: black,
              ),
            ).apply(
              bodyColor: black,
            )),
        initialRoute: WrapScreen.id,
        routes: {
          WrapScreen.id: (context) => WrapScreen(),
          MainScreen.id: (context) => MainScreen(),
          SignInScreen.id: (context) => SignInScreen(),
          SignInCompanyScreen.id: (context) => SignInCompanyScreen(),
          SignUpScreen.id: (context) => SignUpScreen(),
          ResetPassword.id: (context) => ResetPassword(),
          ConsultLevelScreen.id: (context) => ConsultLevelScreen(),
          ConsultNameScreen.id: (context) => ConsultNameScreen(),
          ConsultSurveyScreen.id: (context) => ConsultSurveyScreen(),
          ConsultLevelResultScreen.id: (context) => ConsultLevelResultScreen(),
          TwoCentsContentScreen.id: (context) => TwoCentsContentScreen(),
          SettingScreen.id: (context) => SettingScreen(),
        },
      ),
    );
  }
}
