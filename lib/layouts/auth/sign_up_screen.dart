import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/layouts/auth/sign_up_form.dart';
import 'package:collars_app_flutter/layouts/auth/sign_up_intro.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';

class SignUpScreen extends StatefulWidget {
  static const String id = 'sign_up_screen';

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  int currentPage = 0;
  PageController _pageController = new PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    _pageController.addListener(() {
      setState(() {
        currentPage = _pageController.page.toInt();
      });
    });

    return Scaffold(
      appBar: DefaultAppBar(),
      body: SafeArea(
        child: Container(
          padding: kPaddingHorizontal,
          child: PageView(
            controller: _pageController,
            physics: NeverScrollableScrollPhysics(),
            children: [
              SignUpIntro(
                onPress: () {
                  _pageController.jumpToPage(++currentPage);
                },
              ),
              SignUpForm(),
            ],
          ),
        ),
      ),
    );
  }
}
