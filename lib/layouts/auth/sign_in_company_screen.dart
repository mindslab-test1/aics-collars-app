import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/components/container/TextFieldContainer.dart';
import 'package:collars_app_flutter/components/form/custom_text_field.dart';
import 'package:collars_app_flutter/data/user_data.dart';
import 'package:collars_app_flutter/layouts/wrap_screen.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/services/auth.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignInCompanyScreen extends StatefulWidget {
  static const String id = 'sign_in_company_screen';

  @override
  _SignInCompanyScreenState createState() => _SignInCompanyScreenState();
}

class _SignInCompanyScreenState extends State<SignInCompanyScreen> {
  AuthService _authService = AuthService();

  bool isCompany = false;
  String company;
  bool isId = false;
  String id;
  bool isPw = false;
  String pw;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        padding: kPaddingHorizontal,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: 56),
              Text(
                "기업고객이신가요?\n학습을 위한\n로그인을 해주세요.",
                textAlign: TextAlign.left,
                style: Theme.of(context).textTheme.headline2,
              ),
              SizedBox(height: 60),
              TextFieldContainer(
                isActive: isCompany,
                activeColor: kBtnActive,
                inActiveColor: kBtnInActive,
                child: CustomTextField(
                  onChanged: (value) {
                    setState(() {
                      company = value;
                      if (company == null || company == "")
                        isCompany = false;
                      else
                        isCompany = true;
                    });
                  },
                  icon: kBuilding,
                  activeColor: kBtnActive,
                  inactiveColor: kBtnInActive,
                  hintText: "Company",
                ),
              ),
              SizedBox(height: 16),
              TextFieldContainer(
                isActive: isId,
                activeColor: kBtnActive,
                inActiveColor: kBtnInActive,
                child: CustomTextField(
                  onChanged: (value) {
                    setState(() {
                      id = value;
                      if (id == null || id == "")
                        isId = false;
                      else
                        isId = true;
                    });
                  },
                  icon: kBuilding,
                  activeColor: kBtnActive,
                  inactiveColor: kBtnInActive,
                  hintText: "ID",
                ),
              ),
              SizedBox(height: 16),
              TextFieldContainer(
                isActive: isPw,
                activeColor: kBtnActive,
                inActiveColor: kBtnInActive,
                child: CustomTextField(
                  onChanged: (value) {
                    setState(() {
                      pw = value;
                      if (pw == null || pw == "")
                        isPw = false;
                      else
                        isPw = true;
                    });
                  },
                  icon: kBuilding,
                  activeColor: kBtnActive,
                  inactiveColor: kBtnInActive,
                  hintText: "PASSWORD",
                ),
              ),
              SizedBox(height: 46),
              CapsuleContrastButton(
                type: ONCE,
                padding: kPaddingBtnVertical,
                primaryColor: kSignInBtn1,
                secondaryColor: kSignInBtn2,
                text: '로그인',
                onTap: () async {
                  if(company != null && id != null && pw != null){
                    int _companyId = await _authService.validCompanyUser(code: company, id: id, pw: pw);


                    if(_companyId != null){
                      User _user = Provider.of<User>(context, listen: false);
                      _user = new User();
                      _user.companyId = _companyId;

                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      prefs.setBool("isCompany", true);
                      prefs.setInt("companyId", _companyId);
                      Navigator.pushReplacementNamed(context, WrapScreen.id);
                    }
                  }
                },
              ),

            ],
          ),
        ),
      ),
    );
  }
}
