import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/components/card/terms_card.dart';
import 'package:collars_app_flutter/data/terms_form.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/md.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';

class SignUpIntro extends StatefulWidget {
  final Function onPress;

  const SignUpIntro({Key key, this.onPress}) : super(key: key);

  @override
  _SignUpIntroState createState() => _SignUpIntroState();
}

class _SignUpIntroState extends State<SignUpIntro> {
  List<TermsForm> _terms = [
    TermsForm(title: "칼라스 이용약관 동의", content: kTermOfUse),
    TermsForm(title: "개인정보 수집 및 이용 동의", content: kTermOfInfo),
  ];

  @override
  Widget build(BuildContext context) {
    void _showDialog() async {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
              "칼라스 이용약관과 개인정보 수집 및 이용에 대한 안내 모두 동의해주세요.",
              style: Theme.of(context).textTheme.bodyText1,
            ),
          );
        },
      );
    }

    List<Widget> _buildTerms() {
      List<Widget> _list = List();
      _terms.asMap().forEach((index, value) {
        _list.add(TermsCard(terms: value));
        if (index < _terms.length) _list.add(SizedBox(height: 24));
      });

      return _list;
    }

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "환영합니다.\n회원가입을 위해 먼저\n약관에 동의해주세요.",
            textAlign: TextAlign.left,
            style: Theme.of(context).textTheme.headline2,
          ),
          SizedBox(height: 60),
          ..._buildTerms(),
          SizedBox(height: 33),
          CapsuleContrastButton(
            type: FIXED,
            isActive: true,
            padding: kPaddingBtnVertical,
            primaryColor: kSignInBtn1,
            secondaryColor: kSignInBtn2,
            text: 'NEXT',
            onTap: () {
              bool _conditions = true;
              for(int i = 0; i < _terms.length; i++){
                if(_terms[i].require && !_terms[i].checked){
                  _conditions = false;
                  break;
                }
              }
              _conditions ? widget.onPress() : _showDialog();
            },
          ),
          SizedBox(height: 24),
        ],
      ),
    );
  }
}
