import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/components/button/circle_button.dart';
import 'package:collars_app_flutter/components/container/TextFieldContainer.dart';
import 'package:collars_app_flutter/components/form/custom_text_field.dart';
import 'package:collars_app_flutter/components/text/info_error.dart';
import 'package:collars_app_flutter/layouts/auth/email_verification.dart';
import 'package:collars_app_flutter/layouts/auth/reset_password.dart';
import 'package:collars_app_flutter/layouts/auth/sign_in_company_screen.dart';
import 'package:collars_app_flutter/layouts/auth/sign_up_screen.dart';
import 'package:collars_app_flutter/services/auth.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kakao_flutter_sdk/all.dart';

class SignInScreen extends StatefulWidget {
  static const String id = 'sign_in_screen';

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  AuthService _authService = AuthService();
  String status = '';

  bool isEmail = false;
  String email;
  bool isPassword = false;
  String password;

  @override
  Widget build(BuildContext context) {
    Widget _buildNotice() {
      if (status == SIGN_IN_FAIL) {
        return Center(
          child: Column(
            children: [
              SizedBox(height: 8),
              InfoError(show: true, text: '이메일 또는 비밀번호가 잘못되었습니다.'),
            ],
          ),
        );
      } else {
        return Container();
      }
    }

    Widget buildSocialLogin() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleButton(
            onPressed: () {
              KakaoContext.clientId = DotEnv().env['KAKAO_CLIENT_ID'];
              _authService.signInWithKakao();
            },
            size: kSocialLoginButtonSize,
            color: kKakaoBg,
            child: Center(
              child: SvgPicture.asset(
                kKakaoIcon,
                width: kSocialLoginIconSize,
                height: kSocialLoginIconSize,
                // color: kKaKaoColor,
              ),
            ),
          ),
          SizedBox(width: 12),
          CircleButton(
            onPressed: _authService.signInWithGoogle,
            size: kSocialLoginButtonSize,
            color: kGoogleBg,
            child: Center(
              child: SvgPicture.asset(
                kGoogleIcon,
                width: kSocialLoginIconSize,
                height: kSocialLoginIconSize,
                // color: kGoogleColor,
              ),
            ),
          ),
          SizedBox(width: 12),
          CircleButton(
            onPressed: _authService.signInWithNaver,
            size: kSocialLoginButtonSize,
            color: kNaverBg,
            child: Center(
              child: SvgPicture.asset(
                kNaverIcon,
                width: kSocialLoginIconSize,
                height: kSocialLoginIconSize,
                color: kNaverColor,
              ),
            ),
          ),
        ],
      );
    }

    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: kPaddingHorizontal,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(height: 56),
                Text(
                  "반갑습니다!\n학습을 위한\n회원가입이나 로그인을 해주세요.",
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.headline2,
                ),
                SizedBox(height: 60),
                TextFieldContainer(
                  isActive: isEmail,
                  activeColor: kBtnActive,
                  inActiveColor: kBtnInActive,
                  child: CustomTextField(
                    onChanged: (value) {
                      setState(() {
                        email = value;
                        if (email == null || email == "")
                          isEmail = false;
                        else
                          isEmail = true;
                      });
                    },
                    icon: kMail,
                    activeColor: kBtnActive,
                    inactiveColor: kBtnInActive,
                    hintText: "E-mail",
                  ),
                ),
                SizedBox(height: 16),
                TextFieldContainer(
                  activeColor: kBtnActive,
                  inActiveColor: kBtnInActive,
                  child: CustomTextField(
                    secure: true,
                    onChanged: (value) {
                      setState(() {
                        password = value;
                        if (password == null || password == "")
                          isPassword = false;
                        else
                          isPassword = true;
                      });
                    },
                    icon: kLock,
                    activeColor: kBtnActive,
                    inactiveColor: kBtnInActive,
                    hintText: "Password",
                  ),
                ),
                _buildNotice(),
                SizedBox(height: 46),
                Container(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, ResetPassword.id);
                    },
                    child: Text(
                      "비밀번호를 잊으셨습니까?",
                      textAlign: TextAlign.right,
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ),
                ),
                SizedBox(height: 28),
                CapsuleContrastButton(
                  type: ONCE,
                  padding: kPaddingBtnVertical,
                  primaryColor: kSignInBtn1,
                  secondaryColor: kSignInBtn2,
                  text: '로그인',
                  onTap: () async {
                    _authService.signOut();
                    String result =
                        await _authService.signInWithEmail(email, password);
                    if (result == SIGN_IN_EMAIL_VALID_FAIL) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => EmailVerification(
                            email: email,
                            password: password,
                          ),
                        ),
                      );
                    } else if (result == SIGN_IN_FAIL) {
                      setState(() {
                        status = result;
                      });
                    }
                  },
                ),
                SizedBox(height: 12),
                CapsuleContrastButton(
                  type: ONCE,
                  padding: kPaddingBtnVertical,
                  primaryColor: kSignInBtn1,
                  secondaryColor: kSignInBtn2,
                  text: '회원가입',
                  onTap: () {
                    Navigator.pushNamed(context, SignUpScreen.id);
                  },
                ),
                SizedBox(height: 36),
                buildSocialLogin(),
                SizedBox(height: 36),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "기업 고객이신가요? ",
                      style: Theme.of(context).textTheme.caption,
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.of(context).pushNamed(SignInCompanyScreen.id);
                      },
                      child: Text(
                        "기업고객 로그인",
                        style: Theme.of(context).textTheme.caption.copyWith(
                              color: kActive,
                              decoration: TextDecoration.underline,
                            ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
