import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/components/container/TextFieldContainer.dart';
import 'package:collars_app_flutter/components/form/custom_text_field.dart';
import 'package:collars_app_flutter/components/text/info_error.dart';
import 'package:collars_app_flutter/data/common/pattern.dart';
import 'package:collars_app_flutter/layouts/auth/email_verification.dart';
import 'package:collars_app_flutter/services/auth.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SignUpForm extends StatefulWidget {
  final Function onPress;

  const SignUpForm({Key key, this.onPress}) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  AuthService _authService = AuthService();

  bool isEmail = false;
  String email;
  bool isPassword = false;
  String password;
  bool isPassword2 = false;
  String password2;

  bool showInfo = false;
  bool emailValid = false;
  bool passwordValid = false;
  bool password2Valid = false;

  @override
  Widget build(BuildContext context) {
    Widget _buildNotice() {
      String _text = "";
      if (!emailValid) {
        _text = "※ 이메일 형식이 올바르지 않습니다.";
      } else if (!passwordValid) {
        _text = "※ 비밀번호는  6 ~ 20자 영문 대소문자,숫자,특수문자 조합을 입력해 주세요.";
      } else if (!password2Valid) {
        _text = "※ 비밀번호가 일치하지 않습니다.";
      } else {
        _text = "";
      }

      return Center(
        child: Column(
          children: [
            SizedBox(height: 8),
            InfoError(show: showInfo, text: _text),
          ],
        ),
      );
    }

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "이메일과 비밀번호를 입력해주세요.\n비밀번호는 6자리 이상으로\n입력해주세요.",
            textAlign: TextAlign.left,
            style: Theme.of(context).textTheme.headline2,
          ),
          SizedBox(height: 60),
          TextFieldContainer(
            isActive: isEmail,
            activeColor: kBtnActive,
            inActiveColor: kBtnInActive,
            child: CustomTextField(
              onChanged: (value) {
                setState(() {
                  email = value;
                  if (email == null || email == "")
                    isEmail = false;
                  else
                    isEmail = true;
                });
              },
              icon: kMail,
              activeColor: kBtnActive,
              inactiveColor: kBtnInActive,
              hintText: "E-mail",
            ),
          ),
          SizedBox(height: 16),
          TextFieldContainer(
            isActive: isPassword,
            activeColor: kBtnActive,
            inActiveColor: kBtnInActive,
            child: CustomTextField(
              secure: true,
              onChanged: (value) {
                setState(() {
                  password = value;
                  if (password == null || password == "")
                    isPassword = false;
                  else
                    isPassword = true;
                });
              },
              icon: kLock,
              activeColor: kBtnActive,
              inactiveColor: kBtnInActive,
              hintText: "Password",
            ),
          ),
          SizedBox(height: 16),
          TextFieldContainer(
            isActive: isPassword2,
            activeColor: kBtnActive,
            inActiveColor: kBtnInActive,
            child: CustomTextField(
              secure: true,
              onChanged: (value) {
                setState(() {
                  password2 = value;
                  if (password2 == null || password2 == "")
                    isPassword2 = false;
                  else
                    isPassword2 = true;
                });
              },
              icon: kLock,
              activeColor: kBtnActive,
              inactiveColor: kBtnInActive,
              hintText: "Contirm Password",
            ),
          ),
          _buildNotice(),
          SizedBox(height: 33),
          CapsuleContrastButton(
            type: FIXED,
            isActive: true,
            padding: kPaddingBtnVertical,
            primaryColor: kSignInBtn1,
            secondaryColor: kSignInBtn2,
            text: 'NEXT',
            onTap: () async {
              setState(() {
                showInfo = true;
                emailValid = new RegExp(emailPattern).hasMatch(email);
                passwordValid = new RegExp(passwordPattern).hasMatch(password);
                password2Valid = password == password2 ? true : false;
              });
              if (emailValid && passwordValid && password2Valid) {
                AuthResult authResult =
                    await _authService.signUpWithEmail(email, password);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => EmailVerification(
                      email: email,
                      password: password,
                    ),
                  ),
                );
              }
            },
          ),
          SizedBox(height: 24),
        ],
      ),
    );
  }
}
