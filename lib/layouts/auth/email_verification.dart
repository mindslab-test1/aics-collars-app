import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/services/auth.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';

import '../wrap_screen.dart';

class EmailVerification extends StatefulWidget {
  static const String id = 'email_verification';

  final String email;
  final String password;

  const EmailVerification({
    Key key,
    this.email,
    this.password,
  }) : super(key: key);

  @override
  _EmailVerificationState createState() => _EmailVerificationState();
}

class _EmailVerificationState extends State<EmailVerification> {
  AuthService _authService = AuthService();

  @override
  void initState() {
    _authService.sendEmailVerification(widget.email, widget.password);
  }

  @override
  Widget build(BuildContext context) {
    void _showDialog(String message) async {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
              message,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          );
        },
      );
    }

    return Scaffold(
      appBar: DefaultAppBar(),
      body: SafeArea(
        child: Container(
          padding: kPaddingHorizontal,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  "인증 메일이 발송되었습니다.\n이메일 인증 후\n아래 인증완료 버튼을 눌러주세요.",
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '메일이 오지않으셨나요? ',
                    style: Theme.of(context).textTheme.caption,
                  ),
                  GestureDetector(
                    onTap: () async {
                      await _authService.sendEmailVerification(
                          widget.email, widget.password);
                      _showDialog('인증 메일을 재전송 하였습니다.');
                    },
                    child: Text(
                      '재전송 받기',
                      style: Theme.of(context)
                          .textTheme
                          .caption
                          .copyWith(color: kActive),
                    ),
                  )
                ],
              ),
              SizedBox(height: 36),
              CapsuleContrastButton(
                type: FIXED,
                isActive: true,
                padding: kPaddingBtnVertical,
                primaryColor: kSignInBtn1,
                secondaryColor: kSignInBtn2,
                text: '인증 완료',
                onTap: () async {
                  _authService.signOut();
                  String result = await _authService.signInWithEmail(
                      widget.email, widget.password);

                  if (result == SIGN_IN_EMAIL_VALID_FAIL ||
                      result == SIGN_IN_FAIL) {
                    _showDialog(
                        "아직 인증이 완료되지 않았습니다.\n메일이 오지 않으셨다면\n재전송 받기를 시도해주세요.");
                  }
                  else if(result == SIGN_IN_SUCCESS){
                    Navigator.pushReplacementNamed(context, WrapScreen.id);
                  }
                },
              ),
              SizedBox(height: 24),
            ],
          ),
        ),
      ),
    );
  }
}
