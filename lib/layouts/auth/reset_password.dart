import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/components/container/TextFieldContainer.dart';
import 'package:collars_app_flutter/components/form/custom_text_field.dart';
import 'package:collars_app_flutter/data/common/pattern.dart';
import 'package:collars_app_flutter/services/auth.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';

class ResetPassword extends StatefulWidget {
  static const String id = 'reset_password';

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  AuthService _authService = AuthService();
  bool isEmail = false;
  bool emailValid = false;
  String email = "";

  @override
  Widget build(BuildContext context) {
    void _showDialog(String text) async {
      return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
              text,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          );
        },
      ).then((value) {
        if (emailValid) Navigator.of(context).pop();
      });
    }

    return Scaffold(
      appBar: DefaultAppBar(),
      body: SafeArea(
        child: Container(
          padding: kPaddingHorizontal,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '비밀번호를 잊으셨나요\n재설정 할 비밀번호의\n이메일을 입력해주세요.',
                style: Theme.of(context).textTheme.headline2,
              ),
              SizedBox(height: 60),
              TextFieldContainer(
                isActive: isEmail,
                activeColor: kBtnActive,
                inActiveColor: kBtnInActive,
                child: CustomTextField(
                  onChanged: (value) {
                    setState(() {
                      email = value;
                      if (email == null || email == "")
                        isEmail = false;
                      else
                        isEmail = true;
                    });
                  },
                  icon: kMail,
                  activeColor: kBtnActive,
                  inactiveColor: kBtnInActive,
                  hintText: "E-mail",
                ),
              ),
              SizedBox(height: 46),
              CapsuleContrastButton(
                type: FIXED,
                isActive: true,
                padding: kPaddingBtnVertical,
                primaryColor: kSignInBtn1,
                secondaryColor: kSignInBtn2,
                text: '비밀번호 재설정 메일 전송',
                onTap: () async {
                  setState(() {
                    emailValid = new RegExp(emailPattern).hasMatch(email);
                  });
                  if (emailValid) {
                    _authService.resetPassword(email: email);
                    _showDialog("비밀번호 재설정 메일이 전송 되었습니다.");
                  } else {
                    _showDialog("이메일 형식이 올바르지 않습니다.");
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
