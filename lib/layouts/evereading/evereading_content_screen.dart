import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/data/evereading/data/body.dart';
import 'package:collars_app_flutter/data/evereading/data/vocabulary.dart';
import 'package:collars_app_flutter/data/evereading/evereading.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/image/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EvereadingContentScreen extends StatefulWidget {
  static const String id = 'evereading_content_screen';
  final Evereading data;

  EvereadingContentScreen({
    Key key,
    this.data,
  }) : super(key: key);

  @override
  _EvereadingContentScreenState createState() =>
      _EvereadingContentScreenState(data: data);
}

class _EvereadingContentScreenState extends State<EvereadingContentScreen> {
  bool move = true;
  int currentPosition = 0;
  bool isKor = false;
  final Evereading data;

  _EvereadingContentScreenState({this.data});

  ScrollController _scrollController;
  GlobalKey headerKey;
  GlobalKey contentKey;
  GlobalKey vocaKey;

  @override
  void initState() {
    headerKey = GlobalKey();
    contentKey = GlobalKey();
    vocaKey = GlobalKey();
    _scrollController = ScrollController();

    _scrollController.addListener(() {
      if (move) {
        RenderBox headerBox = headerKey.currentContext.findRenderObject();
        RenderBox contentBox = contentKey.currentContext.findRenderObject();
        RenderBox vocaBox = vocaKey.currentContext.findRenderObject();
        double headerPos = headerBox.localToGlobal(Offset.zero).dy;
        double contentPos = contentBox.localToGlobal(Offset.zero).dy;
        double vocaPos = vocaBox.localToGlobal(Offset.zero).dy;

        if (vocaPos < -10) {
          setState(() {
            currentPosition = 2;
          });
        } else if (contentPos < -10) {
          setState(() {
            currentPosition = 1;
          });
        } else {
          setState(() {
            currentPosition = 0;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildBanner() {
      return Container(
        height: 400,
        padding: kPaddingSymmetric,
        decoration: BoxDecoration(
          image: data.links.introBanner != null
              ? DecorationImage(
                  image: NetworkImage(data.links.introBanner),
                  fit: BoxFit.cover,
                )
              : DecorationImage(
                  image: AssetImage(kEmptyImage),
                  fit: BoxFit.contain,
                ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              data.data.eng.title,
              style: Theme.of(context)
                  .textTheme
                  .headline2
                  .copyWith(fontSize: 22, color: white),
            ),
            SizedBox(height: 4),
            Text(
              data.data.kor.title,
              style:
                  Theme.of(context).textTheme.headline2.copyWith(color: white),
            ),
            SizedBox(height: 4),
            Text(
              data.sector.eng,
              style:
                  Theme.of(context).textTheme.bodyText1.copyWith(color: white),
            ),
          ],
        ),
      );
    }

    Widget _buildHeader({Body body}) {
      NumberFormat formatter = new NumberFormat("00");
      List _list = List();
      body.content.asMap().forEach((index, value) {
        if(value.header != null && value.header != "") {
          _list.add(SizedBox(height: 10));
          _list.add(
            Text(
              '${formatter.format(index + 1)}\t${value.header}',
              style: Theme
                  .of(context)
                  .textTheme
                  .bodyText2,
            ),
          );
        }
      });
      return Container(
        key: headerKey,
        color: Color(0xfff0f0f0),
        padding: kPaddingSymmetric,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Contents',
              style:
                  Theme.of(context).textTheme.subtitle2.copyWith(fontSize: 20),
            ),
            SizedBox(height: 2),
            ..._list,
          ],
        ),
      );
    }

    Widget _buildContent({Body body}) {
      List<Widget> _list = List();
      body.content.asMap().forEach((index, value) {
        _list.add(SizedBox(height: 20));
        if (value.header != "" && value.header != null) {
          _list.add(
            Text(
              '${index + 1}. ${value.header}',
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(fontWeight: FontWeight.bold),
            ),
          );
        }
        _list.add(SizedBox(height: 10));
        _list.add(
          Text(
            '${value.text}',
            style: Theme.of(context).textTheme.bodyText1,
          ),
        );
      });
      return Container(
        key: contentKey,
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _list,
        ),
      );
    }

    Widget _buildVoca({List<Vocabulary> voca}) {
      List<Widget> _list = List();
      voca.asMap().forEach((index, value) {
        _list.add(
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  '${index + 1}. ${value.eng}',
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
              Expanded(
                child: Text(
                  '${index + 1}. ${value.kor}',
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
            ],
          ),
        );
        _list.add(SizedBox(height: 10));
      });

      return Container(
        key: vocaKey,
        color: Color(0xfff0f0f0),
        padding: kPaddingSymmetric,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Vocabulary', style: Theme.of(context).textTheme.headline2),
            SizedBox(height: 12),
            Divider(),
            SizedBox(height: 12),
            ..._list,
          ],
        ),
      );
    }

    Widget _buildBody() {
      Body body;
      if (isKor)
        body = data.data.kor;
      else
        body = data.data.eng;
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _buildHeader(body: body),
          _buildContent(body: body),
          _buildVoca(voca: data.data.vocabulary),
        ],
      );
    }

    Widget _buildTabItem({
      Function onTap,
      String text,
      int position,
      Color active,
      Color inActive,
    }) {
      Color _color = currentPosition == position ? active : inActive;

      return InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(
                width: 1,
                color: _color,
              ),
            ),
          ),
          child: Center(
            child: Text(
              text,
              style:
                  Theme.of(context).textTheme.bodyText1.copyWith(color: _color),
            ),
          ),
        ),
      );
    }

    return Scaffold(
      appBar: DefaultAppBar(
        title: "에브리딩",
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                controller: _scrollController,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    _buildBanner(),
                    _buildBody(),
                  ],
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: kBottomNavigationBarHeight,
              child: Row(
                children: [
                  Expanded(
                    child: _buildTabItem(
                      onTap: () async {
                        setState(() {
                          move = false;
                        });
                        await _scrollController.position.ensureVisible(
                          headerKey.currentContext.findRenderObject(),
                          alignment: 0.5,
                          duration: const Duration(seconds: 1),
                        );
                        setState(() {
                          currentPosition = 0;
                          move = true;
                        });
                      },
                      text: '목차',
                      position: 0,
                      active: kBtnActive,
                      inActive: Color(0xffdfdfdf),
                    ),
                  ),
                  Expanded(
                    child: _buildTabItem(
                      onTap: () async {
                        setState(() {
                          move = false;
                        });
                        await _scrollController.position.ensureVisible(
                          contentKey.currentContext.findRenderObject(),
                          alignment: 0.5,
                          duration: const Duration(seconds: 1),
                        );
                        setState(() {
                          currentPosition = 1;
                          move = true;
                        });
                      },
                      text: '본문',
                      position: 1,
                      active: kBtnActive,
                      inActive: Color(0xffdfdfdf),
                    ),
                  ),
                  Expanded(
                    child: _buildTabItem(
                      onTap: () async {
                        setState(() {
                          move = false;
                        });
                        await _scrollController.position.ensureVisible(
                          vocaKey.currentContext.findRenderObject(),
                          alignment: 0.5,
                          duration: const Duration(seconds: 1),
                        );
                        setState(() {
                          currentPosition = 2;
                          move = true;
                        });
                      },
                      text: '핵심어휘',
                      position: 2,
                      active: kBtnActive,
                      inActive: Color(0xffdfdfdf),
                    ),
                  ),
                  Expanded(
                    child: _buildTabItem(
                      onTap: () {
                        setState(() {
                          isKor = !isKor;
                        });
                      },
                      text: isKor ? 'Eng' : 'Kor',
                      position: -1,
                      active: Color(0xffdfdfdf),
                      inActive: Color(0xffdfdfdf),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
