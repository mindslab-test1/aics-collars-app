import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/button/rect_button.dart';
import 'package:collars_app_flutter/components/button/square_button.dart';
import 'package:collars_app_flutter/components/container/intro_box.dart';
import 'package:collars_app_flutter/components/deco/icon_box.dart';
import 'package:collars_app_flutter/components/etc/rating_bar.dart';
import 'package:collars_app_flutter/data/evereading/evereading.dart';
import 'package:collars_app_flutter/layouts/evereading/evereading_content_screen.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/image/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IntroEvereading extends StatelessWidget {
  final Evereading data;

  const IntroEvereading({
    Key key,
    this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screen = MediaQuery.of(context).size;

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: DefaultAppBar(title: "Evereading", icon: kCloseSvg,),
      body: Container(
        child: Stack(
          children: [
            Positioned.fill(
              child: Container(
                decoration: BoxDecoration(
                  image: data.links.introBanner != null
                      ? DecorationImage(
                          image: NetworkImage(data.links.introBanner),
                          fit: BoxFit.cover,
                        )
                      : DecorationImage(
                          image: AssetImage(kEmptyImage),
                          fit: BoxFit.contain,
                        ),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: IntroBox(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    IconBox(
                      icon: kMicSmallSvg,
                      text: "독해,문법 학습",
                      color: kBtnActive,
                    ),
                    SizedBox(height: 8),
                    Text(
                      data.data.eng.title,
                      style: Theme.of(context)
                          .textTheme
                          .headline2
                          .copyWith(fontWeight: FontWeight.bold, fontSize: 22),
                    ),
                    SizedBox(height: 8),
                    Text(
                      data.data.kor.title,
                      style: Theme.of(context)
                          .textTheme
                          .headline2
                          .copyWith(fontWeight: FontWeight.w500, fontSize: 16),
                    ),
                    SizedBox(height: 10),
                    RatingBar(
                      rating: data.lev,
                      itemCount: 5,
                      activeColor: kStartActive,
                      inactiveColor: kStartInActive,
                    ),
                    SizedBox(height: 14),
                    Row(
                      children: [
                        SquareButton(
                          size: 48,
                          bg: kBtnActive,
                          child: Center(
                            child: SvgPicture.asset(
                              kMail,
                              color: white,
                            ),
                          ),
                        ),
                        SizedBox(width: 8),
                        Expanded(
                          child: RectButton(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) =>
                                      EvereadingContentScreen(data: data),
                                ),
                              );
                            },
                            height: 48,
                            bg: kBtnActive,
                            child: Center(
                              child: Text(
                                "Start",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .copyWith(
                                      fontWeight: FontWeight.bold,
                                      color: white,
                                    ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 8),
                    Text(
                      "혹시 궁금해왔던  주제가 있다면, COLLARS 에 신청해보세요 ~",
                      style: Theme.of(context)
                          .textTheme
                          .caption
                          .copyWith(color: kInfoColor, fontSize: 10),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
