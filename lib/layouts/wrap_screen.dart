import 'package:collars_app_flutter/layouts/auth/sign_in_screen.dart';
import 'package:collars_app_flutter/layouts/intro/consult/consult_name_screen.dart';
import 'package:collars_app_flutter/layouts/main/main_screen.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WrapScreen extends StatelessWidget {
  static const String id = 'wrap_screen';
  AuthService _authService = AuthService();

  Future<SharedPreferences> _getPrefs () async {
    return await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);

    return FutureBuilder(
      future: _getPrefs(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return Container(child: Center(child: CircularProgressIndicator()));
          default:
            SharedPreferences p = snapshot.data;
            bool _isCompany = p.getBool('isCompany') ?? false;
            int _id = p.getInt('companyId') ?? -1;
            if (_isCompany) {
              return FutureBuilder(
                future: _authService.getUserById(_id),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Container(
                          child: Center(child: CircularProgressIndicator()));
                    default:
                      if (snapshot.hasError) {
                        return Container(
                            child: Center(child: CircularProgressIndicator()));
                      } else {
                        _user.companyId = _id;
                        _user.data = snapshot.data;
                        if (_user.data.name == null) {
                          return ConsultNameScreen();
                        } else {
                          // return ConsultNameScreen();
                          return MainScreen();
                        }
                      }
                  }
                },
              );
            } else if (_user.uid != null) {
              return FutureBuilder(
                future: _authService.getUser(_user.uid),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Container(
                          child: Center(child: CircularProgressIndicator()));
                    default:
                      if (snapshot.hasError) {
                        return Container(
                            child: Center(child: CircularProgressIndicator()));
                      } else {
                        _user.data = snapshot.data;
                        if (_user.data.name == null) {
                          return ConsultNameScreen();
                        } else {
                          // return ConsultNameScreen();
                          return MainScreen();
                        }
                      }
                  }
                },
              );
            } else {
              return SignInScreen();
            }
        }
      },
    );
  }
}
