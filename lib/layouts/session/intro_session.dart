import 'dart:async';

import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/deco/icon_box.dart';
import 'package:collars_app_flutter/data/session/data/session_data.dart';
import 'package:collars_app_flutter/data/session/session.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/services/purchase.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/image/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class IntroSession extends StatefulWidget {
  final Session data;

  const IntroSession({
    Key key,
    this.data,
  }) : super(key: key);

  @override
  _IntroSessionState createState() => _IntroSessionState();
}

class _IntroSessionState extends State<IntroSession> {
  final InAppPurchaseConnection _iap = InAppPurchaseConnection.instance;
  StreamSubscription _subscription;
  bool isPurchased = false;
  List<ProductDetails> _products = [];
  List<PurchaseDetails> _purchases = [];

  PurchaseService _purchaseService = PurchaseService();

  @override
  void initState() {
    _initialize();
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  void _initialize() async {
    final _uid = Provider.of<User>(context, listen: false).uid;
    final bool isAvailable = await _iap.isAvailable();
    if (isAvailable) {
      await _getProducts();
      await _getPastPurchases();
      _verifyPurchase(_uid);

      _subscription = _iap.purchaseUpdatedStream.listen((data) {
        _purchases.addAll(data);
        _verifyPurchase(_uid);
      });
    }
  }

  Future<void> _getProducts() async {
    Set<String> ids = Set.from([widget.data.price]);
    ProductDetailsResponse res = await _iap.queryProductDetails(ids);
    setState(() {
      _products = res.productDetails;
    });
  }

  Future<void> _getPastPurchases() async {
    QueryPurchaseDetailsResponse res = await _iap.queryPastPurchases();
    setState(() {
      _purchases = res.pastPurchases;
    });
  }

  Future _buyProducts() async {
    if (!isPurchased && _products.length > 0) {
      final PurchaseParam purchaseParam =
          PurchaseParam(productDetails: _products[0]);
      await _iap.buyConsumable(purchaseParam: purchaseParam, autoConsume: true);
    }
  }

  PurchaseDetails _hasPurchased(String productID) {
    return _purchases.firstWhere((purchase) {
      return purchase.productID == productID;
    }, orElse: () => null);
  }

  void _verifyPurchase(String uid) async {
    PurchaseDetails purchase = _hasPurchased(widget.data.price);
    int id = Provider.of<User>(context, listen: false).companyId;
    int code = 0;
    if (purchase != null && purchase.status == PurchaseStatus.purchased) {

      code = await _purchaseService.purchaseSession(
        id: id,
        uid: uid,
        sessionId: widget.data.id,
        expiration: widget.data.data.expiration,
        purchaseToken: purchase.billingClientPurchase.purchaseToken,
      );
    } else {
      code = await _purchaseService.purchaseSession(
        id: id,
        uid: uid,
        sessionId: widget.data.id,
        expiration: widget.data.data.expiration,
        purchaseToken: null,
      );
    }

    switch (code) {
      case 2000:
        setState(() {
          isPurchased = true;
        });
        await Provider.of<User>(context, listen: false).loadUser();
        break;
      case 4900:
        setState(() {
          isPurchased = true;
        });
        break;
      case 4901:
        setState(() {
          isPurchased = false;
        });
        break;
      default:
        break;
    }

    if (code == 2000 || code == 4900) {
      setState(() {
        isPurchased = true;
      });
    } else if (code == 4901) {
      setState(() {
        isPurchased = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final Data session = widget.data.data;
    final formatCurrency = new NumberFormat.decimalPattern();

    print(session.expiration);

    Widget _introBox({Widget child}) {
      return Container(
        padding: kPaddingAll,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(32),
            topRight: Radius.circular(32),
          ),
        ),
        child: child,
      );
    }

    List<Widget> buildGoal() {
      List<Widget> _list = List();
      session.goal.forEach((goal) {
        _list.add(SizedBox(height: 8));
        _list.add(
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SvgPicture.asset(kCheckedBoxSvg),
              SizedBox(width: 8),
              Expanded(
                child: Text(
                  goal,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(fontSize: 12),
                ),
              ),
            ],
          ),
        );
      });

      return _list;
    }

    Widget buildEffectItem({int score, String text}) {
      double _height = (score * 10).toDouble();
      return Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            '+$score',
            style: Theme.of(context)
                .textTheme
                .subtitle2
                .copyWith(color: kBtnActive, fontSize: 12),
          ),
          Container(
            width: 20,
            height: _height,
            color: kBtnActive,
          ),
          Text(
            text,
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ],
      );
    }

    Widget buildEffect() {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: buildEffectItem(score: session.effect.context, text: "맥락"),
          ),
          Expanded(
            child: buildEffectItem(score: session.effect.voca, text: "어휘"),
          ),
          Expanded(
            child: buildEffectItem(score: session.effect.tone, text: "톤앤매너"),
          ),
          Expanded(
            child:
                buildEffectItem(score: session.effect.knowledge, text: "문화지식"),
          ),
          Expanded(
            child: buildEffectItem(score: session.effect.grammar, text: "문법"),
          )
        ],
      );
    }

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: DefaultAppBar(title: "Session"),
      body: Container(
        child: Stack(
          children: [
            Positioned(
              child: Container(
                width: double.infinity,
                height: 500,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                    image: widget.data.links.thumbnail != null
                        ? NetworkImage(widget.data.links.thumbnail)
                        : AssetImage(kEmptyImage),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Positioned(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(height: 450),
                    _introBox(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              IconBox(
                                icon: kClockSvg,
                                text: '',
                                // "${data.learningTime} min",
                                color: kBtnActive,
                              ),
                              SizedBox(width: 4),
                              IconBox(
                                icon: kCalendarSvg,
                                text: "60 days",
                                color: kBtnActive,
                              )
                            ],
                          ),
                          SizedBox(height: 8),
                          Text(
                            session.title,
                            style: Theme.of(context).textTheme.headline2,
                          ),
                          SizedBox(height: 8),
                          Text(
                            _products.length > 0 ? '${_products[0].price}' : '',
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                                .copyWith(fontSize: 20),
                          ),
                          Text(
                            '번들 및 월 정기구독권 준비중입니다.',
                            style: Theme.of(context)
                                .textTheme
                                .caption
                                .copyWith(color: gray4),
                          ),
                          SizedBox(height: 16),
                          Text(
                            '세션소개',
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                          Text(
                            '${session.introduction}',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(fontSize: 12),
                          ),
                          SizedBox(height: 16),
                          Text(
                            '학습목표',
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                          ...buildGoal(),
                          SizedBox(height: 16),
                          Text(
                            '학습구성',
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                          SizedBox(height: 12),
                          Text(
                            '1. CLASS',
                            style: Theme.of(context)
                                .textTheme
                                .subtitle2
                                .copyWith(fontSize: 14),
                          ),
                          SizedBox(height: 8),
                          Text(
                            '영상강의 [${session.section.video.time} min]',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(color: kBtnActive, fontSize: 12),
                          ),
                          Text(
                            '${session.section.video.title}',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(fontSize: 12),
                          ),
                          SizedBox(height: 4),
                          Text(
                            'QUIZ [${session.section.quiz.time} min]',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(color: kBtnActive, fontSize: 12),
                          ),
                          Text(
                            '${session.section.quiz.title}',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(fontSize: 12),
                          ),
                          SizedBox(height: 12),
                          // Text(
                          //   '2. PRACTICE',
                          //   style: Theme.of(context)
                          //       .textTheme
                          //       .subtitle2
                          //       .copyWith(fontSize: 14),
                          // ),
                          // SizedBox(height: 8),
                          // ...buildPractice(),
                          // SizedBox(height: 16),
                          Text(
                            '기대효과',
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                          SizedBox(height: 12),
                          buildEffect(),
                          SizedBox(height: 24),
                          InkWell(
                            onTap: () async {
                              if (!isPurchased) {
                                await _buyProducts();
                              }
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(vertical: 15),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: isPurchased ? kBtnInActive : kBtnActive,
                              ),
                              child: Center(
                                child: Text(
                                  '세션 시작하기',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      .copyWith(color: white),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
