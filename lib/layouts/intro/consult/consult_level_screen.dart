import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/body/ai_manager.dart';
import 'package:collars_app_flutter/components/builder/exam/exam_choice.dart';
import 'package:collars_app_flutter/components/builder/exam/exam_highlight.dart';
import 'package:collars_app_flutter/components/builder/exam/exam_puzzle.dart';
import 'package:collars_app_flutter/components/builder/exam/exam_short_answer.dart';
import 'package:collars_app_flutter/components/builder/exam/exam_speaking.dart';
import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/components/button/later_button.dart';
import 'package:collars_app_flutter/components/container/TitleContainer.dart';
import 'package:collars_app_flutter/data/exam/exam.dart';
import 'package:collars_app_flutter/data/exam/exam_data.dart';
import 'package:collars_app_flutter/data/exam/exam_result.dart';
import 'package:collars_app_flutter/data/user/user_exam.dart';
import 'package:collars_app_flutter/layouts/intro/consult/consult_level_result_screen.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/services/exam.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConsultLevelScreen extends StatefulWidget {
  static const String id = 'consult_level_screen';

  @override
  _ConsultLevelScreenState createState() => _ConsultLevelScreenState();
}

class _ConsultLevelScreenState extends State<ConsultLevelScreen> {
  ExamService _examService = ExamService();
  int _examId;
  List<ExamData> _examData;
  List<ExamResult> _examResult;
  List<bool> _nextButtonController;
  int _pageLength;

  PageController _pageController = PageController(
    initialPage: 0,
  );

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    _fetchData();
  }

  Future _fetchData() async {
    Exam _data = await _examService.getLevelExam(id: 0);
    setState(() {
      _examId = _data.id;
      _examData = _data.data;
      _examResult = List.filled(_examData.length, null);
      _nextButtonController = List.filled(_examData.length, false);
      _pageLength = _examData.length + 2;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget buildIntro() {
      String _userName = Provider.of<User>(context, listen: false).data.name;
      String text = "$_userName 님, 안녕하세요.\n진단테스트를 통해 당신에게 딱 맞는\n학습설계를 도와드릴게요.";
      return AiManager(
        image: kAvatarMary,
        imageSize: 50.0,
        text: text,
      );
    }

    Widget buildOutro() {
      String _userName = Provider.of<User>(context, listen: false).data.name;
      String text =
          "$_userName 님,\n모든 진단테스트가 완료되었습니다.\n\n테스트를 바탕으로 칼라스AI가 $_userName 님에게 딱 맞는 학습을 분석중입니다.\n\n잠시만 기다려주세요.";
      return AiManager(
        image: kAvatarMary,
        imageSize: 50.0,
        text: text,
      );
    }

    Widget buildBody(int index) {
      if (index == 0) {
        return buildIntro();
      } else if (index == _pageLength - 1) {
        return buildOutro();
      } else {
        int _dataIndex = index - 1;
        ExamData _data;
        if (_dataIndex < _examData.length)
          _data = _examData[_dataIndex];
        else
          _data = null;

        String _type = _data.type.toUpperCase();
        switch (_type) {
          case CHOICE:
            return TitleContainer(
              text: "다음 보기 중 빈칸에 가장 적절한 어휘를 골라주세요.",
              child: ExamChoice(
                onChanged: (value) {
                  setState(() {
                    ExamResult _result = ExamResult();
                    _result.type = CHOICE;
                    _result.result = value;
                    _examResult[_dataIndex] = _result;
                    _nextButtonController[_dataIndex] = true;
                  });
                },
                data: _data,
              ),
            );
          case PUZZLE:
            return TitleContainer(
              text: "다음의 한글 문장과 같은 의미가 되도록 문장의 각 문단을 선택해 문장을 완성해주세요.",
              child: ExamPuzzle(
                onChanged: (value) {
                  setState(() {
                    ExamResult _result = ExamResult();
                    _result.type = PUZZLE;
                    _result.result = value;
                    _examResult[_dataIndex] = _result;
                    if (_data.answer.length == value.length) {
                      _nextButtonController[_dataIndex] = true;
                    } else {
                      _nextButtonController[_dataIndex] = false;
                    }
                  });
                },
                data: _data,
              ),
            );
          case SPEAKING:
            return TitleContainer(
              text: _data.question,
              child: ExamSpeaking(
                onChanged: (value) {
                  setState(() {
                    ExamResult _result = ExamResult();
                    _result.type = SPEAKING;
                    _result.result = value;
                    _examResult[_dataIndex] = _result;
                    if (value != null) {
                      _nextButtonController[_dataIndex] = true;
                    } else {
                      _nextButtonController[_dataIndex] = false;
                    }
                  });
                },
                data: _data,
              ),
            );
          case SHORT_ANSWER:
            return TitleContainer(
              text: _data.question,
              child: ExamShortAnswer(
                data: _data,
                onChanged: (value) {
                  setState(() {
                    ExamResult _result = ExamResult();
                    _result.type = SHORT_ANSWER;
                    _result.result = value;
                    _examResult[_dataIndex] = _result;
                    if (value != null) {
                      _nextButtonController[_dataIndex] = true;
                    } else {
                      _nextButtonController[_dataIndex] = false;
                    }
                  });
                },
              ),
            );
          case HIGHLIGHT:
            return TitleContainer(
              text: _data.question,
              child: ExamHighlight(
                onChanged: (value) {
                  setState(() {
                    ExamResult _result = ExamResult();
                    _result.type = HIGHLIGHT;
                    _result.result = value;
                    _examResult[_dataIndex] = _result;
                    if (value != null) {
                      _nextButtonController[_dataIndex] = true;
                    } else {
                      _nextButtonController[_dataIndex] = false;
                    }
                  });
                },
                data: _data,
              ),
            );
        }
      }
    }

    List<Widget> buildFooter(int index) {
      List<Widget> _list = new List();
      _list.add(SizedBox(height: 12));
      if (index == 0) {
        _list.add(CapsuleContrastButton(
          type: FIXED,
          isActive: true,
          onTap: () async {
            _pageController.jumpToPage(index + 1);
          },
          padding: kPaddingBtnVertical,
          primaryColor: kSignInBtn1,
          secondaryColor: kSignInBtn2,
          text: "진단테스트 시작하기",
        ));
      } else if (index == _pageLength - 1) {
        _list.add(
          CapsuleContrastButton(
            type: FIXED,
            isActive: true,
            onTap: () async {
              UserExam _result = await _examService.uploadExamResult(
                id: Provider.of<User>(context, listen: false).companyId,
                uid: Provider.of<User>(context, listen: false).uid,
                type: LEVEL,
                examId: _examId,
                result: _examResult,
              );
              if (_result != null) {
                await Provider.of<User>(context, listen: false).loadUser();
                await Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (_) => ConsultLevelResultScreen(data: _result)),
                );
              }
            },
            padding: kPaddingBtnVertical,
            primaryColor: kSignInBtn1,
            secondaryColor: kSignInBtn2,
            text: "진단테스트 결과보기",
          ),
        );
      } else {
        int _dataIndex = index - 1;
        _list.add(
          CapsuleContrastButton(
            type: ACTIVATE,
            isActive: _nextButtonController[_dataIndex],
            onTap: () async {
              FocusScope.of(context).unfocus();
              _pageController.jumpToPage(index + 1);
            },
            padding: kPaddingBtnVertical,
            primaryColor: kBtnActive,
            text: "Next",
          ),
        );
      }
      _list.add(SizedBox(height: 20));
      _list.add(LaterButton());
      _list.add(SizedBox(height: 36));
      return _list;
    }

    return Scaffold(
      appBar: DefaultAppBar(
        title: "진단테스트",
      ),
      body: Container(
        padding: kPaddingHorizontal,
        child: _examData != null
            ? PageView.builder(
                controller: _pageController,
                physics: NeverScrollableScrollPhysics(),
                itemCount: _pageLength,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      SizedBox(height: 24),
                      Expanded(
                        child: SingleChildScrollView(
                          child: buildBody(index),
                        ),
                      ),
                      ...buildFooter(index),
                    ],
                  );
                },
              )
            : Container(child: Center(child: CircularProgressIndicator())),
      ),
    );
  }
}
