import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/body/ai_manager.dart';
import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/components/button/checkbox_button.dart';
import 'package:collars_app_flutter/data/survey_data.dart';
import 'package:collars_app_flutter/data/survey_result.dart';
import 'package:collars_app_flutter/layouts/intro/consult/consult_level_screen.dart';
import 'package:collars_app_flutter/layouts/wrap_screen.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/services/survey.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConsultSurveyScreen extends StatefulWidget {
  static const String id = 'consult_survey_screen';

  @override
  _ConsultSurveyScreenState createState() => _ConsultSurveyScreenState();
}

class _ConsultSurveyScreenState extends State<ConsultSurveyScreen> {
  SurveyService _surveyService = SurveyService();
  List<SurveyData> _surveyData;
  List<SurveyResult> _surveyResult = new List();
  int _pageLength;

  PageController _pageController = PageController(
    initialPage: 0,
  );

  @override
  void initState() {
    _fetchData();
    super.initState();
  }

  Future _fetchData() async {
    List<SurveyData> _data = await _surveyService.getSurvey();
    setState(() {
      _surveyData = _data;
      _surveyResult = getSurveyResultForm(_data);
      _pageLength = (_surveyData.length * 2) + 2;
    });
  }

  List<SurveyResult> getSurveyResultForm(List<SurveyData> dataList) {
    List<SurveyResult> _list = new List();
    dataList.forEach((data) {
      SurveyResult _sr = SurveyResult();
      _sr.question = data.id;
      _sr.answer = data.answers[0].id;
      _list.add(_sr);
    });
    return _list;
  }

  @override
  Widget build(BuildContext context) {
    Widget buildAiManager(String text) {
      return AiManager(
        image: kAvatarMary,
        imageSize: 50.0,
        text: text,
      );
    }

    Widget buildSurveyQuestion(SurveyData data, int index) {
      List<Widget> _list = List();
      data.answers.forEach((answer) {
        _list.add(
          GestureDetector(
            onTap: () {
              setState(() {
                _surveyResult[index].answer = answer.id;
              });
            },
            child: CheckBoxButton(
              isActive: _surveyResult[index].answer == answer.id ? true : false,
              padding: EdgeInsets.symmetric(vertical: 14),
              text: answer.answer,
              color: _surveyResult[index].answer == answer.id
                  ? kBtnActive
                  : kBtnInActive,
            ),
          ),
        );
      });

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${data.question}',
            style: Theme.of(context).textTheme.headline2,
          ),
          SizedBox(height: 36),
          SingleChildScrollView(
            child: Container(
              padding: kPaddingHorizontal,
              child: Column(
                children: _list,
              ),
            ),
          )
        ],
      );
    }

    Widget buildSurveyAnswer(SurveyData data, int index) {
      String _text;
      data.answers.forEach((answer) {
        if (answer.id == _surveyResult[index].answer) {
          _text = answer.response;
        }
      });
      return AiManager(
        image: kAvatarMary,
        imageSize: 50.0,
        text: _text,
      );
    }

    Widget buildBody(int index) {
      String _userName = Provider.of<User>(context, listen: false).data.name;
      if (index == 0) {
        String _startText =
            "$_userName 님, 반갑습니다.\n딱 맞는 학습을 설계해 드리기 위해서 $_userName 님에게\n몇 가지 질문을 드려볼게요.\nDon’t worry. I’ll keep it quick :-)";
        return buildAiManager(_startText);
      } else if (index == _pageLength - 1) {
        String _endText =
            "$_userName 님,\n답변해주시느라 고생 많으셨어요.\n추가적인 진단테스트를 통해\n당신에게 딱 맞는 학습설계를\n도와드릴 수 있습니다.";
        return buildAiManager(_endText);
      } else {
        if (index % 2 == 0) {
          int _dataIndex = (index ~/ 2 - 1);
          return buildSurveyAnswer(
            _surveyData[_dataIndex],
            _dataIndex,
          );
        } else {
          int _dataIndex = (index - 1) ~/ 2;
          return buildSurveyQuestion(
            _surveyData[_dataIndex],
            _dataIndex,
          );
        }
      }
    }

    return Scaffold(
      appBar: DefaultAppBar(
        title: "학습설계",
      ),
      body: SafeArea(
        child: Container(
          padding: kPaddingHorizontal,
          child: _surveyData != null
              ? PageView.builder(
                  controller: _pageController,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: _pageLength,
                  itemBuilder: (context, index) {
                    String _buttonText;
                    if (index == _pageLength - 1) {
                      _buttonText = "진단테스트 받으러가기";
                    } else {
                      _buttonText = "Next";
                    }
                    return Column(
                      children: [
                        Expanded(
                          child: buildBody(index),
                        ),
                        SizedBox(height: 12),
                        CapsuleContrastButton(
                          type: FIXED,
                          isActive: true,
                          onTap: () async {
                            if (index >= _pageLength - 1) {
                              bool isSuccess =
                                  await _surveyService.uploadSurveyResult(
                                id: Provider.of<User>(context, listen: false)
                                    .companyId,
                                uid: Provider.of<User>(context, listen: false)
                                    .uid,
                                result: _surveyResult,
                              );
                              if (isSuccess) {
                                await Provider.of<User>(context, listen: false)
                                    .loadUser();
                                await Navigator.pushReplacementNamed(
                                    context, ConsultLevelScreen.id);
                              }
                            } else {
                              _pageController.jumpToPage(index + 1);
                            }
                          },
                          padding: kPaddingBtnVertical,
                          primaryColor: kSignInBtn1,
                          secondaryColor: kSignInBtn2,
                          text: _buttonText,
                        ),
                        SizedBox(height: 20),
                        Container(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pushReplacementNamed(
                                  context, WrapScreen.id);
                            },
                            child: Text(
                              '다음에 할게요',
                              style: Theme.of(context)
                                  .textTheme
                                  .caption
                                  .copyWith(color: kInfoColor),
                            ),
                          ),
                        ),
                        SizedBox(height: 36),
                      ],
                    );
                  },
                )
              : Container(child: Center(child: CircularProgressIndicator())),
        ),
      ),
    );
  }
}
