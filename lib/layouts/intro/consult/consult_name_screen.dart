import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/body/ai_manager.dart';
import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/components/container/TextFieldContainer.dart';
import 'package:collars_app_flutter/components/form/custom_text_field.dart';
import 'package:collars_app_flutter/layouts/intro/consult/consult_survey_screen.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/services/auth.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';



class ConsultNameScreen extends StatefulWidget {
  static const String id = 'consult_name_screen';

  @override
  _ConsultNameScreenState createState() => _ConsultNameScreenState();
}

class _ConsultNameScreenState extends State<ConsultNameScreen> {
  AuthService _authService = AuthService();
  String text =
      "안녕하세요.\n저는 칼라스 AI의 학습매니저\n마리 칼라스(Marie Collars)입니다.\n편하게 ‘마리’라고 불러주세요:-)\n저는 어떻게 부르면 좋을까요?";
  bool isName = false;
  String name = "";

  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);


    return Scaffold(
      appBar: DefaultAppBar(
        title: "학습설계",
      ),
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
          },
          child: Container(
            padding: kPaddingHorizontal,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(height: 24),
                Expanded(
                  child: SingleChildScrollView(
                    child: AiManager(
                      image: kAvatarMary,
                      imageSize: 50.0,
                      text: text,
                      child: TextFieldContainer(
                        isActive: isName,
                        activeColor: kBtnActive,
                        inActiveColor: kBtnInActive,
                        child: CustomTextField(
                          onChanged: (value) {
                            setState(() {
                              name = value;
                              if (name == null || name == "")
                                isName = false;
                              else
                                isName = true;
                            });
                          },
                          icon: kUserSvg,
                          activeColor: kBtnActive,
                          inactiveColor: kBtnInActive,
                          hintText: "이름 또는 별명",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 12),
                CapsuleContrastButton(
                  type: FIXED,
                  isActive: true,
                  onTap: () async {
                    if (name == null || name == "") {
                      // todo: Alert Error
                    } else {
                      bool _isSuccess = await _authService.updateUserName(
                        id: _user.companyId,
                        uid: _user.uid,
                        name: name,
                      );
                      if (_isSuccess) {
                        await Provider.of<User>(context, listen: false).loadUser();
                        await Navigator.pushReplacementNamed(
                            context, ConsultSurveyScreen.id);
                      }
                    }
                  },
                  padding: kPaddingBtnVertical,
                  primaryColor: kSignInBtn1,
                  secondaryColor: kSignInBtn2,
                  text: "학습설계 시작하기",
                ),
                SizedBox(height: 24),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
