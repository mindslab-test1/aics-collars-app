import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/builder/session/suggestion_carousel.dart';
import 'package:collars_app_flutter/components/deco/linear_chart_box.dart';
import 'package:collars_app_flutter/components/deco/radar_chart_box.dart';
import 'package:collars_app_flutter/components/deco/suggestion_box.dart';
import 'package:collars_app_flutter/data/user/user_exam.dart';
import 'package:collars_app_flutter/layouts/wrap_screen.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:collars_app_flutter/styles/text/default.dart';
import 'package:flutter/material.dart';

class ConsultLevelResultScreen extends StatelessWidget {
  static const String id = 'consult_level_result_screen';
  final UserExam data;

  const ConsultLevelResultScreen({
    Key key,
    this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "진단테스트 분석결과",
        leading: () {
          Navigator.pushReplacementNamed(context, WrapScreen.id);
        },
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: kPaddingHorizontal,
          child: Column(
            children: [
              SizedBox(height: 24),
              Center(
                child: Text(
                  '영어 역량 지수',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              ),
              Center(
                child: Text(
                  'Capability Index',
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: gray4),
                ),
              ),
              SizedBox(height: 24),
              RadarChartBox(size: 250, data: data.data),
              SizedBox(height: 20),
              LinearChartBox(data: data.data),
              SizedBox(height: 24),
              SuggestionBox(),
              SizedBox(height: 24),
              SuggestionCarousel(),
              SizedBox(height: 48),
              Text(
                kCopyright,
                style:
                    Theme.of(context).textTheme.caption.copyWith(color: black),
              ),
              SizedBox(height: 60),
            ],
          ),
        ),
      ),
    );
  }
}
