import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/body/ai_manager.dart';
import 'package:collars_app_flutter/components/builder/exam/exam_choice.dart';
import 'package:collars_app_flutter/components/builder/exam/exam_highlight.dart';
import 'package:collars_app_flutter/components/builder/exam/exam_puzzle.dart';
import 'package:collars_app_flutter/components/builder/exam/exam_short_answer.dart';
import 'package:collars_app_flutter/components/builder/exam/exam_speaking.dart';
import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/components/button/later_button.dart';
import 'package:collars_app_flutter/components/container/banner_container.dart';
import 'package:collars_app_flutter/components/container/custom_safe_area.dart';
import 'package:collars_app_flutter/data/exam/exam.dart';
import 'package:collars_app_flutter/data/exam/exam_data.dart';
import 'package:collars_app_flutter/data/exam/exam_result.dart';
import 'package:collars_app_flutter/data/user/user_exam.dart';
import 'package:collars_app_flutter/layouts/wrap_screen.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/services/exam.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SessionExam extends StatefulWidget {
  final Exam data;

  const SessionExam({
    Key key,
    this.data,
  }) : super(key: key);

  @override
  _SessionExamState createState() => _SessionExamState(exams: data.data);
}

class _SessionExamState extends State<SessionExam> {
  ExamService _examService = ExamService();

  final List<ExamData> exams;
  List<ExamResult> _examResult;
  List<bool> _nextButtonController;
  int _pageLength;

  PageController _pageController = PageController(
    initialPage: 0,
  );

  _SessionExamState({this.exams});

  @override
  void initState() {
    _fetchData();
  }

  void _fetchData() {
    setState(() {
      _examResult = List.filled(exams.length, null);
      _nextButtonController = List.filled(exams.length, false);
      _pageLength = exams.length + 2;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget buildIntro() {
      String _userName = Provider.of<User>(context, listen: false).data.name;
      String text = "$_userName 님, 안녕하세요.\n진단테스트를 통해 당신에게 딱 맞는\n학습설계를 도와드릴게요.";
      return Container(
        padding: kPaddingHorizontal,
        child: Column(
          children: [
            CustomSafeArea(),
            AiManager(
              image: kAvatarMary,
              imageSize: 50.0,
              text: text,
            ),
          ],
        ),
      );
    }

    Widget buildOutro() {
      String _userName = Provider.of<User>(context, listen: false).data.name;
      String text =
          "$_userName 님,\n벌써 퀴즈까지 모두 완료하셨네요!\n학습 열정이 대단하세요.\n\n다음 단계는 세션 주제에 맞춘 상황에서 학습자님이 대화 상대방의 한 측을 맡아, 영어소통을 연습하는 시뮬레이션 트레이닝 이에요.\n\n마치 실전 현장처럼 연습해보시길 추천해요!";
      return Container(
        padding: kPaddingHorizontal,
        child: Column(
          children: [
            CustomSafeArea(),
            AiManager(
              image: kAvatarMary,
              imageSize: 50.0,
              text: text,
            ),
          ],
        ),
      );
    }

    Widget buildHeader({int index, int length, String text}) {
      return BannerContainer(
        child: Container(
          padding: kPaddingHorizontal,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomSafeArea(),
              Text(
                '${index}/${length}',
                style: Theme.of(context)
                    .textTheme
                    .subtitle1
                    .copyWith(fontSize: 14, color: white),
              ),
              SizedBox(height: 12),
              Text(
                text,
                style: Theme.of(context)
                    .textTheme
                    .subtitle2
                    .copyWith(fontSize: 18, color: white),
              ),
              SizedBox(height: 36),
            ],
          ),
        ),
      );
    }

    Widget buildBody(int index) {
      if (index == 0) {
        return buildIntro();
      } else if (index == exams.length + 1) {
        return buildOutro();
      } else {
        int _dataIndex = index - 1;
        ExamData _data = exams[_dataIndex];
        switch (_data.type.toUpperCase()) {
          case CHOICE:
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                buildHeader(
                    index: index,
                    length: exams.length,
                    text: "다음 보기 중 빈칸에 가장 적절한 어휘를 골라주세요."),
                Container(
                  padding: kPaddingHorizontal,
                  child: ExamChoice(
                    data: _data,
                    onChanged: (value) {
                      setState(() {
                        ExamResult _result = ExamResult();
                        _result.type = CHOICE;
                        _result.result = value;
                        _examResult[_dataIndex] = _result;
                        _nextButtonController[_dataIndex] = true;
                      });
                    },
                  ),
                ),
              ],
            );
          case PUZZLE:
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                buildHeader(
                  index: index,
                  length: exams.length,
                  text: "다음의 지문과 같은 의미가 되도록 문장의 각 문단을 선택해 아래 빈칸을 채워주세요.",
                ),
                Container(
                  padding: kPaddingHorizontal,
                  child: ExamPuzzle(
                    data: _data,
                    onChanged: (value) {
                      setState(() {
                        ExamResult _result = ExamResult();
                        _result.type = PUZZLE;
                        _result.result = value;
                        _examResult[_dataIndex] = _result;
                        if (_data.answer.length == value.length) {
                          _nextButtonController[_dataIndex] = true;
                        } else {
                          _nextButtonController[_dataIndex] = false;
                        }
                      });
                    },
                  ),
                )
              ],
            );
          case SPEAKING:
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                buildHeader(
                  index: index,
                  length: exams.length,
                  text: _data.question,
                ),
                Container(
                  padding: kPaddingHorizontal,
                  child: ExamSpeaking(
                    data: _data,
                    onChanged: (value) {
                      setState(() {
                        ExamResult _result = ExamResult();
                        _result.type = SPEAKING;
                        _result.result = value;
                        _examResult[_dataIndex] = _result;
                        if (value != null) {
                          _nextButtonController[_dataIndex] = true;
                        } else {
                          _nextButtonController[_dataIndex] = false;
                        }
                      });
                    },
                  ),
                )
              ],
            );
          case SHORT_ANSWER:
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                buildHeader(
                  index: index,
                  length: exams.length,
                  text: _data.question,
                ),
                ExamShortAnswer(
                  data: _data,
                  onChanged: (value) {
                    setState(() {
                      ExamResult _result = ExamResult();
                      _result.type = SHORT_ANSWER;
                      _result.result = value;
                      _examResult[_dataIndex] = _result;
                      if (value != null) {
                        _nextButtonController[_dataIndex] = true;
                      } else {
                        _nextButtonController[_dataIndex] = false;
                      }
                    });
                  },
                ),
              ],
            );
          case HIGHLIGHT:
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                buildHeader(
                  index: index,
                  length: exams.length,
                  text: _data.question,
                ),
                ExamHighlight(
                  data: _data,
                  onChanged: (value) {
                    setState(() {
                      ExamResult _result = ExamResult();
                      _result.type = HIGHLIGHT;
                      _result.result = value;
                      _examResult[_dataIndex] = _result;
                      if (value != null) {
                        _nextButtonController[_dataIndex] = true;
                      } else {
                        _nextButtonController[_dataIndex] = false;
                      }
                    });
                  },
                ),
              ],
            );
        }
      }
    }

    List<Widget> buildFooter(int index) {
      List<Widget> _list = new List();
      _list.add(SizedBox(height: 12));
      if (index == 0) {
        _list.add(
          Container(
            padding: kPaddingHorizontal,
            child: CapsuleContrastButton(
              type: FIXED,
              isActive: true,
              onTap: () async {
                _pageController.jumpToPage(index + 1);
              },
              padding: kPaddingBtnVertical,
              primaryColor: kSignInBtn1,
              secondaryColor: kSignInBtn2,
              text: "Next",
            ),
          ),
        );
      } else if (index == _pageLength - 1) {
        _list.add(
          Container(
            padding: kPaddingHorizontal,
            child: CapsuleContrastButton(
              type: FIXED,
              isActive: true,
              onTap: () async {
                UserExam _result = await _examService.uploadExamResult(
                  id: Provider.of<User>(context, listen: false).companyId,
                  uid: Provider.of<User>(context, listen: false).uid,
                  type: SESSION,
                  examId: widget.data.id,
                  result: _examResult,
                );
                Navigator.pushReplacementNamed(context, WrapScreen.id);
              },
              padding: kPaddingBtnVertical,
              primaryColor: kSignInBtn1,
              secondaryColor: kSignInBtn2,
              text: "Home",
            ),
          ),
        );
      } else {
        int _dataIndex = index - 1;
        _list.add(
          Container(
            padding: kPaddingHorizontal,
            child: CapsuleContrastButton(
              type: ACTIVATE,
              isActive: _nextButtonController[_dataIndex],
              onTap: () async {
                bool isCorrect = false;
                switch (_examResult[_dataIndex].type) {
                  case CHOICE:
                    if (exams[_dataIndex].answer[0] ==
                        _examResult[_dataIndex].result) {
                      isCorrect = true;
                    } else {
                      isCorrect = false;
                    }
                    break;
                  case PUZZLE:
                    Function eq = const ListEquality().equals;
                    isCorrect = eq(_examResult[_dataIndex].result,
                        exams[_dataIndex].answer);
                    break;
                  case SHORT_ANSWER:
                    if (exams[_dataIndex].answer[0] ==
                        _examResult[_dataIndex].result) {
                      isCorrect = true;
                    } else {
                      isCorrect = false;
                    }
                    break;
                }

                Widget correctButtons() {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Review Lesson",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              .copyWith(color: gray4),
                        ),
                      ),
                      SizedBox(width: 24),
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          FocusScope.of(context).unfocus();
                          _pageController.jumpToPage(index + 1);
                        },
                        child: Text(
                          "Next",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              .copyWith(color: kCorrect),
                        ),
                      ),
                    ],
                  );
                }

                Widget wrongButtons() {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          FocusScope.of(context).unfocus();
                          _pageController.jumpToPage(index + 1);
                        },
                        child: Text(
                          "Next",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              .copyWith(color: gray4),
                        ),
                      ),
                      SizedBox(width: 24),
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Try Again",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              .copyWith(color: kWrong),
                        ),
                      ),
                    ],
                  );
                }

                showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    contentPadding: EdgeInsets.all(0.0),
                    content: Container(
                      height: 213,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 69,
                            color: isCorrect ? kCorrect : kWrong,
                            child: Center(
                              child: Text(
                                isCorrect ? "Correct" : "Wrong",
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .copyWith(fontSize: 16, color: white),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Center(
                              child: Text(
                                isCorrect ? "정답입니다" : "아쉽네요\n다시한번 도전해 보세요",
                                textAlign: TextAlign.center,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .copyWith(fontSize: 16),
                              ),
                            ),
                          ),
                          Container(
                            padding: kPaddingHorizontal,
                            child:
                                isCorrect ? correctButtons() : wrongButtons(),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
                // FocusScope.of(context).unfocus();
                // _pageController.jumpToPage(index + 1);
              },
              padding: kPaddingBtnVertical,
              primaryColor: kBtnActive,
              text: "Ok",
            ),
          ),
        );
      }
      _list.add(SizedBox(height: 20));
      _list.add(LaterButton());
      _list.add(SizedBox(height: 36));
      return _list;
    }

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: DefaultAppBar(
        title: "QUIZ",
        color: white,
      ),
      body: exams == null
          ? Container(child: Center(child: CircularProgressIndicator()))
          : PageView.builder(
              controller: _pageController,
              physics: NeverScrollableScrollPhysics(),
              itemCount: _pageLength,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Expanded(
                      child: SingleChildScrollView(child: buildBody(index)),
                    ),
                    ...buildFooter(index),
                  ],
                );
              },
            ),
    );
  }
}
