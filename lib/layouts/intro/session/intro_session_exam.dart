import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/data/exam/exam.dart';
import 'package:collars_app_flutter/layouts/intro/session/session_exam.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';

class IntroSessionExam extends StatelessWidget {
  static const String id = 'intro_session_exam';

  final Exam data;

  const IntroSessionExam({
    Key key,
    this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: DefaultAppBar(title: "",color: white,),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Container(
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                gradient: kDefaultGradient,
              ),
              child: Text(
                "QUIZ",
                style: Theme.of(context)
                    .textTheme
                    .headline2
                    .copyWith(fontSize: 60, color: white),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: kPaddingHorizontal,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 12),
                  Text('4 Question'),
                  Expanded(child: Container()),
                  CapsuleContrastButton(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => SessionExam(data: data)),
                      );
                    },
                    type: FIXED,
                    padding: EdgeInsets.symmetric(vertical: 16),
                    isActive: true,
                    text: "Start",
                    primaryColor: kBtnActive,
                  ),
                  SizedBox(height: 20),
                  SizedBox(height: 36),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
