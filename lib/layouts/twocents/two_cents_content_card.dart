import 'package:audioplayers/audioplayers.dart';
import 'package:collars_app_flutter/data/twoCents/data/content.dart';
import 'package:collars_app_flutter/providers/twocents_provider.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class TwoCentContentCard extends StatefulWidget {
  final Content data;
  final Function onMic;

  const TwoCentContentCard({
    Key key,
    this.data,
    this.onMic,
  }) : super(key: key);

  @override
  _TwoCentContentCardState createState() =>
      _TwoCentContentCardState(data: data);
}

class _TwoCentContentCardState extends State<TwoCentContentCard> {
  static String _mediaServer = DotEnv().env['MEDIA_SERVER'];
  final Content data;
  bool isDisposed = false;
  bool isKor = false;
  bool isAudio = false;
  bool isMic = false;
  bool isLeft = false;
  int recordScore;

  AudioPlayer _audioPlayer = AudioPlayer();

  _TwoCentContentCardState({this.data});

  @override
  void initState() {
    setState(() {
      isLeft = widget.data.floating.toUpperCase() == "LEFT" ? true : false;
    });
  }

  @override
  void dispose() {
    _audioPlayer?.dispose();
    isDisposed = true;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screen = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment:
          isLeft ? CrossAxisAlignment.start : CrossAxisAlignment.end,
      children: [
        isLeft ? leftHeader() : rightHeader(),
        SizedBox(height: 4),
        ConstrainedBox(
          constraints: BoxConstraints(maxWidth: screen.width * 0.73),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: isLeft ? Radius.circular(15) : Radius.zero,
                topLeft: !isLeft ? Radius.circular(15) : Radius.zero,
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15),
              ),
              border: Border.all(color: kLeftMsgBorder, width: 1),
              color: isAudio || isMic ? kBtnActive : kLeftMsgBg,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.all(12.0),
                  child: Text(
                    data.eng,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(color: isAudio || isMic ? white : black0),
                  ),
                ),
                isKor
                    ? Container(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(
                          data.kor,
                          style: Theme.of(context).textTheme.bodyText2.copyWith(
                              color: isAudio || isMic ? white : black0),
                        ),
                      )
                    : Container(width: 0),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget leftHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          data.name,
          style: Theme.of(context).textTheme.bodyText2,
        ),
        SizedBox(width: 12),
        // Audio
        InkWell(
          onTap: () {
            playAudio();
          },
          child: button(
            image: kSoundSvg,
            isActive: isAudio,
          ),
        ),
        SizedBox(width: 4),
        // Translate
        InkWell(
          onTap: () {
            setState(() {
              isKor = !isKor;
            });
          },
          child: button(
            image: kHangulSvg,
            isActive: isKor,
          ),
        ),
        SizedBox(width: 4),
        // Mic
        InkWell(
          onTap: () async {
            recordAudio();
          },
          child: button(
            image: kMicSmallSvg,
            isActive: isMic,
          ),
        ),
        SizedBox(width: 4),
        recordScore != null ? scoreBox(score: recordScore) : Container(),
      ],
    );
  }

  Widget rightHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        recordScore != null ? scoreBox(score: recordScore) : Container(),
        SizedBox(width: 4),
        // Mic
        InkWell(
          onTap: () {
            recordAudio();
          },
          child: button(
            image: kMicSmallSvg,
            isActive: isMic,
          ),
        ),
        SizedBox(width: 4),
        // Translate
        InkWell(
          onTap: () {
            setState(() {
              isKor = !isKor;
            });
          },
          child: button(
            image: kHangulSvg,
            isActive: isKor,
          ),
        ),
        SizedBox(width: 4),
        // Audio
        InkWell(
          onTap: () {
            playAudio();
          },
          child: button(
            image: kSoundSvg,
            isActive: isAudio,
          ),
        ),
        SizedBox(width: 12),
        Text(
          data.name,
          style: Theme.of(context).textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget scoreBox({int score}) {
    String text = "";
    if (score >= 70) {
      text = "A";
    } else if (score >= 40) {
      text = "B";
    } else {
      text = "C";
    }
    return Container(
      width: 28,
      height: 28,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: kBtnActive2, width: 1),
        color: kBtnActive2,
      ),
      child: Center(
        child: Text(
          text,
          style: Theme.of(context)
              .textTheme
              .bodyText1
              .copyWith(color: white, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget button({String image, bool isActive = false}) {
    return Container(
      width: 28,
      height: 28,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: kBtnActive, width: 1),
        color: isActive ? kBtnActive : Colors.transparent,
      ),
      child: SizedBox(
        width: 10,
        height: 10,
        child: SvgPicture.asset(
          image,
          color: isActive ? white : kBtnActive,
        ),
      ),
    );
  }

  void recordAudio() async {
    TwoCentsProvider tp = Provider.of<TwoCentsProvider>(context, listen: false);
    if (!tp.isAudioActive && !tp.isMicActive) {
      tp.isMicActive = true;
      setState(() {
        isMic = true;
      });

      int score = await widget.onMic(text: data.eng);

      tp.isMicActive = false;
      if (!isDisposed) {
        setState(() {
          recordScore = score;
          isMic = false;
        });
      }
    }
  }

  void playAudio() async {
    TwoCentsProvider tp = Provider.of<TwoCentsProvider>(context, listen: false);
    if (!tp.isAudioActive && !tp.isMicActive) {
      int result = await _audioPlayer.play('${data.audio}');
      if (result == 1) {
        tp.isAudioActive = true;
        setState(() {
          isAudio = true;
        });
      }
      _audioPlayer.onPlayerStateChanged.listen((event) {
        switch (event) {
          case AudioPlayerState.COMPLETED:
          case AudioPlayerState.STOPPED:
          case AudioPlayerState.PAUSED:
            tp.isAudioActive = false;
            if (!isDisposed) {
              setState(() {
                isAudio = false;
              });
            }
            break;
        }
      });
    }
  }
}
