import 'dart:async';
import 'dart:io';

import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/data/twoCents/twoCents.dart';
import 'package:collars_app_flutter/layouts/twocents/two_cents_content_card.dart';
import 'package:collars_app_flutter/services/maum.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/utils/directory_helper.dart';
import 'package:file/local.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:path_provider/path_provider.dart';

class TwoCentsContentScreen extends StatefulWidget {
  static const String id = 'two_cents_content_screen';
  final TwoCents data;

  const TwoCentsContentScreen({
    Key key,
    this.data,
  }) : super(key: key);

  @override
  _TwoCentsContentScreenState createState() =>
      _TwoCentsContentScreenState(data: data);
}

class _TwoCentsContentScreenState extends State<TwoCentsContentScreen>
    with TickerProviderStateMixin {
  MaumService _maumService = MaumService();
  final TwoCents data;
  bool isRecording = false;
  DirectoryHelper _directoryHelper = DirectoryHelper();
  LocalFileSystem localFileSystem = LocalFileSystem();
  FlutterAudioRecorder _recorder;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  String tempPath;

  String recordingStatus = AUDIO_STOPPED;
  final recordingTime = 8;

  // Modal
  bool isModalOpen = false;
  AnimationController _modalAniController;
  Animation<Offset> _modalAni;
  AnimationController _progressAniController;
  Animation<double> _progressAni;

  _TwoCentsContentScreenState({this.data});

  @override
  void initState() {
    _micInit();
    _animationInit();
  }

  _micInit() async {
    if (await FlutterAudioRecorder.hasPermissions) {
      await _directoryHelper.deleteCacheDir();
      dynamic tempDirectory = await getTemporaryDirectory();
      tempPath = tempDirectory.path + "/request.wav";
      _recorder = FlutterAudioRecorder(tempPath, audioFormat: AudioFormat.WAV);
      await _recorder.initialized;
      var current = await _recorder.current(channel: 0);
      setState(() {
        _currentStatus = current.status;
      });
    }
  }

  _animationInit() {
    _modalAniController =
        AnimationController(duration: Duration(milliseconds: 300), vsync: this);
    _modalAni = Tween<Offset>(begin: Offset(0.0, 1.0), end: Offset.zero)
        .animate(_modalAniController);

    _progressAniController = AnimationController(
        duration: Duration(seconds: recordingTime), vsync: this);
    _progressAni =
        Tween<double>(begin: 0.0, end: 1.0).animate(_progressAniController)
          ..addListener(() {
            setState(() {});
          });
  }

  @override
  void dispose() {
    super.dispose();
    if (_currentStatus != RecordingStatus.Unset &&
        _currentStatus != RecordingStatus.Initialized) _recorder.stop();
    _modalAniController?.dispose();
    _progressAniController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screen = MediaQuery.of(context).size;
    String recordingText = "";
    switch (recordingStatus) {
      case AUDIO_RECORDING:
        recordingText = "위 문장을 따라 읽어보세요.";
        break;
      case AUDIO_SENDING:
        recordingText = "AI 가 발음을 평가중입니다...";
        break;
      default:
        recordingText = "";
        break;
    }

    void openModal() {
      _modalAniController.forward(from: 0);
      _progressAniController.forward(from: 0);
    }

    Future delayedMic({String text}) {
      int score;
      final duration = Duration(seconds: recordingTime);
      return Future.delayed(duration, () async {
        Recording recordResult = await _recorder.stop();
        setState(() {
          recordingStatus = AUDIO_SENDING;
        });
        File file = localFileSystem.file(recordResult.path);
        score = await _maumService.speaking(file: file, textList: [text]);
        await localFileSystem.file(recordResult.path).delete();
        setState(() {
          recordingStatus = AUDIO_STOPPED;
        });
        _modalAniController.reverse(from: 1.0);
        return score;
      });
    }

    Future<int> onMic({String text}) async {
      if (recordingStatus == AUDIO_STOPPED) {
        openModal();

        setState(() {
          recordingStatus = AUDIO_RECORDING;
        });
        _recorder.start();
        var recording = await _recorder.current(channel: 0);
        return await delayedMic(text: text).then((score) => score);
      }
    }

    List<Widget> _buildBody() {
      List<Widget> _list = List();
      data.data.content.forEach((value) {
        _list.add(SizedBox(height: 16));
        _list.add(TwoCentContentCard(
          data: value,
          onMic: onMic,
        ));
      });
      return _list;
    }

    return Scaffold(
      appBar: DefaultAppBar(
        title: "Two Cents",
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    ..._buildBody(),
                    SizedBox(height: 100),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SlideTransition(
                position: _modalAni,
                child: Container(
                  width: screen.width,
                  height: 70,
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 6,
                        child: LinearProgressIndicator(
                          value: _progressAniController.value,
                          backgroundColor: Colors.black,
                          valueColor: AlwaysStoppedAnimation<Color>(kBtnActive),
                        ),
                      ),
                      Container(
                        height: 64,
                        child: Center(
                          child: Text("$recordingText"),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
