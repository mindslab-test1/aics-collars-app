import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/container/top_msg_container.dart';
import 'package:collars_app_flutter/data/twoCents/twoCents.dart';
import 'package:collars_app_flutter/layouts/exam/ai_practice/message.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TempTwoCentScreen extends StatefulWidget {
  final TwoCents data;

  const TempTwoCentScreen({Key key, this.data}) : super(key: key);

  @override
  _TempTwoCentScreenState createState() => _TempTwoCentScreenState();
}

class _TempTwoCentScreenState extends State<TempTwoCentScreen> {
  // top msg appeared
  bool isAppeared = true;

  // chat, user 대화 List
  List<Message> _msgList = List();



  @override
  void initState(){
    super.initState();
    _msgList.add(Message(direction: 'center', title: widget.data.data.title.eng));
    twoCentData();
  }



  // 각각의 대화를 분류
  twoCentData(){
    widget.data.data.content.forEach((element) {
      if(element.floating == 'left'){
        leftSpeak(element.floating, element.eng, element.kor, element.name);
      }else{
        rightSpeak(element.floating, element.eng, element.kor, element.name);
      }
    });
  }

  // ui 왼쪽
  leftSpeak(String direction, String eng, String kor, String nickName){
    _msgList.add(Message(direction: direction, content: eng, hintKor:kor, needButtons: true, isTwoCent: true, nickName: nickName, isFinished: false, ));
  }

  // ui 오른쪽
  rightSpeak(String direction, String eng, String kor, String nickName){
    _msgList.add(Message(direction: direction, content: eng, hintKor:kor, needButtons: true, isTwoCent: true, nickName: nickName, isFinished: false,));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: 'Two Cents', color: black,),
      body: Container(
          child: Column(
            children: [
              Visibility(
                visible: isAppeared,
                child: Column(
                  children: [
                    TopMsgContainer(
                      isAppeared: isAppeared,
                      onTap: (){
                        setState(() {
                          isAppeared = !isAppeared;
                        });
                      },
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                  ],
                ),
              ),
              Flexible(
                child: ListView.builder(
                  itemCount: _msgList.length,
                  itemBuilder: (_, int idx) => _msgList[idx],
                ),
              ),
            ],
          )
      ),
    );
  }
}
