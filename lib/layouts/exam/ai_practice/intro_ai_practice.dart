import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/button/capsule_contrast_button.dart';
import 'package:collars_app_flutter/components/button/later_button.dart';
import 'package:collars_app_flutter/layouts/exam/ai_practice/ai_practice_screen.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class IntroAiPractice extends StatelessWidget {
  static const String id = 'intro_ai_practice';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: DefaultAppBar(title: "",color: white,),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Container(
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                gradient: kDefaultGradient,
              ),
              child: Container(
                margin: EdgeInsets.only(bottom: 15.4),
                child: SvgPicture.asset('assets/title/txt_AI PRACTICE.svg'),
              )
            ),
          ),
          Expanded(
            child: Container(
              padding: kPaddingHorizontal,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 12),
                  Container(
                    padding: kPaddingHorizontal,
                    child: Text('#1 라이선스 계약 제안 시 상황에서 쓸 수 있는 실용적인 대화표현',
                    style: TextStyle(
                      color: gray4,
                      fontSize: 16
                    ),
                    textAlign: TextAlign.center,),
                  ),
                  Expanded(child: Container()),
                  CapsuleContrastButton(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => AiPracticeScreen()),
                      );
                    },
                    type: FIXED,
                    padding: EdgeInsets.symmetric(vertical: 16),
                    isActive: true,
                    text: "Start",
                    primaryColor: kBtnActive,
                  ),
                  SizedBox(height: 8),
                  LaterButton(),
                  SizedBox(height: 36,)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
