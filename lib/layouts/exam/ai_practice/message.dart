import 'dart:io';
import 'package:audioplayers/audioplayers.dart';
import 'package:collars_app_flutter/components/button/chatbot_no_button.dart';
import 'package:collars_app_flutter/components/button/chatbot_yes_button.dart';
import 'package:collars_app_flutter/components/button/mic_button.dart';
import 'package:collars_app_flutter/components/button/hint_button.dart';
import 'package:collars_app_flutter/components/container/mic_record_container.dart';
import 'package:collars_app_flutter/providers/twocents_provider.dart';
import 'package:collars_app_flutter/services/maum.dart';
import 'package:collars_app_flutter/services/types.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:collars_app_flutter/styles/text/default.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:file/local.dart';

class Message extends StatefulWidget {
  // common
  final String direction;
  final String content;
  final String title;
  final bool needButtons;

  // ai practice
  final Function chatbotMsg;
  final Function callChatbot;
  final bool isFinished;

  // two cent
  final bool isTwoCent;
  final String hintKor;
  final String nickName;

  const Message({
    Key key,
    this.direction,
    this.content,
    this.chatbotMsg,
    this.title,
    this.callChatbot,
    this.isFinished,
    this.needButtons,
    this.isTwoCent,
    this.hintKor,
    this.nickName,
  }) : super(key: key);

  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> with AutomaticKeepAliveClientMixin {
  AudioPlayer audioPlayer = AudioPlayer();

  bool isSpeechClicked = false;
  bool isHintBtnPressed = false;
  bool isMicBtnPressed = false;
  bool isScoreAppeared = false;
  TwoCentsProvider twoCentsProvider;
  int speakScore;
  MaumService _maumService = MaumService();
  LocalFileSystem localFileSystem = LocalFileSystem();
  String recordingStatus = AUDIO_STOPPED;
  double screenWidth;


  Future receiveChatbotAudio(String msg) async {
    final response = await MaumService().downloadChatAudio(msg);
    // wav file 재생되는 부분
    audioPlayer.playBytes(response);
    return;
  }

  // two cent score 관련
  Future delayedMic(Recording recordResult, File file)  async{
    int score = await _maumService.speaking(
        file: file,
        textList: [widget.content]
    );
    await localFileSystem.file(recordResult.path).delete();
    setState(() {
      recordingStatus = AUDIO_STOPPED;
    });
    speakScore = score;
    isMicBtnPressed = !isMicBtnPressed;
    setState(() {
      isScoreAppeared = true;
    });
  }

  // two cent common speak
  Widget twoCentSpeak(
    bool isLeft,
    String nickName,
  ) {
    return Column(
      crossAxisAlignment:
          isLeft ? CrossAxisAlignment.start : CrossAxisAlignment.end,
      children: [
        IntrinsicWidth(
          child: Container(
            margin: isLeft
                ? const EdgeInsets.only(left: 24)
                : const EdgeInsets.only(right: 24.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment:
                  isLeft ? MainAxisAlignment.start : MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                    margin: const EdgeInsets.only(bottom: 8),
                    child: isLeft
                        ? Text(
                            nickName,
                            style: kAiMsgNameStyle,
                          )
                        : Text(
                            nickName,
                            style: kAiMsgNameStyle,
                            textAlign: TextAlign.right,
                          )),
                GestureDetector(
                  onTap: () {
                    if (isSpeechClicked == false &&
                        twoCentsProvider.isAudioActive == false &&
                        twoCentsProvider.isRecordBtnActive == false) {
                      setState(() {
                        isSpeechClicked = true;
                        twoCentsProvider.isAudioActive = true;
                        receiveChatbotAudio(widget.content);
                        audioPlayer.onPlayerCompletion.listen((event) {
                          setState(() {
                            isSpeechClicked = false;
                            twoCentsProvider.isAudioActive = false;
                          });
                        });
                      });
                    }
                  },
                  child: Container(
                    constraints: BoxConstraints(maxWidth: 240),
                    padding: const EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                        color: isSpeechClicked ? Colors.white : kChatbotMsgBg,
                        border: isSpeechClicked
                            ? Border.all(width: 2, color: blue4)
                            : Border.all(width: 0, color: Colors.transparent),
                        borderRadius: BorderRadius.only(
                          topLeft: isLeft ? Radius.zero : Radius.circular(8.0),
                          topRight: isLeft ? Radius.circular(8.0) : Radius.zero,
                          bottomRight: Radius.circular(8.0),
                          bottomLeft: Radius.circular(8.0),
                        )),
                    child: Stack(children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Flexible(
                              child: Text(
                            widget.content,
                            style: kAiMsgNameStyle.copyWith(
                                fontWeight: FontWeight.w500),
                          )),
                          if (isScoreAppeared && speakScore!=null) scoreImage(speakScore),
                        ],
                      ),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
        hideHintSpeak(isLeft),
        Container(
          margin: isLeft
              ? const EdgeInsets.only(left: 24.0)
              : const EdgeInsets.only(right: 24.0),
          constraints: BoxConstraints(maxWidth: screenWidth * 0.481),
          child: Column(
            children: [
              SizedBox(
                height: 4,
              ),
              if (isLeft)
                buttonMenu(hintButton(true), micButton(true), widget.isTwoCent),
              if (isLeft == false)
                buttonMenu(
                    micButton(false), hintButton(false), widget.isTwoCent),
            ],
          ),
        ),
        SizedBox(
          height: 12,
        )
      ],
    );
  }

  // two cent에서 사용하는 한글힌트 컨테이너
  Widget hideHintSpeak(bool isLeft) {
    return Visibility(
      visible: isHintBtnPressed,
      child: Container(
        constraints: BoxConstraints(maxWidth: 240),
        padding: const EdgeInsets.all(12.0),
        margin: isLeft
            ? const EdgeInsets.only(top: 4, bottom: 4, left: 24)
            : const EdgeInsets.only(top: 4, bottom: 4, right: 24),
        decoration: BoxDecoration(
            color: kChatbotMsgBg,
            borderRadius: BorderRadius.only(
              topRight: isLeft ? Radius.circular(8.0) : Radius.zero,
              topLeft: isLeft ? Radius.zero : Radius.circular(8.0),
              bottomRight: Radius.circular(8.0),
              bottomLeft: Radius.circular(8.0),
            )),
        child: Text(widget.hintKor == null ? "hello" : widget.hintKor,
            style: kAiMsgNameStyle.copyWith(fontWeight: FontWeight.w500)),
      ),
    );
  }

  // twocent score
  Widget scoreImage(int score) {
    String img = "";
    if (score >= 70) {
      img = eGradeAsvg;
    } else if (score >= 40) {
      img = eGradBsvg;
    } else {
      img = eGradeCsvg;
    }
    return Container(child: SvgPicture.asset(img));
  }

  // ai practice left speak
  Widget leftSpeak() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        IntrinsicWidth(
          child: Container(
            margin: const EdgeInsets.only(left: 24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                    margin: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      'Keith',
                      style: kAiMsgNameStyle,
                    )),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isSpeechClicked = !isSpeechClicked;
                      receiveChatbotAudio(widget.content);
                      if (isSpeechClicked) {
                        audioPlayer.onPlayerCompletion.listen((event) {
                          setState(() {
                            isSpeechClicked = false;
                          });
                        });
                      }
                    });
                  },
                  child: Container(
                    constraints: BoxConstraints(maxWidth: 240),
                    padding: const EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                        color: isSpeechClicked ? Colors.white : kChatbotMsgBg,
                        border: isSpeechClicked
                            ? Border.all(width: 2, color: blue4)
                            : Border.all(width: 0, color: Colors.transparent),
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8.0),
                          bottomRight: Radius.circular(8.0),
                          bottomLeft: Radius.circular(8.0),
                        )),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          widget.content,
                          style: kAiMsgNameStyle.copyWith(
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        if (widget.isFinished)
          Container(
            margin: const EdgeInsets.only(left: 24.0),
            child: Row(
              children: [ChatbotYesButton(), ChatbotNoButton()],
            ),
          ),
        SizedBox(
          height: 12,
        )
      ],
    );
  }

  // ai practice right speak
  Widget rightSpeak() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        IntrinsicWidth(
          child: Container(
            margin: const EdgeInsets.only(right: 24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                    margin: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      'Chloe',
                      style: kAiMsgNameStyle,
                      textAlign: TextAlign.right,
                    )),
                if (widget.needButtons)
                  buttonMenu(
                      micButton(false), hintButton(false), widget.isTwoCent)
              ],
            ),
          ),
        ),
        SizedBox(
          height: 12,
        )
      ],
    );
  }

  // mic btn
  Widget micButton(bool micLeft) {
    if (!twoCentsProvider.isRecordBtnActive) {
      isMicBtnPressed = false;
    }
    return GestureDetector(
      onTap: () {
        if (twoCentsProvider.isRecordBtnActive == false &&
            twoCentsProvider.isAudioActive == false) {
          setState(() {
            isMicBtnPressed = true;
            twoCentsProvider.isRecordBtnActive = true;
          });
        }
      },
      child: MicButton(
        isLeft: micLeft,
      ),
    );
  }

  // hint btn
  Widget hintButton(bool hintLeft) {
    return HintButton(
      isLeft: hintLeft,
      imgPath: widget.isTwoCent ? kHintHangulSvg : kHintSoundSvg,
      color: isHintBtnPressed ? gray4 : blue4,
      onTap: () {
        setState(() {
          isHintBtnPressed = !isHintBtnPressed;
        });
      },
    );
  }

  // mic, hint btn 세트(two cent와 ai practice의 버튼 순서가 다르기 때문)
  Widget buttonMenu(Widget firstMenu, Widget secondMenu, bool isTwoCent) {
    return Stack(
      children: [
        if (isMicBtnPressed == false)
          Container(
            child: Row(
              children: [
                if (widget.direction == 'right') Flexible(child: Container()),
                firstMenu,
                SizedBox(
                  width: 1,
                ),
                secondMenu
              ],
            ),
          ),
        if (isMicBtnPressed)
          MicRecordContainer(
            chatbotMsg: widget.chatbotMsg,
            callChatbot: widget.callChatbot,
            isTwoCent: isTwoCent,
            micButton: secondMenu,
            hintButton: firstMenu,
            direction: widget.direction,
            recordFinish: delayedMic,
            screenWidth: screenWidth,
          )
      ],
    );
  }

  // chapter title
  Widget centerSpeak() {
    return Container(
        height: 24.0,
        margin: const EdgeInsets.only(bottom: 12),
        child: Center(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 3.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(13.0), color: Colors.black),
              child: Text(
                widget.title,
                style: kAiMsgStyle.copyWith(color: Colors.white),
                textAlign: TextAlign.center,
              ),
        )));
  }


  @override
  void dispose(){
    super.dispose();
    audioPlayer.stop();
  }

  @override
  Widget build(BuildContext context) {
    twoCentsProvider = Provider.of<TwoCentsProvider>(context);
    screenWidth = MediaQuery.of(context).size.width;
    return Container(
        width: double.infinity,
        child: widget.direction == 'center'
            ? centerSpeak()
            : widget.direction == 'left'
                ? widget.isTwoCent
                    ? twoCentSpeak(true, widget.nickName)
                    : leftSpeak()
                : widget.isTwoCent
                    ? twoCentSpeak(false, widget.nickName)
                    : rightSpeak());
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
