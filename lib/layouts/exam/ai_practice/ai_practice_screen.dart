import 'dart:async';
import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/container/top_msg_container.dart';
import 'package:collars_app_flutter/layouts/exam/ai_practice/message.dart';
import 'package:collars_app_flutter/services/ai_practice.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/text/default.dart';
import 'package:flutter/material.dart';

import '../../../data/chatbot_result.dart';

class AiPracticeScreen extends StatefulWidget {

  @override
  _AiPracticeScreenState createState() => _AiPracticeScreenState();
}

class _AiPracticeScreenState extends State<AiPracticeScreen> {
  List<Message> _msgList = List();
  ScrollController _scrollController = ScrollController();
  ChatbotResult chatbotRes;
  // top msg show
  bool msgShow = true;

  userMsg(String msg) {
    setState(() {
      _msgList.add(Message(direction: 'right', chatbotMsg: chatbotMsg, callChatbot: callChatbot,));
    });
  }

  chatbotMsg(String msg){
    setState((){
      _msgList.add(Message(direction: 'left', content: msg, isFinished: false, isTwoCent: false, needButtons: true,));
      _msgList.add(Message(direction: 'right', chatbotMsg: chatbotMsg, callChatbot: callChatbot, isTwoCent: false, needButtons: true,));
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState(){
    super.initState();
    _msgList.add(Message(direction: 'center', title: '#1 라이선스 계약 제안',));
  callChatbot('test_ha', '안녕하세요');
  }

  // chapter start
  Future callChatbot(String userInfo, String msg) async{
    // 특정 말이 입력되면 chatbot에서 endMsg를 띄움
    msg = msg.replaceAll(' ', '');
    if(msg == '그만'){
      setState(() {
        _msgList.add(Message(direction: 'left', content: kAiFinishMsg, isFinished: true, needButtons: false, isTwoCent: false,));
      });
      return;
    }
    final response = await ChatbotService().sendUserMsg(userInfo, msg);
    chatbotRes = ChatbotResult.fromJson(response);
    setState(() {
      chatbotMsg(chatbotRes.answer);
    });

  }



  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 1), ()=>{
      if(_scrollController.hasClients){
        _scrollController.animateTo(_scrollController.position.maxScrollExtent, duration: Duration(milliseconds: 500), curve: Curves.easeInOut)
      }
    });


    return Scaffold(
        appBar: DefaultAppBar(title: 'AI Practice', color: black,),
        body: Container(
          child: Column(
            children: [
              Visibility(
                visible: msgShow,
                child: Column(
                  children: [
                    TopMsgContainer(
                      isAppeared: msgShow,
                      onTap: (){
                        setState(() {
                          msgShow = !msgShow;
                        });
                      },
                    ),
                    SizedBox(
                      height: 16.0,
                    )
                  ],
                ),
              ),
              Flexible(
                child: ListView.builder(
                  controller: _scrollController,
                  itemCount: _msgList.length,
                  itemBuilder: (_, int idx) => _msgList[idx],
                ),
              ),
            ],
          )
                ),
              );
  }
}
