import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/components/deco/divider/horizontal_divider.dart';
import 'package:collars_app_flutter/layouts/wrap_screen.dart';
import 'package:collars_app_flutter/services/auth.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';

class SettingScreen extends StatelessWidget {
  static const String id = 'setting_screen';

  @override
  Widget build(BuildContext context) {

    // Default Items
    Widget SettingContainer({Widget child}) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 14),
        child: child,
      );
    }
    Widget SettingText(String text,
        {FontWeight fontWeight = FontWeight.normal}) {
      return Text(
        text,
        style: Theme.of(context)
            .textTheme
            .bodyText2
            .copyWith(fontWeight: fontWeight),
      );
    }

    return Scaffold(
      appBar: DefaultAppBar(title: "환경설정"),
      body: Container(
        padding: kPaddingHorizontal,
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SettingContainer(
                      child: SettingText('알림설정', fontWeight: FontWeight.w500),
                    ),
                    HorizontalDivider(color: kDivider),
                    SettingContainer(
                      child: SettingText('공지사항', fontWeight: FontWeight.w500),
                    ),
                    HorizontalDivider(color: kDivider),
                    SettingContainer(
                      child: SettingText('FAQ', fontWeight: FontWeight.w500),
                    ),
                    HorizontalDivider(color: kDivider),
                    SettingContainer(
                      child: SettingText('증빙 서류 발급', fontWeight: FontWeight.w500),
                    ),
                    HorizontalDivider(color: kDivider),
                    SettingContainer(
                      child: SettingText('이벤트 / 마케팅 정보 수신 설정', fontWeight: FontWeight.w500),
                    ),
                    HorizontalDivider(color: kDivider),
                  ],
                ),
              ),
            ),
            Container(
              height: 52,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: (){
                        AuthService _authService = AuthService();
                        _authService.signOut();
                        Navigator.pushReplacementNamed(context, WrapScreen.id);
                      },
                      child: Center(
                        child: SettingText('로그아웃'),
                      ),
                    ),
                  ),
                  VerticalDivider(indent: 12, thickness: 1, color: kDivider),
                  Expanded(
                    child: Center(
                      child: SettingText('회원탈퇴'),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 24,
            )
          ],
        ),
      ),
    );
  }
}