import 'package:collars_app_flutter/components/deco/info_box.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:collars_app_flutter/styles/text/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InfoScreen extends StatefulWidget {
  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  @override
  Widget build(BuildContext context) {
    final User _user = Provider.of<User>(context);

    String _getInfo({int questionId}) {
      String result = "";
      _user.data.survey.forEach((survey) {
        if (survey.questionId == questionId) result = survey.answer;
      });
      return result;
    }

    return Container(
      padding: kPaddingHorizontal,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 24),
          InfoBox(
            title: "이름",
            info: _user.data.name,
          ),
          SizedBox(height: 16),
          InfoBox(
            title: "직무",
            info: _getInfo(questionId: 1),
          ),
          SizedBox(height: 16),
          InfoBox(
            title: "직군",
            info: _getInfo(questionId: 2),
          ),
          SizedBox(height: 16),
          InfoBox(
            title: "학습목적",
            info: _getInfo(questionId: 3),
          ),
          SizedBox(height: 16),
          InfoBox(
            title: "개선하고자 하는 역량",
            info: _getInfo(questionId: 4),
          ),
          SizedBox(height: 52),
          Text(
            kCopyright,
            style: Theme.of(context).textTheme.caption.copyWith(color: black),
          )
        ],
      ),
    );
  }
}
