import 'package:collars_app_flutter/components/container/banner_container.dart';
import 'package:collars_app_flutter/components/container/custom_app_bar.dart';
import 'package:collars_app_flutter/components/container/custom_safe_area.dart';
import 'package:collars_app_flutter/components/container/custom_tab_bar.dart';
import 'package:collars_app_flutter/components/etc/prepareing.dart';
import 'package:collars_app_flutter/layouts/main/mypage/info_screen.dart';
import 'package:collars_app_flutter/layouts/main/mypage/insight_screen.dart';
import 'package:collars_app_flutter/layouts/setting/setting_screen.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/routes/route_form.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class MyPageScreen extends StatefulWidget {
  static const String id = 'my_page_screen';

  @override
  _MyPageScreenState createState() => _MyPageScreenState();
}

class _MyPageScreenState extends State<MyPageScreen> {
  int _current = 0;

  List<RouteForm> _pages = [
    RouteForm(name: "인사이트", component: InsightScreen()),
    RouteForm(name: "북마크", component: Preparing()),
    RouteForm(name: "수강내역", component: Preparing()),
    RouteForm(name: "내정보", component: InfoScreen()),
  ];

  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);

    Widget _buildBanner() {
      return BannerContainer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CustomSafeArea(),
            CustomAppBar(
              title: "마이페이지",
              titleColor: white,
              action: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, SettingScreen.id);
                },
                child: SvgPicture.asset(kSetting),
              ),
            ),
            SizedBox(height: 8),
            SvgPicture.asset(kFakeUserEditSvg),
            SizedBox(height: 16),
            Text(
              _user.data.name,
              style:
                  Theme.of(context).textTheme.headline2.copyWith(color: white),
            ),
            SizedBox(height: 24),
          ],
        ),
      );
    }

    return SingleChildScrollView(
      child: Column(
        children: [
          _buildBanner(),
          CustomTabBar(
            onChanged: (index) {
              setState(() {
                _current = index;
              });
            },
            initialIndex: _current,
            children: _pages,
          ),
          _pages[_current].component,
        ],
      ),
    );
  }
}
