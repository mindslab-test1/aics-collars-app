import 'package:collars_app_flutter/components/container/banner_container.dart';
import 'package:collars_app_flutter/components/container/custom_app_bar.dart';
import 'package:collars_app_flutter/components/container/custom_safe_area.dart';
import 'package:collars_app_flutter/components/container/custom_tab_bar.dart';
import 'package:collars_app_flutter/layouts/main/collarspro/collars_pro_section.dart';
import 'package:collars_app_flutter/layouts/main/collarspro/my_session_section.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/routes/route_form.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class CollarsProScreen extends StatefulWidget {
  static const String id = 'collars_pro_screen';

  @override
  _CollarsProScreenState createState() => _CollarsProScreenState();
}

class _CollarsProScreenState extends State<CollarsProScreen> {
  int _current = 0;

  List<RouteForm> _pages = [
    RouteForm(name: "COLLARS PRO", component: CollarsProSection()),
    RouteForm(name: "MY SESSION", component: MySessionSection()),
  ];

  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);

    Widget _buildBanner() {
      return BannerContainer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CustomSafeArea(),
            CustomAppBar(
              title: "Collars Pro",
              titleColor: white,
            ),
            SizedBox(height: 8),
            SvgPicture.asset(kUserEditSvg),
            SizedBox(height: 16),
            Text(
              _user.data.name,
              style:
                  Theme.of(context).textTheme.headline2.copyWith(color: white),
            ),
            SizedBox(height: 24),
          ],
        ),
      );
    }

    return SingleChildScrollView(
      child: Column(
        children: [
          _buildBanner(),
          CustomTabBar(
            onChanged: (index) {
              setState(() {
                _current = index;
              });
            },
            initialIndex: _current,
            children: _pages,
          ),
          _pages[_current].component,
        ],
      ),
    );
  }
}
