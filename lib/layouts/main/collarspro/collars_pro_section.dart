import 'package:collars_app_flutter/components/builder/session/suggestion_carousel.dart';
import 'package:collars_app_flutter/components/deco/linear_chart_box.dart';
import 'package:collars_app_flutter/components/deco/radar_chart_box.dart';
import 'package:collars_app_flutter/components/deco/suggestion_box.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/text/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CollarsProSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final User _user = Provider.of<User>(context);

    return _user.data.exam.length <= 0
        ? Container()
        : Column(
      children: [
        SizedBox(height: 24),
        RadarChartBox(size: 250, data: _user.data.exam[0].data),
        SizedBox(height: 20),
        LinearChartBox(data: _user.data.exam[0].data),
        SizedBox(height: 24),
        SuggestionBox(),
        SizedBox(height: 24),
        SuggestionCarousel(),
        SizedBox(height: 48),
        Text(
          kCopyright,
          style:
          Theme.of(context).textTheme.caption.copyWith(color: black),
        ),
        SizedBox(height: 60),
      ],
    );
  }
}
