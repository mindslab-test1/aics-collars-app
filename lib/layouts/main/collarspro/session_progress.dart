import 'package:collars_app_flutter/data/exam/exam.dart';
import 'package:collars_app_flutter/layouts/intro/session/intro_session_exam.dart';
import 'package:collars_app_flutter/layouts/main/collarspro/session_video_player.dart';
import 'package:collars_app_flutter/providers/session_provider.dart';
import 'package:collars_app_flutter/services/exam.dart';
import 'package:collars_app_flutter/styles/color/default.dart';
import 'package:collars_app_flutter/styles/color/palette.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SessionProgress extends StatelessWidget {
  ExamService _examService = ExamService();

  @override
  Widget build(BuildContext context) {
    SessionProvider _session = Provider.of<SessionProvider>(context);

    Widget _box(int index, String text) {
      NumberFormat formatter = new NumberFormat("00");
      return Container(
          padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 24),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: kBtnActive,
          ),
          child: Row(
            children: [
              Text(
                '${formatter.format(index)}',
                style: Theme.of(context)
                    .textTheme
                    .subtitle2
                    .copyWith(color: white, fontSize: 20),
              ),
              SizedBox(width: 16),
              Expanded(
                  child: Text(
                text,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2
                    .copyWith(color: white),
              ))
            ],
          ));
    }

    Widget verticalLine() {
      return Container(
        width: 1,
        height: 16,
        margin: EdgeInsets.only(left: 36),
        decoration: BoxDecoration(border: Border.all(color: gray4)),
      );
    }

    return Container(
      padding: kPaddingHorizontal,
      child: _session.current == null
          ? Container()
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '세션 학습현황',
                  style: Theme.of(context).textTheme.subtitle2,
                ),
                SizedBox(height: 12),
                Text(
                  _session.current.data.title,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      .copyWith(fontSize: 18, color: kBtnActive),
                ),
                Text(
                  '',
                  // "Suggestion | ${_session.current.data.} minutes",
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: gray4),
                ),
                SizedBox(height: 12),
                GestureDetector(
                  onTap: () {
                    SessionProvider _session =
                        Provider.of<SessionProvider>(context, listen: false);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => SessionVideoPlayer(
                          data: _session.current,
                        ),
                      ),
                    );
                  },
                  child: _box(1, "영상강의듣기"),
                ),
                verticalLine(),
                GestureDetector(
                  onTap: () async {
                    Exam data = await _examService.getSessionExam(
                        examId: _session.current.examId);
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => IntroSessionExam(data: data)),
                    );
                  },
                  child: _box(2, "QUIZ"),
                ),
                SizedBox(height: 60),
              ],
            ),
    );
  }
}
