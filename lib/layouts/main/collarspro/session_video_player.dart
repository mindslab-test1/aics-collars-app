import 'dart:async';

import 'package:chewie/chewie.dart';
import 'package:collars_app_flutter/components/app_bar/default_app_bar.dart';
import 'package:collars_app_flutter/data/session/session.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class SessionVideoPlayer extends StatefulWidget {
  final Session data;

  const SessionVideoPlayer({
    Key key,
    this.data,
  }) : super(key: key);

  @override
  _SessionVideoPlayerState createState() => _SessionVideoPlayerState();
}

class _SessionVideoPlayerState extends State<SessionVideoPlayer> {
  Timer _timer;
  VideoPlayerController videoPlayerController;
  ChewieController chewieController;

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(const Duration(seconds: 10), (timer) {
      if(chewieController!=null && chewieController.videoPlayerController.value.isPlaying){
        Duration a = chewieController.videoPlayerController.value.position;
      }
    });
    _init();
  }

  Future<void> _init() async {
    videoPlayerController =
        VideoPlayerController.network(widget.data.links.video);
    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      aspectRatio: 3 / 2,
      autoPlay: true,
      looping: false,
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    videoPlayerController.dispose();
    chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final playerWidget = chewieController != null
        ? Chewie(
            controller: chewieController,
          )
        : Container();

    return Scaffold(
      appBar: DefaultAppBar(
        title: "",
      ),
      body: Column(
        children: [
          Container(
            height: 300,
            child: playerWidget,
          ),
        ],
      ),
    );
  }
}
