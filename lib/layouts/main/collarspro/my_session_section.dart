import 'package:collars_app_flutter/components/builder/session/my_session_carousel.dart';
import 'package:collars_app_flutter/layouts/main/collarspro/session_progress.dart';
import 'package:collars_app_flutter/providers/user.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MySessionSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final User _user = Provider.of<User>(context);

    return _user.data.session.length <= 0
        ? Container(
            width: double.infinity,
            height: 500,
            child: Center(
              child: Text(
                '신청한 세션이 없습니다.',
                style: Theme.of(context).textTheme.subtitle2,
              ),
            ),
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 24),
              MySessionCarousel(),
              SizedBox(height: 24),
              SessionProgress(),
            ],
          );
  }
}
