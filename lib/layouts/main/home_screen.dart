import 'package:collars_app_flutter/components/builder/evereading/evereading_carousel.dart';
import 'package:collars_app_flutter/components/builder/evereading/sectors.dart';
import 'package:collars_app_flutter/components/builder/twocents/two_cents_carousel.dart';
import 'package:collars_app_flutter/components/card/intro_nav_card.dart';
import 'package:collars_app_flutter/layouts/main/home/home_banner.dart';
import 'package:collars_app_flutter/styles/size/default.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  static const String id = 'home_screen';

  final Function jumpToPage;

  const HomeScreen({
    Key key,
    this.jumpToPage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          HomeBanner(),
          SizedBox(height: 24),
          Container(
            padding: kPaddingHorizontal,
            child: IntroNavCard(jumpToPage: jumpToPage,),
          ),
          SizedBox(height: 24),
          Container(
            padding: EdgeInsets.only(left: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Two Cents',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                Text(
                  '비즈니스 스몰토크 5분 연습하기',
                  style: Theme.of(context).textTheme.caption,
                ),
              ],
            ),
          ),
          SizedBox(height: 12),
          TwoCentsCarousel(height: 200),
          SizedBox(height: 24),
          Container(
            padding: EdgeInsets.only(left: 24),
            child: Text(
              '하루 5분, 영어읽는 습관',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          SizedBox(height: 12),
          Sectors(),
          SizedBox(height: 12),
          EvereadingCarousel(height: 200),
          SizedBox(height: 60),
        ],
      ),
    );
  }
}
