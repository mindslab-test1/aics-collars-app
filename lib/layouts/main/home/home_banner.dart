import 'package:collars_app_flutter/components/deco/image_slider.dart';
import 'package:collars_app_flutter/providers/session_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SessionProvider _session = Provider.of<SessionProvider>(context);

    List<String> recommandThumbnails(){
      List<String> _list = List();
      _session.recommand.forEach((value) {
        _list.add(value.links.thumbnail);
      });
      return _list;
    }

    return _session.recommand.length <= 0
        ? Container()
        : Container(
            height: 250,
            child: ImageSlider(
              items: recommandThumbnails(),
            ),
          );
  }
}
