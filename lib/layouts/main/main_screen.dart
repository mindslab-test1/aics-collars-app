import 'package:collars_app_flutter/components/bottom_navigtaion_bar/custom_bottom_naviagtion_bar.dart';
import 'package:collars_app_flutter/layouts/main/collars_pro_screen.dart';
import 'package:collars_app_flutter/layouts/main/home_screen.dart';
import 'package:collars_app_flutter/layouts/main/my_page_screen.dart';
import 'package:collars_app_flutter/routes/route_form.dart';
import 'package:collars_app_flutter/styles/icon/default.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  static const String id = 'main_screen';

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int currentPage = 0;

  PageController _pageController = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    _pageController.addListener(() {
      setState(() {
        currentPage = _pageController.page.toInt();
      });
    });

    void jumpToPage(index) {
      _pageController.jumpToPage(index);
    }

    List<RouteForm> _pages = [
      RouteForm(
        icon: kHome,
        component: HomeScreen(
          jumpToPage: jumpToPage,
        ),
      ),
      RouteForm(icon: kCollars, component: CollarsProScreen()),
      RouteForm(icon: kMyPage, component: MyPageScreen()),
    ];

    return Scaffold(
      body: PageView.builder(
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        itemCount: _pages.length,
        itemBuilder: (context, index) {
          return Container(
            child: Stack(
              children: [
                Container(child: _pages[index].component),
                Positioned(
                  bottom: 0,
                  child: CustomBottomNavigationBar(
                    height: 54,
                    children: _pages,
                    jumpToPage: jumpToPage,
                    currentPage: currentPage,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
